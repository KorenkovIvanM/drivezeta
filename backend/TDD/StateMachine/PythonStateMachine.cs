﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using StateMachine.Models;
using StateMachine.ModelDb;

namespace StateMachine
{
    public class PythonStateMachine : StateMachineDb
    {
        private class Script
        {
            public string Path { get; set; }
            public string[] Arguments { get; set; }
            public Script(string path, string[] arguments)
            {
                Path = path;
                Arguments = arguments;
            }
            public Script(ref string[] arguments)
            {
                Path = arguments[0];
                int count = Convert.ToInt32(arguments[1]);
                Arguments = new string[count];
                for (int i = 0; i < count; i++)
                    Arguments[i] = arguments[i];
                string[] newArguments = new string[arguments.Length - (count + 2)];
                for (int i = 0; i < newArguments.Length; i++)
                    newArguments[i] = arguments[i + count + 1];
                arguments = newArguments;
            }
        }

        private Script script;

        protected override (bool, string) _run(ref string[] arguments)
        {
            if (new FileInfo(script.Path).Exists)
            {
                var command = $"python {script.Path}";
                var result = Process.Start("cmd.exe", "/C " + command);
                return (true, SuccessMessage);
            }
            else
                return (false, FailedMessage);
            
            
        }

        public PythonStateMachine(int id)
        {
            var result = ToolsModel.GetById<PythonScript>(id).ResultRead[0];
            script = new Script(result.GlobalPath, new string[0]);
        }
    }
}
