﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine.Models
{
    public class PythonScript
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? GlobalPath { get; set; }
        public string? LocalPath { get; set; }
        public DateTime DateCreate { get; set; } = DateTime.Now;
    }
}
