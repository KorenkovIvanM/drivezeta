﻿
namespace StateMachine.Models
{
    public class Worker
    {
        public int Id { get; set; }
        public string Title { get; set; } = "";
        public string? Description { get; set; }
        public StatusWorker Status { get; set; } = StatusWorker.NonActiv;
        public TypeWorker Type { get; set; }
        public int StateMachineId { get; set; }
        public StateMachine? Start { get; set; }
    }
    public enum StatusWorker
    {
        Running,
        Stoping,
        NonActiv,
    }
    public enum TypeWorker
    {
        Siger,
        Date,
        Form,
    }
}
