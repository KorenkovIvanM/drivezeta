﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using state = StateMachine.Models;

namespace StateMachine
{
    public class Context: DbContext
    {
        public DbSet<state.PythonScript> PythonScripts { get; set; } = null!;
        public DbSet<state.StateMachine> StateMachines { get; set; } = null!;
        public DbSet<state.Worker> Workers { get; set; } = null!;
        public Context() => Database.EnsureCreated();
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseSqlite("data source=config.db");
    }
}
