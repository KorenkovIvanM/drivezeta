﻿using StateMachine;

PythonStateMachine pythonStateMachine = new PythonStateMachine(1);
var arg = new string[2] { "test.py", "0" };
var result = pythonStateMachine.Run(ref arg);
Console.WriteLine(result);
Console.WriteLine(pythonStateMachine.SuccessMessage);
Console.WriteLine(result == pythonStateMachine.SuccessMessage);
