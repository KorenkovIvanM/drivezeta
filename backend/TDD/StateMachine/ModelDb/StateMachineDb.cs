﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine.ModelDb
{
    public abstract class StateMachineDb
    {
        public string
            SuccessMessage = "Success",
            FailedMessage = "Failed",
            ErrorMessage = "Error";
        public StateMachineDb? Success { get; set; }
        public StateMachineDb? Error { get; set; }
        public StateMachineDb? Failed { get; set; }
        protected abstract (bool, string) _run(ref string[] arguments);
        public virtual string Run(ref string[] arguments)
        {
            try
            {
                var result = _run(ref arguments);
                bool type = result.Item1;
                string message = result.Item2;

                if(type)
                {
                    if(Success != null)
                        return Success.Run(ref arguments);
                }
                else
                {
                    if(Failed != null)
                        return Failed.Run(ref arguments);
                }
                return message;
            }
            catch (Exception ex)
            {
                string[] buff = new string[1];
                buff[0] = ex.Message;
                if (Error != null)
                    return Error.Run(ref buff);
                else
                    return ErrorMessage;
            }
        }
    }
}
