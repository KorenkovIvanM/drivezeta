﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine.ModelDb;

namespace StateMachine
{
    public class TestStateMachine : StateMachineDb
    {
        protected override (bool, string) _run(ref string[] arguments)
        {
            Console.WriteLine("Test into state macine");
            return (true, SuccessMessage);
        }
    }
}
