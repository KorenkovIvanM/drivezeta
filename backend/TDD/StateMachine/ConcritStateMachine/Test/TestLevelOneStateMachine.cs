﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine.ModelDb;

namespace StateMachine
{
    public class TestLevelOneStateMachine : StateMachineDb
    {
        protected override (bool, string) _run(ref string[] arguments)
        {
            return (false, FailedMessage);
        }
    }
}
