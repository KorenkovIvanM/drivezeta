﻿using System.Text.Json;
using System.Reflection;
using AutoMap.ActionEntity;
using Microsoft.EntityFrameworkCore;

namespace ToolsEntity
{
    public static class ToolsModel
    {
        public static string
            MESSAGE_ERROR_JSON = "Not json",
            VOID_JSON = "{}";

        public static ResponseDb<T> Create<T, C>(T item) where C : DbContext, new() => 
            decoratorForAction<T, C>(item, _create<T, C>);
        public static ResponseDb<T> Create<T, C>(List<T> item) where C : DbContext, new() => 
            decoratorForAction<T, C>(item, _create<T, C>);
        public static ResponseDb<T> Read<T, C>(int numberPage = 0, int sizePage = 15) where C : DbContext, new() => 
            Read<T, C>(VOID_JSON, numberPage, sizePage);
        public static ResponseDb<T> Read<T, C>(string mask, int numberPage = 0, int sizePage = 15) where C : DbContext, new() => 
            decoratorForAction<T, C>((mask, numberPage, sizePage), _read<T, C>);
        public static ResponseDb<T> Delete<T, C>(int Id) where C : DbContext, new() => 
            decoratorForAction<T, C>(Id, _delete<T, C>);
        public static ResponseDb<T> Update<T, C>(T item, string? mask = null) where C : DbContext, new() =>
            decoratorForAction<T, C>((item, mask), _update<T, C>);
        public static ResponseDb<T> GetById<T, C>(int Id) where C : DbContext, new() =>
            decoratorForAction<T, C>(("{\"Id\": " + Id + "}", 0, 1), _read<T, C>);

        public static ResponseDb<T> _create<T, C>(dynamic parametrs, C context) where C : DbContext
        {
            Type
                typeContext = typeof(C),
                typeItem = typeof(T);

            var buff = typeContext.GetProperty($"{typeItem.Name}s");
            dynamic table = buff.GetValue(context);
            if(parametrs is List<T>)
                table.AddRange(parametrs);
            else
                table.Add(parametrs);
            context.SaveChanges();
            return new ResponseDb<T>(RequestType.Create, true);
        }
        public static ResponseDb<T> _read<T, C>(dynamic parametrs, C context) where C : DbContext
        {
            T? _mask = JsonSerializer.Deserialize<T>(parametrs.Item1);
            Type
                    typeContext = typeof(C),
                    typeItem = typeof(T);
            dynamic table = typeContext.GetProperty($"{typeof(T).Name}s");
            if (table != null)
            {
                IEnumerable<T> _table = table.GetValue(context);
                return new ResponseDb<T>(
                    RequestType.Read,
                    (
                        from item in _table
                        where ActionEntity.IsMask(_mask, item, parametrs.Item1)
                        select item)
                        .Skip<T>((int)parametrs.Item2 * (int)parametrs.Item3)
                        .Take<T>((int)parametrs.Item3)
                        .ToList());
            }
            else
            {
                throw new Exception("Not table");
            }
        }
        public static ResponseDb<T> _delete<T, C>(dynamic parametrs, C context) where C : DbContext
        {
            Type typeContext = typeof(C);
            dynamic table = typeContext.GetProperty($"{typeof(T).Name}s").GetValue(context);
            table.Remove(table.Find(parametrs));
            context.SaveChanges();
            return new ResponseDb<T>(RequestType.Delete, true);
        }
        public static ResponseDb<T> _update<T, C>(dynamic parametrs, C context) where C : DbContext
        {
            Type typeContext = typeof(C);
            dynamic table = typeContext.GetProperty($"{typeof(T).Name}s").GetValue(context);
            T origin = table.Find(parametrs.Item1.Id);
            T _in = origin;
            T _out = parametrs.Item1;
            string _mask = parametrs.Item2;
            ActionEntity.Update(ref _in, ref _out, _mask);
            context.SaveChanges();
            return new ResponseDb<T>(RequestType.Delete, true);
        }
        private static ResponseDb<T> decoratorForAction<T, C>(dynamic parametrs, Func<dynamic, C, ResponseDb<T>> callBechk) where C : DbContext, new()
        {
            try
            {
                ResponseDb<T> result;
                using (C context = new())
                {
                    result = callBechk(parametrs, context);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
                return new ResponseDb<T>(RequestType.Delete, false);
            }
        }
    }
}
