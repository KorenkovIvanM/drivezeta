﻿using System.Text.Json;
using AutoMap.ActionEntity;
using Models;
using ToolsEntity;

namespace Models
{
    public class ToolsModelDb
    {
        public static ResponseDb<T> Create<T>(T item) => ToolsModel.Create(item);
        public static ResponseDb<T> Create<T>(List<T> item) => ToolsModel.Create(item);
        public static ResponseDb<T> Read<T>(int numberPage = 0, int sizePage = 15) => ToolsModel.Read<T>(numberPage, sizePage).Upgrade();
        public static ResponseDb<T> Read<T>(string mask, int numberPage = 0, int sizePage = 15) => ToolsModel.Read<T>(mask, numberPage, sizePage).Upgrade();
        public static ResponseDb<T> Delete<T>(int Id) => ToolsModel.Delete<T>(Id);
        public static ResponseDb<T> Update<T>(T item, string? mask = null) => ToolsModel.Update(item, mask);
        public static ResponseDb<T> GetById<T>(int Id) => ToolsModel.GetById<T>(Id).Upgrade();
    }
}
