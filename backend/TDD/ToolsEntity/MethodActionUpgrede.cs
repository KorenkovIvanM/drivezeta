﻿using AutoMap.ActionEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolsEntity
{
    public class MethodActionUpgrede
    {
        public static dynamic Upgrede<T>(T item)
        {
            Type?
                type = typeof(T),
                _type = Type.GetType($"Models.ModelDb.{type.Name}Db", false, true);
            dynamic _item = (T)Activator.CreateInstance(_type);

            ActionEntity.AutoMappper(ref _item, ref item);
            return _item;
        }
        public static void Upgrede<T>(List<T> items)
        {
            for(int i = 0; i < items.Count; i++)
                items[i] = Upgrede(items[i]);
        }
    }
}
