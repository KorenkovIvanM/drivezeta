﻿namespace Models
{
    public class ResponseDb<T>
    {
        public string? ErrorInfo { get; }
        public List<T>? ResultRead { get; }
        public bool Result { get; }
        public RequestType RequestType { get; }
        public ResponseDb(RequestType type, string Error)
        {
            RequestType = type;
            ErrorInfo = Error;
        }
        public ResponseDb(RequestType type, List<T> result)
        {
            RequestType = type;
            ResultRead = result;
        }
        public ResponseDb(RequestType type, bool result)
        {
            RequestType = type;
            Result = result;
        }
        public ResponseDb<T> Upgrade()
        {
            MethodActionUpgrede.Upgrede(this.ResultRead);
            return this;
        }
    }

    public enum RequestType
    {
        Create,
        Update,
        Delete,
        Read,
    }
}
