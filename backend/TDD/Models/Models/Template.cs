﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Template
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public Order? Order { get; set; }
        public bool Activ { get; set; }
        public int Mask { get; set; } = 0;
        public DateTime Time { get; set; }
    }
}
