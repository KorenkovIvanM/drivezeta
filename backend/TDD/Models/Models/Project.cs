﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public string? Icon { get; set; } = "https://www.pinclipart.com/picdir/big/230-2302507_project-cycle-icon-clipart.png";
        public string? Color { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Stop { get; set; }
        public List<Target>? Targets { get; set; }
        public StatusProject Status { get; set; }
    }

    public enum StatusProject
    {
        Fine,
        UnderThreat,
        Overdue,
        Closed,
        Failed,
    }

}
