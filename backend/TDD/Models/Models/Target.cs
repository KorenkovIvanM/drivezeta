﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Target
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public int Value { get; set; } = 0;
        public int Max { get; set; } = 100;
        public List<Order>? Orders { get; set; }
    }
}
