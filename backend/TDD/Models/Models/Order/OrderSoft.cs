﻿using Models.ModelDb;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Models
{
    [Table("OrderSoft")]
    public class OrderSoft : OrderProjectDb<OrderSoft>
    {
        public int? Prioritet { get; set; }
    }
}
