﻿using System.ComponentModel.DataAnnotations.Schema;
using Models.ModelDb;

namespace Models.Models
{
    [Table("OrderWeek")]
    public class OrderWeek : OrderDb
    {
        public int? ParentId { get; set; }
        public List<OrderWeek>? Children { get; set; }
    }
}
