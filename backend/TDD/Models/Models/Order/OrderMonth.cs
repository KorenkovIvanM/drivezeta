﻿using Models.ModelDb;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Models
{
    [Table("OrderMonth")]
    public class OrderMonth : OrderDb
    {
        public int? ParentId { get; set; }
        public List<OrderMonth>? Children { get; set; }
    }
}
