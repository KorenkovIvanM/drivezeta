﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models.Interface;

namespace Models.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public DateTime Begin { get; set; } = DateTime.Now;
        public DateTime? End { get; set; }
        public StatusOrdr Status { get; set; } = StatusOrdr.Activ;
    }

    public enum StatusOrdr
    {
        NonActiv,
        Activ,
        Closed,
        Overdue,
        Filed,
    }

}
