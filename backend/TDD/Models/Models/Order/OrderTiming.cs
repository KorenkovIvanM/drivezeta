﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Models
{
    [Table("OrderTiming")]
    public class OrderTiming : Order
    {
        public int TimingId { get; set; }
        public Timing Category { get; set; }
    }
}
