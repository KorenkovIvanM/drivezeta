﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Models
{
    [Table("OrderKeyros")]
    public class OrderKeyros : Order
    {
        public int? ParentId { get; set; }
        public List<OrderKeyros>? Children { get; set; }
        public int? KeyrosId { get; set; }
        public Keyros? Keyros { get; set; } = null!;
    }
}
