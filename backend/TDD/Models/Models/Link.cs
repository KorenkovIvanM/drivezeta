﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Link
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public int OrderBein { get; set; }
        public int OrderEnd { get; set; }
    }
}
