﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Models.Interface
{
    public interface IParentChild
    {
        public int? ParentId { get; set; }
        public List<OrderWeek>? Children { get; set; }
    }
}
