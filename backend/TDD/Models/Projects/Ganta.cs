﻿using Models.ModelDb;
using Models.Models;

namespace Models.Projects
{
    public class Ganta
    {
        public List<OrderSoft> Orders { get; set; }
        public List<Link> Links { get; set; }
        public Ganta(int ProjectId)
        {
            var orders = ToolsModel.Read<OrderSoft>("{\"ProjectId\": " + ProjectId + " }", 0, int.MaxValue).ResultRead;
            MethodActionUpgrede.Upgrede(ref orders);
            Orders = orders;

            using(Context context = new())
                Links = (
                    from 
                        order in context.OrderSofts join 
                        link in context.Links 
                            on order.Id equals link.OrderBein
                    where 
                        order.ProjectId == ProjectId
                    select 
                        link).ToList();
        }

        public List<int> SortGanta()
        {
            var Orders = this.Orders.ToList();
            var Links = this.Links.ToList();
            var result = new List<int>();

            Dictionary<int, int> relationIdIndex = new();
            for (int i = 0; i < Orders.Count; i++)
                relationIdIndex[Orders[i].Id] = i;

            foreach (var order in Orders)
                order.Children = new();

            foreach (var link in Links)
                Orders[relationIdIndex[link.OrderEnd]]
                    .Children
                    .Add(Orders[relationIdIndex[link.OrderBein]]);

            while(Orders.Count > 0)
            {
                var idForBreak = 0;
                for(int i = 0; i < Orders.Count; i++)
                    if(Orders[i].Children.Count == 0)
                    {
                        idForBreak = Orders[i].Id;
                        break;
                    }
                result.Add(idForBreak);
                for (int i = 0; i < Orders.Count; i++)
                    Orders[i].Children = Orders[i].Children.FindAll((item) => item.Id != idForBreak);

                Orders = Orders.FindAll((item) => item.Id != idForBreak);
            }

            return result;
        }
    }
}
