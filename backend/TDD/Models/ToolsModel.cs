﻿using System.Text.Json;
using System.Reflection;
using AutoMap.ActionEntity;
using core = GenericToolsEntitys;
using Models.ModelDb.Interface;

namespace Models
{
    public static class ToolsModel
    {
        public static string
            MESSAGE_ERROR_JSON = "Not json",
            VOID_JSON = "{}";

        public static core.ResponseDb<T> Create<T>(T item) 
            => core.ToolsModel.Create<T, Context>(item);
        public static core.ResponseDb<T> Create<T>(List<T> item)
            => core.ToolsModel.Create<T, Context>(item);
        public static core.ResponseDb<T> Read<T>(int numberPage = 0, int sizePage = 15) 
            => core.ToolsModel.Read<T, Context>(numberPage, sizePage);
        public static core.ResponseDb<T> Read<T>(string mask, int numberPage = 0, int sizePage = 15) 
            => core.ToolsModel.Read<T, Context>(mask, numberPage, sizePage);
        public static core.ResponseDb<T> Read<T>(T mask, int numberPage = 0, int sizePage = 15)
            => core.ToolsModel.Read<T, Context>(mask, numberPage, sizePage);
        public static core.ResponseDb<T> Delete<T>(int Id)
            => core.ToolsModel.Delete<T, Context>(Id);
        public static core.ResponseDb<T> Update<T>(T item, string? mask = null) 
            => core.ToolsModel.Update<T, Context>(item, mask);
        public static core.ResponseDb<T> GetById<T>(int Id) 
            => core.ToolsModel.GetById<T, Context>(Id);
    }
}
