﻿using Microsoft.EntityFrameworkCore;
using Models.Models;

namespace Models
{
    public class Context : DbContext
    {
        public DbSet<Idea> Ideas { get; set; } = null!;
        public DbSet<Keyros> Keyross { get; set; } = null!;
        public DbSet<Project> Projects { get; set; } = null!;
        public DbSet<Target> Targets { get; set; } = null!;
        public DbSet<Template> Templates { get; set; } = null!;
        public DbSet<Timing> Timings { get; set; } = null!;
        public DbSet<Link> Links { get; set; } = null!;
        //  Order
        public DbSet<Order> Orders { get; set; } = null!;
        public DbSet<OrderHard> OrderHards { get; set; } = null!;
        public DbSet<OrderKeyros> OrderKeyross { get; set; } = null!;
        public DbSet<OrderMonth> OrderMonths { get; set; } = null!;
        public DbSet<OrderSoft> OrderSofts { get; set; } = null!;
        public DbSet<OrderTiming> OrderTimings { get; set; } = null!;
        public DbSet<OrderWeek> OrderWeeks { get; set; } = null!;
        public Context() => Database.EnsureCreated();
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("data source=DRIVEZETA.db");
        }

    }
}
