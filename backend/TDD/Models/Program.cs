﻿using Models;
using Models.Models;
using Models.ModelDb;
using core = GenericToolsEntitys;
using Models.Projects;

var old_count_month = ToolsModel.Read<OrderMonth>().ResultRead.Count;
var old_count_week = ToolsModel.Read<OrderWeek>().ResultRead.Count;


var order_month = new OrderMonthDb()
{
    Title = "1234",
};
ToolsModelDb.Create(order_month);

var miid_count_month = ToolsModel.Read<OrderMonth>().ResultRead.Count;
var miid_count_week = ToolsModel.Read<OrderWeek>().ResultRead.Count;

order_month.Push();

var new_count_month = ToolsModel.Read<OrderMonth>().ResultRead.Count;
var new_count_week = ToolsModel.Read<OrderWeek>().ResultRead.Count;

Console.WriteLine(old_count_month + 1 == miid_count_month);
Console.WriteLine(old_count_week == miid_count_week);
Console.WriteLine(miid_count_month - 1 == new_count_month);
Console.WriteLine(miid_count_week + 1 == new_count_week);
