﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb.Interface
{
    public interface IParentChildren<T>
    {
        int? ParentId { get; set; }
        List<T>? Children { get; set; }
        //void CreateTree(ref IEnumerable<T> list);
    }
}
