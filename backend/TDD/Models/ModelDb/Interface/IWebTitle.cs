﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb.Interface
{
    public interface IWebTitle
    {
        public string Title { get; set; }
        public string? Description { get; set; }
    }
}
