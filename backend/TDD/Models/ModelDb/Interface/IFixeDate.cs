﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb.Interface
{
    public interface IFixeDate
    {
        public DateTime? Start { get; set; }
        public DateTime? Stop { get; set; }
    }
}
