﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb.Interface
{
    public interface ITarget
    {
        public int? Weight { get; set; }
        public int? Progress { get; set; }
        public int? TargetId { get; set; }
    }
}
