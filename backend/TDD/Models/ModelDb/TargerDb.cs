﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models;
using Models.ModelDb.Interface;

namespace Models.ModelDb
{
    public class TargerDb : Target, IWebTitle
    {
        public TargerDb(string Title, string? Description, int Max)
        {
            this.Title = Title;
            this.Description = Description;
            this.Max = Max;
            this.Value = 0;
        }
        public TargerDb(string Title, int Max)
            : this(Title, null, Max) { }
        public TargerDb(string Title)
            : this(Title, 100) { }
        public TargerDb()
            : this("Not a title") { }

        public delegate void ForClose();
        public event ForClose OnClose;

        public int Value
        {
            get 
            { 
                return base.Value; 
            }
            set 
            { 
                if(value >= Max && Value != Max)
                {
                    base.Value = Max;
                    OnClose?.Invoke();
                }
                else
                    base.Value = value; 
            }
        }
    }
}
