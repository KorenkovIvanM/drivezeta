﻿using Models.Models;
using Models.ModelDb.Interface;
using GenericToolsEntitys;

namespace Models.ModelDb
{
    public abstract class OrderProjectDb<T> : OrderDb, IParentChildren<T> where T : Order, IParentChildren<T>
    {
        public int? ProjectId { get; set; }
        public Project? Project { get; set; }
        public DateTime? Stop { get; set; }
        public int? Weight { get; set; }
        public int? Progress { get; set; }
        public int? TargetId { get; set; }
        public Target? Target { get; set; }
        public int? ParentId { get; set; }
        public List<T>? Children { get; set; }

        #region Constractor
        public OrderProjectDb(string Title, string? Description, StatusOrdr Status, DateTime Start, DateTime? Stop)
        {
            this.Title = Title;
            this.Description = Description;
            Begin = DateTime.Now;
            this.Status = Status;
            this.Start = Start;
            this.Stop = Stop ?? Start.AddHours(2);
            Close += writeTarget;
        }
        private void writeTarget()
        {
            if (Target != null)
            {
                Target.Value += Weight ?? 0;
                ToolsModel.Update(Target);
            }

        }
        public OrderProjectDb(string Title, StatusOrdr Status, DateTime Start, DateTime? Stop)
            : this(Title, null, Status, Start, Stop) { }
        public OrderProjectDb(string Title, StatusOrdr Status, DateTime Start)
            : this(Title, Status, Start, null) { }
        public OrderProjectDb(string Title, StatusOrdr Status)
            : this(Title, Status, DateTime.Now) { }
        public OrderProjectDb(string Title)
            : this(Title, StatusOrdr.Activ) { }
        public OrderProjectDb()
            : this("Not a Order") { }
        #endregion

        public delegate void MoveTime(DateTime? oldTime, DateTime? newTime);
        public event MoveTime Update;
        private DateTime? start;
        public DateTime? Start
        {
            get
            {
                return start;
            }
            set
            {
                Update?.Invoke(start, value);
                start = value;
            }
        }
        public static void CreateTree(ref List<T> list)
        {
            Dictionary<int, T> buff = new();

            foreach (var item in list)
                buff[item.Id] = item;

            foreach (var item in buff)
                if (item.Value.ParentId != null && (int)item.Value.ParentId != 0)
                {
                    if (buff[(int)item.Value.ParentId].Children == null)
                        buff[(int)item.Value.ParentId].Children = new();
                    buff[(int)item.Value.ParentId].Children.Add(item.Value);
                }
            list = list.Where(item => item.ParentId == null || item.ParentId == 0).ToList();

        }

        public void DeleteChildren()
        {
            // TODO you addation this function in the interface IParentChildren
            var buff = ToolsModel.Read<T>("{\"ParentId\":" + Id + "}", 0, int.MaxValue);
            foreach(var item in buff.ResultRead)
                ToolsModel.Delete<T>(item.Id);
        }
    }
}
