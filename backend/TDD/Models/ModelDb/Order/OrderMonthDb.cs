﻿using AutoMap.ActionEntity;
using Models.ModelDb.Interface;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb
{
    public class OrderMonthDb : OrderMonth, IWebTitle, IParentChildren<OrderMonth>
    {
        public void CreateTree(ref IEnumerable<OrderMonth> list)
        {
            throw new NotImplementedException();
        }

        public void Push()
        {
            OrderWeekDb order = new OrderWeekDb();
            var buff = this;
            ActionEntity.AutoMappper(ref order, ref buff);
            order.Id = 0;
            ToolsModelDb.Create(order);
            ToolsModelDb.Delete<OrderMonthDb>(this.Id);
        }
    }
}
