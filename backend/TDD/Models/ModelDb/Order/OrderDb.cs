﻿using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb
{
    public class OrderDb: Order
    {
        protected OrderDb(string Title, string? Description, StatusOrdr Status)
        {
            this.Title = Title;
            this.Description = Description;
            Begin = DateTime.Now;
            this.Status = Status;
        }
        protected OrderDb(string Title, string? Description)
            :this(Title, Description, StatusOrdr.Activ) { }
        protected OrderDb(string Title)
            : this(Title, null) { }
        protected OrderDb()
            : this("Title not have") { }

        public delegate void ClosedOrder();
        public event ClosedOrder Close;

        public StatusOrdr Status 
        { 
            get
            {
                return base.Status;
            }
            set
            {
                if(value == StatusOrdr.Closed)
                {
                    base.Status = value;
                    End= DateTime.Now;
                    Close?.Invoke();
                }
            } 
        }
    }
}
