﻿using Models.ModelDb.Interface;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb
{
    public class OrderHardDb : OrderHard, IFixeDate, ITarget, IWebTitle
    {
        #region Constractor
        public OrderHardDb(string Title, string? Description, StatusOrdr Status, DateTime Start, DateTime? Stop)
        {
            this.Title = Title;
            this.Description = Description;
            Begin = DateTime.Now;
            this.Status = Status;
            this.Start = Start;
            this.Stop = Stop ?? Start.AddHours(2);
            Close += writeTarget;
        }

        private void writeTarget()
        {
            if (Target != null)
            {
                Target.Value += this.Weight ?? 0;
                ToolsModel.Update(Target);
            }

        }
        public OrderHardDb(string Title, StatusOrdr Status, DateTime Start, DateTime? Stop)
            : this(Title, null, Status, Start, Stop) { }
        public OrderHardDb(string Title, StatusOrdr Status, DateTime Start)
            : this(Title, Status, Start, null) { }
        public OrderHardDb(string Title, StatusOrdr Status)
            : this(Title, Status, DateTime.Now) { }
        public OrderHardDb(string Title)
            : this(Title, StatusOrdr.Activ) { }
        public OrderHardDb()
            : this("Not a Order") { }
        #endregion

        //public delegate void MoveTime(DateTime? oldTime, DateTime? newTime);
        //public event MoveTime Update;
        //public new DateTime? Start 
        //{
        //    get 
        //    {
        //        return base.Start;
        //    }
        //    set
        //    {
        //        Update?.Invoke(base.Start, value);
        //        base.Start = value;
        //    }
        //}
    }
}
