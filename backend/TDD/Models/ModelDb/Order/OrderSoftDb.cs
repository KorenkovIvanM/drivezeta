﻿using Models.ModelDb.Interface;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb
{
    public class OrderSoftDb : OrderSoft, IFixeDate, ITarget, IWebTitle//, IParentChildren<OrderSoft>
    {
        #region Constractor
        public OrderSoftDb(string Title, string? Description, StatusOrdr Status, DateTime Start, DateTime? Stop)
        {
            this.Title = Title;
            this.Description = Description;
            Begin = DateTime.Now;
            this.Status = Status;
            this.Start = Start;
            this.Stop = Stop ?? Start.AddHours(2);
            Close += writeTarget;
        }

        private void writeTarget()
        {
            if (Target != null)
            {
                Target.Value += Weight ?? 0;
                ToolsModel.Update(Target);
            }

        }

        public OrderSoftDb(string Title, StatusOrdr Status, DateTime Start, DateTime? Stop)
            : this(Title, null, Status, Start, Stop) { }
        public OrderSoftDb(string Title, StatusOrdr Status, DateTime Start)
            : this(Title, Status, Start, null) { }
        public OrderSoftDb(string Title, StatusOrdr Status)
            : this(Title, Status, DateTime.Now) { }
        public OrderSoftDb(string Title)
            : this(Title, StatusOrdr.Activ) { }
        public OrderSoftDb()
            : this("Not a Order") { }
        #endregion

        //public delegate void MoveTime(DateTime? oldTime, DateTime? newTime);
        //public event MoveTime Update;
        //public DateTime? Start
        //{
        //    get
        //    {
        //        return base.Start;
        //    }
        //    set
        //    {
        //        Update?.Invoke(base.Start, value);
        //        base.Start = value;
        //    }
        //}
    }
}
