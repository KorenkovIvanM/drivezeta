﻿using AutoMap.ActionEntity;
using Models.ModelDb.Interface;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ModelDb
{
    public class OrderWeekDb : OrderWeek, IWebTitle, IParentChildren<OrderWeek>
    {
        public void CreateTree(ref IEnumerable<OrderWeek> list)
        {
            throw new NotImplementedException();
        }

        public void Push()
        {
            OrderSoftDb order = new OrderSoftDb();
            var buff = this;
            ActionEntity.AutoMappper(ref order, ref buff);
            order.Id = 0;
            ToolsModelDb.Create(order);
            ToolsModelDb.Delete<OrderWeekDb>(Id);
        }
    }
}
