﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.ModelDb.Interface;
using Models.Models;

namespace Models.ModelDb
{
    public class IdeaDb : Idea, IWebTitle
    {
        public IdeaDb() { }
        public IdeaDb(string Title = null, string? Description = null) 
        { 
            this.Title = Title;
            this.Description = Description;
        }
    }
}
