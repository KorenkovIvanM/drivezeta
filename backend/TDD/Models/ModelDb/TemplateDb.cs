﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models;

namespace Models.ModelDb
{
    public class TemplateDb : Template
    {
        public TemplateDb(int OrderId, bool Activ, int Mask, DateTime Time)
        {
            this.OrderId = OrderId;
            this.Activ = Activ;
            this.Mask = Mask;
            this.Time = Time;
        }
        public TemplateDb(int OrderId)
            : this(OrderId, false, 0, DateTime.Now) { }
        public bool CreateOrder()
        {
            var buff = ToolsModel.GetById<OrderHard>(OrderId).ResultRead[0];
            buff.Id = 0;
            return ToolsModel.Create(buff).Result;
        }
    }
}
