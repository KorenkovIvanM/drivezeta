﻿using TDD.Controllers;
using MakeWork.ModelDb;
using MakeWork.Models;
using MakeWork;
using System.Text;

namespace FOR_TEST
{
    internal class Class_for_test
    {
        public HandlerstateMachineController _handlerStateMachine = new();
        public StateMachineController _stateMachine = new();
        public ToolsWorkerController _tools = new();
        public ArgumentController _argument = new();
        public AnswerController _answer = new();

        const string
            FILE_NAME = "test.py",
            FILE_PATH = "D:\\Drivezeta\\Resource\\",
            TITLE = "_for_test_python_",
            TITLE2 = "_for_test_python_2",
            TITLE3 = "_for_test_python_3",
            TITLE4 = "_for_test_python_4",
            TITLE_START = "Start My test",
            ARG_1 = "123",
            ARG_2 = "945",
            RESULT = "result.txt";

        StateMachine
            _python,
            _statr;
        HandlerStateMachine
            _handler;
        Argument
            _item_1,
            _item_2;

        internal void Run()
        {
            _python = new StateMachine
            {
                Title = TITLE2,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);

            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Form,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            _tools.StartWorker(_statr.Id, "a=123 b=199");
            for (int i = 0; i < int.MaxValue; i++) { }

            FileInfo file = new FileInfo(RESULT);

            Console.Write(file.Exists);
            int result = 0;
            using (FileStream fstream = File.OpenRead(RESULT))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.Read(buffer, 0, buffer.Length);
                string textFromFile = Encoding.Default.GetString(buffer);
                result = Convert.ToInt32(textFromFile);
            }
            Console.Write(result == 322);
        }
    }
}
