﻿using System.Reflection;
using System.Text.Json;
using AutoMap.ActionEntity.Settings;

namespace AutoMap.ActionEntity
{
    public static class ActionEntity
    {
        private static string
            NAME_ID = "Id",
            NAME_LITTLE_ID = "id",
            MESSAGE_ERROR = "Id not int",
            MESSAGE_ERROR_NOT_ID = "Not Id",
            MESSAGE_ERROR_FOR_NOT_JSON = "Not JSON";

        /// <summary>
        /// The method returns an ID for the specified entity.
        /// </summary>
        /// <typeparam name="T">Type entity</typeparam>
        /// <param name="item">Exapmple</param>
        /// <returns>(T)Id</returns>
        /// <exception cref="Exception">The item have not in DataBase</exception>
        public static int? GetId<T>(T item)
        {
            Type 
                type = typeof(T);
            dynamic 
                field = typeof(T).GetProperty(NAME_ID);

            if (field == null) field = type.GetProperty(NAME_LITTLE_ID);
            if (field == null) field = type.GetField(NAME_ID);
            if (field == null) field = type.GetField(NAME_LITTLE_ID);

            if (field == null) throw new Exception(MESSAGE_ERROR_NOT_ID);


            object fieldValue = field.GetValue(item);

            if (fieldValue is int?)
                return (int?)fieldValue;
            else
                throw new Exception(MESSAGE_ERROR);
        }

        /// <summary>
        /// Iten is default or not
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>Iten is default or not</returns>
        private static bool s_IsDefault(dynamic item)
        {
            if (item is DateTime)
                return (DateTime)item == new DateTime();
            return false;
        }

        /// <summary>
        /// Update data for onr entity
        /// </summary>
        /// <typeparam name="T">entity</typeparam>
        /// <param name="inItev">in iem</param>
        /// <param name="outItem">out item</param>
        /// <param name="json">json for in item</param>
        /// <exception cref="Exception">if "json" not json</exception>
        public static void Update<T>(ref T inItev, ref T outItem, string? json = null)
            => AutoMappper(ref inItev, ref outItem, json);


        /// <summary>
        /// copy AutoMaper
        /// </summary>
        /// <typeparam name="Tin">Type where script set data</typeparam>
        /// <typeparam name="Tout">Type where script get data</typeparam>
        /// <param name="inItev">Item in</param>
        /// <param name="outItem">Item Out</param>
        /// <param name="json">Json for out item</param>
        /// <exception cref="Exception">For error json serializable</exception>
        public static void AutoMappper<Tin, Tout>(ref Tin inItev, ref Tout outItem, string? json = null)
        {

            if (json == null)
            {
                s_updating(inItev, outItem, json, (PropertyInfo prop, object inItev) => !s_IsDefault(prop.GetValue((Tin)inItev)));// this
            }
            else
            {
                try
                {
                    Tout? _mask = JsonSerializer.Deserialize<Tout>(json);
                    if (_mask == null)
                        return;
                }
                catch (Exception ex)
                {
                    throw new Exception(MESSAGE_ERROR_FOR_NOT_JSON);
                }
                s_updating(inItev, outItem, json, (PropertyInfo prop, object json) => ((string)json).ToUpper().IndexOf($"\"{prop.Name.ToUpper()}\":") != -1);
            }
        }

        private static void s_updating<Tin, Tout>(Tin inItev, Tout outItem, string json, Func<PropertyInfo, dynamic, bool> IsChooses)
        {
            Type
                typeIn = inItev.GetType(),
                typeOut = outItem.GetType();
            foreach (PropertyInfo out_prop in typeOut.GetProperties())
            {
                PropertyInfo in_prop = typeIn.GetProperty(out_prop.Name);
                if(
                    in_prop != null &&
                    out_prop.GetValue(outItem) != null &&
                    IsChooses(in_prop, json == null ? inItev : json))
                {
                    in_prop.SetValue(inItev, out_prop.GetValue(outItem));
                }
                        
            }
                
        }

        /// <summary>
        /// IsMask for sql select
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Example</param>
        /// <param name="json">Json</param>
        /// <returns>is valied & ttrue : false</returns>
        /// <exception cref="Exception"></exception>
        public static bool IsMask<T>(T mask, T example, string? json = null)
        {
            Type
                type = typeof(T);
            bool 
                result = true;
            TypeSQLWhereCommand
                typeSQLWhereCommand = TypeSQLWhereCommand.NonAtribyte;
            TypeIncludePeriod
                typeIncludePeriod = TypeIncludePeriod.Day;

            try
            {
                if (json != null)
                {
                    T? _mask = JsonSerializer.Deserialize<T>(json);
                    if (_mask == null)
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MESSAGE_ERROR_FOR_NOT_JSON);
            }

            foreach (PropertyInfo prop in type.GetProperties())
            {
                typeSQLWhereCommand = s_GetSQLAction(typeSQLWhereCommand, prop);
                if (typeSQLWhereCommand == TypeSQLWhereCommand.Ignore) continue;
                if (typeSQLWhereCommand == TypeSQLWhereCommand.Date)
                    typeIncludePeriod = s_GetTypeDate(prop);
                result = result && s_masking(mask, example, result, typeSQLWhereCommand, typeIncludePeriod, prop, json,
                    json == null ? null : (string json, PropertyInfo prop) => json.ToUpper().IndexOf($"\"{prop.Name.ToUpper()}\":") == -1);
            }
            return result;
        }

        /// <summary>
        /// Maping for is mask
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Example</param>
        /// <param name="result">Result for buffer</param>
        /// <param name="typeSQLWhereCommand">typeSQLWhereCommand</param>
        /// <param name="typeIncludePeriod">typeIncludePeriod</param>
        /// <param name="prop">Property</param>
        /// <param name="json">Json</param>
        /// <param name="callBeak">Callback for json</param>
        /// <returns></returns>
        private static bool s_masking<T>(
            T mask, 
            T example, 
            bool result, 
            TypeSQLWhereCommand typeSQLWhereCommand, 
            TypeIncludePeriod typeIncludePeriod, 
            PropertyInfo prop, 
            string? json = null,
            Func<string, PropertyInfo, bool>? callBeak = null)
        {
            if (callBeak != null && json != null && callBeak((string)json, prop))
                return true;
            if (prop.GetValue(mask) != null && prop.GetValue(example) == null)
                result = result && false;
            else if (prop.GetValue(mask) != null)
                if (prop.GetValue(mask) is int && s_IsSQLAttribyte(mask, example, typeSQLWhereCommand, prop, s_IsEquals))
                    result = result && false;
                else if (prop.GetValue(mask) is string && s_IsSQLAttribyte(mask, example, typeSQLWhereCommand, prop, s_IsLike))
                    result = result && false;
                else if (prop.GetValue(mask) is Enum && s_IsSQLAttribyte(mask, example, typeSQLWhereCommand, prop, s_IsEquals))
                    result = result && false;
                else if (prop.GetValue(mask) is DateTime && s_isSQLDate(mask, example, prop, typeIncludePeriod))
                    result = result && false;
            return result;
        }

        /// <summary>
        /// Is date in period
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Example</param>
        /// <param name="prop">Priprit</param>
        /// <param name="typeIncludePeriod">typeIncludePeriod</param>
        /// <returns>Date in Interval ? true : false </returns>
        private static bool s_isSQLDate<T>(
            T mask,
            T example,
            PropertyInfo prop,
            TypeIncludePeriod typeIncludePeriod)
        {
            DateTime
                _mask = (DateTime)prop.GetValue(mask),
                _example = (DateTime)prop.GetValue(example);
            return s_IsDate(typeIncludePeriod, _mask, _example);
        }

        /// <summary>
        /// Mask and example together one period
        /// </summary>
        /// <param name="typeIncludePeriod">typeIncludePeriod</param>
        /// <param name="_mask">Mask</param>
        /// <param name="_example">Example</param>
        /// <returns>one period ? true : else </returns>
        private static bool s_IsDate(TypeIncludePeriod typeIncludePeriod, DateTime _mask, DateTime _example)
        {
            return (
                typeIncludePeriod == TypeIncludePeriod.Year
                        && (_mask.Year != _example.Year))
                    || (typeIncludePeriod == TypeIncludePeriod.Month
                        && (
                        _mask.Year != _example.Year ||
                        _mask.Month != _example.Month))
                    || (typeIncludePeriod == TypeIncludePeriod.Day
                        && (
                        _mask.Year != _example.Year ||
                        _mask.Month != _example.Month ||
                        _mask.Day != _example.Day))
                    || (typeIncludePeriod == TypeIncludePeriod.House
                        && (
                        _mask.Year != _example.Year ||
                        _mask.Month != _example.Month ||
                        _mask.Day != _example.Day ||
                        _mask.Hour != _example.Hour))
                    || (typeIncludePeriod == TypeIncludePeriod.Week
                        && (
                        _example.Day - _mask.Day < 6 - _mask.Day));
        }


        /// <summary>
        /// Get type include perriod
        /// </summary>
        /// <param name="prop">property</param>
        /// <returns>TypeIncludePeriod</returns>
        private static TypeIncludePeriod s_GetTypeDate(PropertyInfo prop)
        {
            TypeIncludePeriod typeIncludePeriod;
            var buff = prop.GetCustomAttribute(typeof(SQLIncludePeriodAttribute));
            var _buff = (SQLIncludePeriodAttribute)buff;
            typeIncludePeriod = _buff.typeIncludePeriod;
            return typeIncludePeriod;
        }

        /// <summary>
        /// Get SQL action for property
        /// </summary>
        /// <param name="typeSQLWhereCommand">type propery</param>
        /// <param name="prop">Property</param>
        /// <returns>Type Property</returns>
        private static TypeSQLWhereCommand s_GetSQLAction(TypeSQLWhereCommand typeSQLWhereCommand, PropertyInfo prop)
        {
            if (prop.GetCustomAttribute(typeof(SQLNotAttribute)) != null) typeSQLWhereCommand = TypeSQLWhereCommand.NonAtribyte;
            else if (prop.GetCustomAttribute(typeof(SQLLikeAttribute)) != null) typeSQLWhereCommand = TypeSQLWhereCommand.Like;
            else if (prop.GetCustomAttribute(typeof(SQLEqualesAttribute)) != null) typeSQLWhereCommand = TypeSQLWhereCommand.Equals;
            else if (prop.GetCustomAttribute(typeof(SQLIgnoreAttribute)) != null) typeSQLWhereCommand = TypeSQLWhereCommand.Ignore;
            else if (prop.GetCustomAttribute(typeof(SQLIncludePeriodAttribute)) != null) typeSQLWhereCommand = TypeSQLWhereCommand.Date;
            return typeSQLWhereCommand;
        }

        /// <summary>
        /// function for serch value all attribytes
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Examle</param>
        /// <param name="typeSQLWhereCommand">Action</param>
        /// <param name="prop">Property</param>
        /// <param name="defaultAction">Function default value</param>
        /// <returns></returns>
        private static bool s_IsSQLAttribyte<T>(
            T mask, 
            T example, 
            TypeSQLWhereCommand typeSQLWhereCommand, 
            PropertyInfo prop, 
            Func<T, T, PropertyInfo, bool> defaultAction)
        {
            return 
                typeSQLWhereCommand == TypeSQLWhereCommand.NonAtribyte ? defaultAction(mask, example, prop) : false ||
                typeSQLWhereCommand == TypeSQLWhereCommand.Like ? s_IsLike(mask, example, prop) : false ||
                typeSQLWhereCommand == TypeSQLWhereCommand.Equals ? s_IsEquals(mask, example, prop) : false;
        }

        /// <summary>
        /// Function for like two entitys
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Examlie</param>
        /// <param name="prop">Property</param>
        /// <returns>like ? true : false</returns>
        private static bool s_IsEquals<T>(T mask, T example, PropertyInfo prop)
        {
            return prop.GetValue(mask).ToString() != prop.GetValue(example).ToString();
        }

        /// <summary>
        /// Function for equality two entitys
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="mask">Mask</param>
        /// <param name="example">Examlie</param>
        /// <param name="prop">Property</param>
        /// <returns>equality ? true : false</returns>
        private static bool s_IsLike<T>(T mask, T example, PropertyInfo prop)
        {
            return prop.GetValue(example).ToString().IndexOf(prop.GetValue(mask).ToString()) == -1;
        }
    }

    public enum TypeSQLWhereCommand
    {
        NonAtribyte,
        Like,
        Equals,
        Ignore,
        Date
    }
}
