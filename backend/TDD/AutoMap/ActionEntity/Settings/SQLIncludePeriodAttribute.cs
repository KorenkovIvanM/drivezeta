﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMap.ActionEntity.Settings
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class SQLIncludePeriodAttribute : Attribute
    {
        public TypeIncludePeriod typeIncludePeriod;
        public SQLIncludePeriodAttribute(TypeIncludePeriod typeIncludePeriod)
        {
            this.typeIncludePeriod = typeIncludePeriod;
        }
    }

    public enum TypeIncludePeriod
    {
        Year,
        Month,
        Day,
        Week,
        House,
    }
}
