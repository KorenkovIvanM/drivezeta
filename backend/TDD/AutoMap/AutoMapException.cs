﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMap
{
    public class AutoMapException: Exception
    {
        public AutoMapException(string message)
        : base(message) { }
    }
}
