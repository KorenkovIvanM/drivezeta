﻿using System.Text.Json;
using System.Reflection;
using AutoMap.ActionEntity;
using Microsoft.EntityFrameworkCore;

namespace GenericToolsEntitys
{
    public static class ToolsModel
    {
        private const string
            MESSAGE_ERROR_JSON = "Not json",
            VOID_JSON = "{}";

        public static ResponseDb<T> Create<T, C>(T item) where C : DbContext, new() => 
            s_decoratorForAction<T, C>(item, s_create<T, C>);
        public static ResponseDb<T> Create<T, C>(List<T> item) where C : DbContext, new() => 
            s_decoratorForAction<T, C>(item, s_create<T, C>);
        public static ResponseDb<T> Read<T, C>(int numberPage = 0, int sizePage = 15) where C : DbContext, new() 
            => Read<T, C>(VOID_JSON, numberPage, sizePage);
        public static ResponseDb<T> Read<T, C>(string mask, int numberPage = 0, int sizePage = 15) where C : DbContext, new() => 
            s_decoratorForAction<T, C>((mask, numberPage, sizePage), s_read<T, C>);
        public static ResponseDb<T> Read<T, C>(T mask, int numberPage = 0, int sizePage = 15) where C : DbContext, new() =>
            s_decoratorForAction<T, C>((mask, numberPage, sizePage), s_read<T, C>);
        public static ResponseDb<T> Delete<T, C>(int Id) where C : DbContext, new() => 
            s_decoratorForAction<T, C>(Id, s_delete<T, C>);
        public static ResponseDb<T> Update<T, C>(T item, string? mask = null) where C : DbContext, new() =>
            s_decoratorForAction<T, C>((item, mask), s_update<T, C>);
        public static ResponseDb<T> GetById<T, C>(int Id) where C : DbContext, new() =>
            s_decoratorForAction<T, C>(("{\"Id\": " + Id + "}", 0, 1), s_read<T, C>);
        private static ResponseDb<T> s_create<T, C>(dynamic parametrs, C context) where C : DbContext, new()
        {
            Type
                typeContext = typeof(C),
                typeItem = typeof(T);
            PropertyInfo? buff = s_GetNameModels(typeContext, typeItem);
            dynamic table = buff.GetValue(context);
            if (parametrs is List<T>)
                table.AddRange(parametrs);
            else
                table.Add(parametrs);
            context.SaveChanges();
            return new ResponseDb<T>(true);
        }
        private static PropertyInfo? s_GetNameModels(Type typeContext, Type typeItem)
        {
            if (typeItem.Name.IndexOf("Db") == -1)
                return typeContext.GetProperty($"{typeItem.Name}s");
            else
                return typeContext.GetProperty($"{typeItem.Name.Replace("Db", "")}s");
        }
        private static ResponseDb<T> s_read<T, C>(dynamic parametrs, C context)
        {
            T? _mask = JsonSerializer.Deserialize<T>(parametrs.Item1);
            if (parametrs.Item1 is string)
                _mask = JsonSerializer.Deserialize<T>(parametrs.Item1);
            else if(parametrs.Item1 is T)
                _mask = parametrs.Item1;
            else
                _mask = JsonSerializer.Deserialize<T>(parametrs.Item1);
            Type
                    typeContext = typeof(C),
                    typeItem = typeof(T);
            dynamic table = s_GetNameModels(typeContext, typeItem);
            if (table != null)
            {
                IEnumerable<T> _table = table.GetValue(context);
                var buff = new ResponseDb<T>(
                    (
                        from item in _table
                        where ActionEntity.IsMask(_mask, item, parametrs.Item1)
                        select item)
                        .Skip((int)parametrs.Item2 * (int)parametrs.Item3)
                        .Take((int)parametrs.Item3)
                        .ToList());
                return buff;
            }
            else
            {
                throw new Exception("Not table");
            }
        }
        private static ResponseDb<T> s_delete<T, C>(dynamic parametrs, C context) where C : DbContext, new()
        {
            Type typeContext = typeof(C);
            dynamic table = s_GetNameModels(typeContext, typeof(T)).GetValue(context);
            table.Remove(table.Find(parametrs));
            context.SaveChanges();
            return new ResponseDb<T>(true);
        }
        private static ResponseDb<T> s_update<T, C>(dynamic parametrs, C context) where C : DbContext, new()
        {
            Type typeContext = typeof(C);
            dynamic table = s_GetNameModels(typeContext, typeof(T)).GetValue(context);
            T origin = table.Find(parametrs.Item1.Id);
            T _in = origin;
            T _out = parametrs.Item1;
            string _mask = parametrs.Item2;
            ActionEntity.Update(ref _in, ref _out, _mask);
            context.SaveChanges();
            return new ResponseDb<T>(true);
        }
        private static ResponseDb<T> s_decoratorForAction<T, C>(dynamic parametrs, Func<dynamic, C, ResponseDb<T>> callBechk) where C : DbContext, new()
        {
            try
            {
                ResponseDb<T> result;
                using (C context = new())
                {
                    result = callBechk(parametrs, context);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                    Console.WriteLine(ex.InnerException.Message);
                return new ResponseDb<T>(false);
            }
        }
    }
}
