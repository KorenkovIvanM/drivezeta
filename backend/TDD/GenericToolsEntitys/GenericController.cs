﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace GenericToolsEntitys
{
    [ApiController]
    [Route("[controller]")]
    public class GenericController<T, C> : ControllerBase where C : DbContext, new()
    {
        #region GetById
        public delegate void PredGetById(ref int Id);
        public event PredGetById OnPredGetById;
        public delegate void PostGetById(ref T? item);
        public event PostGetById OnPostGetById;
        [HttpGet("Id")]
        public virtual T? GetById(int Id)
        {
            OnPredGetById?.Invoke(ref Id);
            var result = 
                ToolsModel
                    .GetById<T, C>(Id)
                    .ResultRead
                    .Last();
            OnPostGetById?.Invoke(ref result);
            return result;
        }
        #endregion
        #region Get
        public delegate void PredGet(ref string mask);
        public event PredGet OnPredGet;
        public delegate void PostGet(ref List<T>? list);
        public event PostGet OnPostGet;
        [HttpGet]
        public virtual List<T>? Get(string mask = "{}", int numberPage = 0, int sizePage = 15)
        {
            OnPredGet?.Invoke(ref mask);
            var result = ToolsModel.Read<T, C>(mask, numberPage, sizePage).ResultRead;
            OnPostGet?.Invoke(ref result);
            return result;
        }
        #endregion
        #region Post
        public delegate void PredPost(ref T item);
        public event PredPost OnPredPost;
        public delegate void PostPost(ref bool result);
        public event PostPost OnPostPost;
        [HttpPost]
        public virtual T Post(T item)
        {
            OnPredPost?.Invoke(ref item);
            var result = ToolsModel.Create<T, C>(item).Result;
            OnPostPost?.Invoke(ref result);
            return item;
        }
        #endregion
        #region Put
        public delegate void PredPut(ref T? mask);
        public event PredPut OnPredPut;
        public delegate void PostPut(ref bool result);
        public event PostPut OnPostPut;

        [HttpPut]
        public virtual bool Put(string mask)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };
            T? _mask = JsonSerializer.Deserialize<T>(mask, options);
            OnPredPut?.Invoke(ref _mask);
            var result = ToolsModel.Update<T, C>(_mask, mask).Result;
            OnPostPut?.Invoke(ref result);
            return result;
        }
        #endregion
        #region Delete
        public delegate void PredDelete(ref int id);
        public event PredDelete OnPredDelete;
        public delegate void PostDelete(ref bool result);
        public event PostDelete OnPostDelete;
        [HttpDelete]
        public virtual bool Delete(int id)
        {
            OnPredDelete?.Invoke(ref id);
            var result = ToolsModel.Delete<T, C>(id).Result;
            OnPostDelete?.Invoke(ref result);
            return result;
        }
        #endregion
    }
}
