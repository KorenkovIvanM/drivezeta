﻿namespace GenericToolsEntitys
{
    public class ResponseDb<T>
    {
        public string? ErrorInfo { get; }
        public List<T>? ResultRead { get; }
        public bool Result { get; }
        public ResponseDb(string Error) => ErrorInfo = Error;
        public ResponseDb(List<T> result) => ResultRead = result;
        public ResponseDb(bool result) => Result = result;
        public ResponseDb<T> Upgrade()
        {
            MethodActionUpgrede.Upgrede(ResultRead);
            return this;
        }
    }
}
