﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using MakeWork.Models;
using System.IO;

namespace TDD.Controllers.Tests
{
    [TestClass()]
    public class ToolsWorkerControllerTests
    {
        public HandlerstateMachineController _handlerStateMachine = new();
        public StateMachineController _stateMachine = new();
        public ToolsWorkerController _tools = new();
        public ArgumentController _argument = new();
        public AnswerController _answer = new();

        const string
            FILE_NAME = "test.py",
            FILE_PATH = "D:\\Drivezeta\\Resource\\",
            TITLE = "_for_test_python_",
            TITLE2 = "_for_test_python_2",
            TITLE3 = "_for_test_python_3",
            TITLE4 = "_for_test_python_4",
            TITLE_START = "Start My test",
            ARG_1 = "123",
            ARG_2 = "945",
            RESULT = "result.txt";

        StateMachine 
            _python, 
            _statr;
        HandlerStateMachine 
            _handler;
        Argument 
            _item_1, 
            _item_2;

        [TestMethod()]
        public void Create_State_Machine()
        {
            _python = new StateMachine 
            { 
                Title = TITLE, 
                Type = StateMachineType.Python 
            };
            _stateMachine.Post(_python);
            Assert.IsTrue(_python.Id != 0);
        }
        [TestMethod()]
        public void Create_Handler_State_Machine()
        {
            _python = new StateMachine
            {
                Title = TITLE,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);
            Assert.IsTrue(_handler.Id != 0);
            Assert.IsTrue(_handler.StateMachineId == _python.Id);
            Assert.IsTrue(_handler.FileName == FILE_NAME);
            Assert.IsTrue(_handler.Path == FILE_PATH + _python.Title + "\\");
        }
        [TestMethod()]
        public void Create_Handler_Argument()
        {
            _python = new StateMachine
            {
                Title = TITLE,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);
            _item_1 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_1
            };
            _argument.Post(_item_1);
            _item_2 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_2
            };
            _argument.Post(_item_2);
            Assert.IsTrue(_item_1.Id != 0);
            Assert.IsTrue(_item_2.Id != 0);
        }
        [TestMethod()]
        public void Create_Handler_Start()
        {
            _python = new StateMachine
            {
                Title = TITLE,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);
            _item_1 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_1
            };
            _argument.Post(_item_1);
            _item_2 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_2
            };
            _argument.Post(_item_2);
            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Button,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            Assert.IsTrue(_statr.Id != 0);
        }
        [TestMethod()]
        public void Create_Handler_Start_Start()
        {
            _python = new StateMachine
            {
                Title = TITLE,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);
            _item_1 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_1
            };
            _argument.Post(_item_1);
            _item_2 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_2
            };
            _argument.Post(_item_2);
            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Button,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            _tools.StartWorker(_statr.Id);
            for(int i = 0; i < int.MaxValue; i++) { }

            Assert.IsTrue(new FileInfo(RESULT).Exists);
        }

        [TestMethod()]
        public void Create_Handler_Start_Start_and_Result()
        {
            _python = new StateMachine
            {
                Title = TITLE,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);
            _item_1 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_1
            };
            _argument.Post(_item_1);
            _item_2 = new Argument
            {
                StateMachineId = _python.Id,
                Arguments = ARG_2
            };
            _argument.Post(_item_2);
            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Button,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            _tools.StartWorker(_statr.Id);
            for (int i = 0; i < int.MaxValue; i++) { }

            FileInfo file = new FileInfo(RESULT);

            Assert.IsTrue(file.Exists);
            int result = 0;
            using (FileStream fstream = File.OpenRead(RESULT))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.Read(buffer, 0, buffer.Length);
                string textFromFile = Encoding.Default.GetString(buffer);
                result = Convert.ToInt32(textFromFile);
            }
            Assert.IsTrue(result == Convert.ToInt32(ARG_1) + Convert.ToInt32(ARG_2));
        }
        [TestMethod()]
        public void Start_form()
        {
            _python = new StateMachine
            {
                Title = TITLE2,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);

            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Form,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            _tools.StartWorker(_statr.Id, "a=123 b=199");
            for (int i = 0; i < int.MaxValue; i++) { }

            FileInfo file = new FileInfo(RESULT);

            Assert.IsTrue(file.Exists);
            int result = 0;
            using (FileStream fstream = File.OpenRead(RESULT))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.Read(buffer, 0, buffer.Length);
                string textFromFile = Encoding.Default.GetString(buffer);
                result = Convert.ToInt32(textFromFile);
            }
            Assert.IsTrue(result == 322);
        }

        [TestMethod]
        public void GoDB()
        {
            _python = new StateMachine
            {
                Title = TITLE3,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);

            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Form,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            var db = $"db={Directory.GetCurrentDirectory()}\\config.db";
            _tools.StartWorker(_statr.Id, $"db={Directory.GetCurrentDirectory()}\\config.db");

            var re = _answer.Get("{\"StatusMachineId\": " + 1 + ", \"Data\": \"i win\"}");
            System.Threading.Thread.Sleep(3000);
            Assert.IsTrue(re.Count != 0);
        }

        [TestMethod]
        public void ErrorsTest()
        {
            _python = new StateMachine
            {
                Title = TITLE4,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);

            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Form,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            var res1 = _answer.Get("{}", 0, int.MaxValue).Count;
            _tools.StartWorker(_statr.Id, "");
            System.Threading.Thread.Sleep(3000);
            var res2 = _answer.Get("{}", 0, int.MaxValue).Count;
            Assert.IsTrue(res1 + 1 == res2);
        }

        [TestMethod]
        public void StatusTest()
        {
            _python = new StateMachine
            {
                Title = TITLE4,
                Type = StateMachineType.Python
            };
            _stateMachine.Post(_python);
            _handler = new HandlerStateMachine
            {
                StateMachineId = _python.Id,
                FileName = FILE_NAME,
                Path = FILE_PATH + _python.Title + "\\"
            };
            _handlerStateMachine.Post(_handler);

            _statr = new StateMachine
            {
                Title = TITLE_START,
                Type = StateMachineType.Form,
                SuccessId = _python.Id,
            };
            _stateMachine.Post(_statr);
            _tools.StartWorker(_statr.Id, "");
            Assert.IsTrue(_stateMachine.GetById(_python.Id).Status == StateMachineStatus.Active);
        }
    }
}