﻿using MakeWork.Models;
using Microsoft.EntityFrameworkCore;

namespace MakeWork
{
    public class Context : DbContext
    {
        public DbSet<StateMachine> StateMachines { get; set; } = null!;
        public DbSet<Argument> Arguments { get; set; } = null!;
        public DbSet<HandlerStateMachine> HandlerStateMachines { get; set; } = null!;
        public DbSet<Input> Inputs { get; set; } = null!;
        public DbSet<DateDemon> DateDemons { get; set; } = null!;
        public DbSet<Answer> Answers { get; set; } = null!;
        public DbSet<AddationalFunctions> AddationalFunctionss { get; set; } = null!;
        public Context() => Database.EnsureCreated();
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseSqlite("data source=config.db");
    }
}
