﻿using AutoMap.ActionEntity;

namespace MakeWork
{
    public class MethodActionUpgrede
    {
        public static dynamic Upgrede<T>(T item)
        {
            Type?
                type = typeof(T),
                _type = Type.GetType($"{type.FullName.Split('.')[0]}.ModelDb.{type.Name}Db", false, true);
            dynamic _item = (T)Activator.CreateInstance(_type);

            ActionEntity.AutoMappper(ref _item, ref item);
            return _item;
        }
        public static void Upgrede<T>(List<T> items)
        {
            for (int i = 0; i < items.Count; i++)
                items[i] = Upgrede(items[i]);
        }

        public static void Upgrede<T>(ref List<T> items)
        {
            for (int i = 0; i < items.Count; i++)
                items[i] = Upgrede(items[i]);
        }
    }
}
