﻿using MakeWork.Models;
using MakeWork;

namespace MakeWork.ModelDb
{
    public abstract class StateMachineDb: StateMachine
    {
        private static ButtonCreater
            s_startCreater = new();
        public const string
            SUCCESS_MASSEG = "Success",
            FAILED_MASSEG = "Failed",
            ERROR_MASSEG = "Error";
        private StateMachine?
            successMachine,
            failedMachine,
            errorMachine;
        public StateMachineDb? SuccessMachine 
        {
            get => successMachine as StateMachineDb;
            private set => successMachine = value;
        }
        public StateMachineDb? FailedMachine
        {
            get => failedMachine as StateMachineDb;
            private set => failedMachine = value;
        }
        public StateMachineDb? ErrorMachine
        {
            get => errorMachine as StateMachineDb;
            private set => errorMachine = value;
        }

        public string Run(ref string[] arguments)
        {
            try
            {
                var result = IntoRun(ref arguments);
                bool type = result.Item1;
                string message = result.Item2;

                if (type)
                {
                    if (SuccessMachine != null)
                    {
                        string[] arg = arguments;
                        if(arguments.Length == 0)
                        {
                            var buff = ToolsModels.Read<Argument>("{\"StateMachineId\": " + SuccessMachine.Id + "}").ResultRead;
                            if (buff != null)
                            {
                                arg = new string[buff.Count];
                                for (int i = 0; i < arg.Length; i++)
                                    arg[i] = buff[i].Arguments;
                            }
                            else
                            {
                                arg = new string[0];
                            }
                        }
                        if(!(this is HandlerStateMachineDb))
                            return SuccessMachine.Run(ref arg);
                    }
                }
                else
                {
                    if (FailedMachine != null)
                        return FailedMachine.Run(ref arguments);
                }
                return message;
            }
            catch (Exception ex)
            {
                string[] buff = new string[1];
                buff[0] = ex.Message;
                if (ErrorMachine != null)
                    return ErrorMachine.Run(ref buff);
                else
                    return ERROR_MASSEG;
            }
        }
        protected abstract (bool, string) IntoRun(ref string[] arguments);
        public void CreaterChain()
        {
            StateMachine buff;
            if (SuccessId != null && SuccessMachine == null)
            {
                var _buff_ = ToolsModels.GetById<StateMachine>(SuccessId ?? 0);
                buff = ToolsModels.GetById<StateMachine>(SuccessId ?? 0).ResultRead[0];
                SuccessMachine = Upgrad(buff);
                SuccessMachine?.CreaterChain();
            }
            
        }
        public static StateMachineDb? Upgrad(StateMachine item)
        {
            return s_startCreater.GetStateMachine(item);
        }
        public static string RunId(int Id)
        {
            var item = ToolsModels.GetById<StateMachine>(Id).ResultRead[0];
            StateMachineDb buff = Upgrad(item);
            buff.CreaterChain();
            string[] arr = new string[0];
            return buff.Run(ref arr);
        }
        public static void RunId(int Id, string[] arguments)
        {
            var item = ToolsModels.GetById<StateMachine>(Id).ResultRead[0];
            StateMachineDb buff = Upgrad(item);
            buff.CreaterChain();
            buff.Run(ref arguments);
        }
        protected void Loging(bool result, string? answer)
        {
            ToolsModels.Create(new Answer
            {
                StateMachineId = Id,
                Data = answer,
                LastUse = DateTime.Now,
                Result = result,
            });
        }
        public static void CreateTree(ref List<StateMachine> list)
        {
            Dictionary<int, StateMachine> buff = new();
            foreach (var item in list) buff[item.Id] = item;

            foreach (var item in buff)
            {
                if (item.Value.SuccessId != null && (int)item.Value.SuccessId != 0)
                    buff[(int)item.Value.SuccessId].Success = item.Value;
                if (item.Value.FailedId != null && (int)item.Value.FailedId != 0)
                    buff[(int)item.Value.FailedId].Failed = item.Value;
                if (item.Value.ErrorId != null && (int)item.Value.ErrorId != 0)
                    buff[(int)item.Value.ErrorId].Error = item.Value;
            }
            list = list.Where(item => 
                item.Type == StateMachineType.Date || 
                item.Type == StateMachineType.Form ||
                item.Type == StateMachineType.Button
                ).ToList();
        }
    }
}
