﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    public abstract class StartStetaMachineDb: StateMachineDb
    {
        public const string
            MASSEGE_ERROR_NOT_STARTER = "Not a starter",
            MASSEGE_NOT_HAVE_ELEMENT = "Not have element";
        private static ButtonCreater
            s_startCreater = new();
        protected abstract string[] GetArgument();
        public static StartStetaMachineDb Create(int id)
        {
            var result = ToolsModels.GetById<StateMachine>(id);
            if (result.ResultRead.Count == 0) 
                throw new Exception(MASSEGE_NOT_HAVE_ELEMENT);

            if // TODO fixed thet bad code
                (
                result.ResultRead[0].Type == StateMachineType.Date ||
                result.ResultRead[0].Type == StateMachineType.Form ||
                result.ResultRead[0].Type == StateMachineType.Button
                )
            {
                StateMachine buffOut = result.ResultRead.Last();
                return 
                    ((StartStetaMachineDb)s_startCreater
                    .GetStateMachine(buffOut))?
                    ._createChain();
            }
            else 
                throw new Exception(MASSEGE_ERROR_NOT_STARTER);
        }

        private StartStetaMachineDb _createChain()
        {
            CreaterChain();
            return this;
        }
    }
}
