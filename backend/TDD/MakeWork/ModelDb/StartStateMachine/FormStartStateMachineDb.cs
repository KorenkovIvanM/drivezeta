﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.ModelDb
{
    public class FormStartStateMachineDb: StartStetaMachineDb
    {
        private const string
            MASSAGE = "Begin";
        protected override string[] GetArgument()
        {
            return new string[0];
        }

        protected override (bool, string) IntoRun(ref string[] arguments)
        {
            if(arguments.Length == 0)
                arguments = GetArgument();
            return (true, MASSAGE);
        }
    }
}
