﻿using MakeWork.Models;

namespace MakeWork.ModelDb
{
    public class ButtonStartStateMachineDb : StartStetaMachineDb
    {
        private const string
            MASSAGE = "Begin";
        protected override string[] GetArgument()
        {
            var buff = ToolsModels.Read<Argument>("{\"StateMachineId\":" + Id + "}", 0, int.MaxValue);
            string[] result = new string[buff.ResultRead.Count];
            if(buff.ResultRead.Count == 0)
                return new string[0];
            for(int i = 0; i < result.Length; i++)
                result[i] = buff.ResultRead[i].Arguments;
            return result;
        }

        protected override (bool, string) IntoRun(ref string[] arguments)
        {
            arguments = GetArgument();
            return (true, MASSAGE);
        }
    }
}
