﻿using MakeWork.ModelDb.IntoStateMachine;
using MakeWork.Models;
using Microsoft.Data.Sqlite;

namespace MakeWork.ModelDb
{
    public class SQLIntoStateMachineDb: IntoStateMachineDb
    {
        public const string
            RESULT_SUCCSESS = "result in the answer",
            NOT_ARGUMRNT = "Not argument",
            NOT_HANDLER = "Not Handler",
            CONNECTION_STRING = "data source=config.db";
        protected IProcessingSQL
            HandlerData = new DefaultProcessingSQL();
        private string
            respons;
        //public SQLIntoStateMachineDb()
        //{

        //}
        protected override (bool, string) IntoRun(ref string[] arguments)
        {
            #region вынеси в конструктор
            var arg = ToolsModels.Read<Argument>("{\"StateMachineId\": " + Id + "}");
            if (arg.ResultRead == null || arg.ResultRead.Count == 0) return (false, NOT_ARGUMRNT);
            respons = arg.ResultRead[0].Arguments;

            TypeHandlerDocsCreater handler;
            if (arg.ResultRead.Count == 1)
                handler = TypeHandlerDocsCreater.Default;
            else
                handler = (TypeHandlerDocsCreater)Convert.ToInt32(arg.ResultRead[1].Arguments);

            // TODO delet me place
            if (handler == TypeHandlerDocsCreater.Default) HandlerData = new DefaultProcessingSQL();
            else if (handler == TypeHandlerDocsCreater.EXEL) HandlerData = new ExelReportDocs();
            #endregion

            using (var connection = new SqliteConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqliteCommand command = new SqliteCommand(respons, connection);
                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        if(HandlerData != null)
                        {
                            var buff = reader;
                            var result = HandlerData.ProcessingSQL(ref buff);
                            Log(result);
                            return result;
                        }
                        else
                        {
                            return (false, NOT_HANDLER);
                        }
                    }
                    else 
                        return (false, NOT_ARGUMRNT);
                }
            }
            return (false, ERROR_MASSEG);
        }
    }

    public enum TypeHandlerDocsCreater
    {
        Default,
        EXEL,
    }
}
