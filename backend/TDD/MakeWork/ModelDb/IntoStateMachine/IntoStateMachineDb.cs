﻿using MakeWork.Models;

namespace MakeWork.ModelDb
{
    public abstract class IntoStateMachineDb : StateMachineDb
    {
        public void Log(bool result, string data)
        {
            ToolsModels.Create(new Answer 
            { 
                StateMachineId = Id,
                Data = data,
                LastUse = DateTime.Now,
                Result = result,
            });
        }
        public void Log((bool, string)data)
        {
            ToolsModels.Create(new Answer
            {
                StateMachineId = Id,
                Data = data.Item2,
                LastUse = DateTime.Now,
                Result = data.Item1,
            });
        }
    }
}
