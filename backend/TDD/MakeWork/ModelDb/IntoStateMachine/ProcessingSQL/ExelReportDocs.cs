﻿using Microsoft.Data.Sqlite;
using System;
using MakeWork.Models;
using System.Data;
using Newtonsoft.Json;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace MakeWork.ModelDb.IntoStateMachine
{
    internal class ExelReportDocs : ReportDocs
    {
        private string FileName
        {
            get
            {
                return "TestNewData.xlsx";
            }
        }
        protected override string _createDocs(ref SqliteDataReader data)
        {
            List<string[]> list = new List<string[]>();
            while(data.Read())
            {
                var row = new string[data.FieldCount];
                for(int i = 0; i < row.Length; i++)
                    row[i] = data.GetValue(i).ToString();
                list.Add(row);
            }

            //DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(list), (typeof(DataTable)));

            using (SpreadsheetDocument document = SpreadsheetDocument.Create(PATH_FOR_WORK + FileName, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };

                sheets.Append(sheet);

                //Row headerRow = new Row();

                //List<String> columns = new List<string>();
                //foreach (System.Data.DataColumn column in table.Columns)
                //{
                //    columns.Add(column.ColumnName);

                //    Cell cell = new Cell();
                //    cell.DataType = CellValues.String;
                //    cell.CellValue = new CellValue(column.ColumnName);
                //    //headerRow.AppendChild(cell);
                //}

                //sheetData.AppendChild(headerRow);

                for(int i = 0; i < list.Count; i++)
                {
                    Row newRow = new Row();
                    for (int j = 0; j < list[i].Length; j++)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(list[i][j]);//new CellValue(dsrow[col].ToString());
                        newRow.AppendChild(cell);
                    }
                    sheetData.AppendChild(newRow);
                }

                //foreach (DataRow dsrow in table.Rows)
                //{
                //    Row newRow = new Row();
                //    foreach (String col in columns)
                //    {
                //        Cell cell = new Cell();
                //        cell.DataType = CellValues.String;
                //        cell.CellValue = new CellValue(dsrow[col].ToString());
                //        newRow.AppendChild(cell);
                //    }

                //    sheetData.AppendChild(newRow);
                //}

                workbookPart.Workbook.Save();
            }

            return FileName;
        }
    }
}
