﻿using Microsoft.Data.Sqlite;

namespace MakeWork.ModelDb.IntoStateMachine
{
    public interface IProcessingSQL
    {
        (bool, string) ProcessingSQL(ref SqliteDataReader data);
    }
}
