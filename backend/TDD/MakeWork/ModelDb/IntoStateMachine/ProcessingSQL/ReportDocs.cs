﻿using Microsoft.Data.Sqlite;
using MakeWork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace MakeWork.ModelDb.IntoStateMachine
{
    public abstract class ReportDocs : IProcessingSQL
    {
        protected const string
            PATH_FOR_WORK = "D:\\drivezeta\\ReportDocs\\";

        protected abstract string _createDocs(ref SqliteDataReader data);
        public (bool, string) ProcessingSQL(ref SqliteDataReader data)
        {
            if (!Directory.Exists(PATH_FOR_WORK))
                Directory.CreateDirectory(PATH_FOR_WORK);
            return (true, PATH_FOR_WORK + _createDocs(ref data));
        }
    }
}
