﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.ModelDb.IntoStateMachine
{
    internal class DefaultProcessingSQL : IProcessingSQL
    {
        public (bool, string) ProcessingSQL(ref SqliteDataReader data)
        {
            return (true, data.Read() ? data.GetValue(0).ToString() : null);
        }
    }
}
