﻿using MakeWork.Models;

namespace MakeWork.ModelDb
{
    public class HandlerStateMachineDb : StateMachineDb
    {
        private const string 
            PATH_RESOOURCE = "Resourse/";
        private HandlerStateMachine?
            metaData;
        public HandlerStateMachine? MetaData 
        {
            get
            {
                _InitialFileInfo();
                return metaData;
            }
        }
        public string FullName
        {
            get
            {
                _InitialFileInfo();
                return MetaData.Path + MetaData.FileName;
            }
        }
        private void _InitialFileInfo()
        {
            if (metaData == null)
            {
                var buff = ToolsModels.Read<HandlerStateMachine>("{\"StateMachineId\":" + Id + "}");
                if (buff.ResultRead?.Count != 0)
                    metaData = buff.ResultRead[0];
            }
        }
        public void Write(byte[]? file, string fileName, int StateMachineId)
        {
            bool result = false;
            using (var fileStream = new FileStream(PATH_RESOOURCE + fileName, FileMode.OpenOrCreate))
            {
                fileStream.WriteAsync(file, 0, file.Length);
                result = true;
            }
            if (result)
                ToolsModels.Create(new HandlerStateMachine()
                {
                    FileName = fileName,
                    Path = PATH_RESOOURCE,
                    StateMachineId = StateMachineId,
                });
        }
        
        protected override (bool, string) IntoRun(ref string[] arguments)
        {
            throw new NotImplementedException();
        }
    }
}
