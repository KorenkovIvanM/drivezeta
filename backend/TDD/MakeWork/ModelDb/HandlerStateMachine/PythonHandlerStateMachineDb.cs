﻿using System.Diagnostics;
using MakeWork.Models;
using MakeWork;

namespace MakeWork.ModelDb
{
    public class PythonHandlerStateMachineDb: HandlerStateMachineDb
    {
        protected override (bool, string) IntoRun(ref string[] arguments)
        {
            // TODO вынеси в отдельный метод
            if (new FileInfo(FullName).Exists)
            {
                var command = $"python \"{FullName}\" " + $"db={Directory.GetCurrentDirectory()}\\config.db " + String.Join(' ', arguments);
                var result = Process.Start("cmd.exe", "/C" + command);
                result.EnableRaisingEvents = true;
                result.Exited += onSuccess;
                result.ErrorDataReceived += onError;

                // TODO execute
                Status = StateMachineStatus.Active;
                ToolsModels.Update<StateMachine>(this);

                return (true, SUCCESS_MASSEG);
            }
            else
                return (false, FAILED_MASSEG);
        }

        private void onError(object sender, DataReceivedEventArgs e)
        {
            Status = StateMachineStatus.NonActive;
            ToolsModels.Update<StateMachine>(this);
            ToolsModels.Create(new Answer()
            { 
                LastUse = DateTime.Now,
                Result = false,
                StateMachineId = Id
            });
        }

        private void onSuccess(object? sender, EventArgs e)
        {
            Status = StateMachineStatus.NonActive;
            ToolsModels.Update<StateMachine>(this);
            ToolsModels.Create(new Answer()
            {
                LastUse = DateTime.Now,
                Result = true,
                StateMachineId = Id
            });
            string[] arg = new string[0];
            SuccessMachine?.Run(ref arg);
        }
    }
}
