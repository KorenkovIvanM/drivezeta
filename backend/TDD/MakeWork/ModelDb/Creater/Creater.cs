﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MakeWork.Models;

namespace MakeWork.ModelDb
{
    internal abstract class Creater
    {
        protected abstract StateMachineType Type { get; }
        protected Creater? NextCreater { get; set; }
        protected abstract StateMachineDb _GetStateMachine(ref StateMachine item);
        internal StateMachineDb? GetStateMachine(StateMachine item)
        {
            if (item.Type == Type)
            {
                return _GetStateMachine(ref item);
            }
            else if (NextCreater != null)
            {
                return NextCreater.GetStateMachine(item);
            }
            else
            {
                return null;
            }
        }
    }
}
