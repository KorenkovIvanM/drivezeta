﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    internal class PythonCreater: Creater
    {
        protected override StateMachineType Type => StateMachineType.Python;
        internal PythonCreater() => NextCreater = new FormCreater();
        protected override StateMachineDb _GetStateMachine(ref StateMachine item)
        {
            PythonHandlerStateMachineDb buffIn = new PythonHandlerStateMachineDb();
            ActionEntity.AutoMappper(ref buffIn, ref item);
            return buffIn;
        }
    }
}
