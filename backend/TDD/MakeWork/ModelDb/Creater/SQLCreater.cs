﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    internal class SQLCreater: Creater
    {
        protected override StateMachineType Type => StateMachineType.SQL;
        internal SQLCreater() => NextCreater = null;
        protected override StateMachineDb _GetStateMachine(ref StateMachine item)
        {
            SQLIntoStateMachineDb buffIn = new SQLIntoStateMachineDb();
            ActionEntity.AutoMappper(ref buffIn, ref item);
            return buffIn;
        }
    }
}
