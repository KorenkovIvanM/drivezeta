﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    internal class ButtonCreater : Creater
    {
        internal ButtonCreater() => NextCreater = new DateCreater();
        protected override StateMachineType Type => StateMachineType.Button;
        protected override StateMachineDb _GetStateMachine(ref StateMachine item)
        {
            ButtonStartStateMachineDb buffIn = new ButtonStartStateMachineDb();
            ActionEntity.AutoMappper(ref buffIn, ref item);
            return buffIn;
        }
    }
}
