﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    internal class FormCreater: Creater
    {
        protected override StateMachineType Type => StateMachineType.Form;
        internal FormCreater() => NextCreater = new SQLCreater();
        protected override StateMachineDb _GetStateMachine(ref StateMachine item)
        {
            FormStartStateMachineDb buffIn = new FormStartStateMachineDb();
            ActionEntity.AutoMappper(ref buffIn, ref item);
            return buffIn;
        }
    }
}
