﻿using MakeWork.Models;
using AutoMap.ActionEntity;

namespace MakeWork.ModelDb
{
    internal class DateCreater: Creater
    {
        protected override StateMachineType Type => StateMachineType.Date;
        internal DateCreater() => NextCreater = new PythonCreater();
        protected override StateMachineDb _GetStateMachine(ref StateMachine item)
        {
            DateStartStateMachineDb buffIn = new DateStartStateMachineDb();
            ActionEntity.AutoMappper(ref buffIn, ref item);
            return buffIn;
        }
    }
}
