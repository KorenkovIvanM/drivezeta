﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class SMProcess
    {
        public int Id { get; set; }
        public int StateMachineId { get; set; }
        public StateMachine StateMachine { get; set; } = null!;
        public DateTime Start { get; set; } = DateTime.Now;
        public DateTime? Stop { get; set; }
        public bool Result { get; set; }
        public List<Answer> Answers { get; set; } = new List<Answer>();
    }
}
