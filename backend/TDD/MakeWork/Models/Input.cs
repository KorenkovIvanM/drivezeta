﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class Input
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int StartStateMachineDbId { get; set; }
        public InputType Type { get; set; }
    }
    public enum InputType
    {
        Text,
        Number,
        Date,
        File,
    }
}
