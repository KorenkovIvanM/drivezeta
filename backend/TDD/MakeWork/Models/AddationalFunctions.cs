﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class AddationalFunctions
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string URL { get; set; } = null!;
        public string NamePage { get; set; } = null!;
        public int StateMachineId { get; set; }
    }
}
