﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class Argument
    {
        public int Id { get; set; }
        public int StateMachineId { get; set; }
        public string Arguments { get; set; } = null!;
    }
}
