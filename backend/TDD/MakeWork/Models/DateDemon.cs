﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class DateDemon
    {
        public int Id { get; set; }
        public int StartMachneDbId { get; set; }
        public string Demon { get; set; } = null!;
        public TypeDateDemon Type { get; set; } = TypeDateDemon.Activ;
    }
    public enum TypeDateDemon
    {
        Activ,
        NonActiv,
        BreackNext,
    }
}
