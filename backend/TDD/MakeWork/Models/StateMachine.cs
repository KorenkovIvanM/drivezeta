﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MakeWork.Models
{
    public class StateMachine
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string? Description { get; set; } = null;
        public int? SuccessId { get; set; } = null;
        public int? FailedId { get; set; } = null;
        public int? ErrorId { get; set; } = null;
        public StateMachineType Type { get; set; }
        public StateMachineStatus Status { get; set; }
        [NotMapped]
        public StateMachine? Success { get; set; }
        [NotMapped]
        public StateMachine? Failed { get; set; }
        [NotMapped]
        public StateMachine? Error { get; set; }
    }
    public enum StateMachineType
    {
        Python,
        SQL,
        JavaScript,
        Bat,
        Into,
        Date,
        Form,
        Button,
    }
    public enum StateMachineStatus
    {
        NonActive,
        Active,
        Stoping,
    }
}
