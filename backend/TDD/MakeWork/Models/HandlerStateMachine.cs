﻿namespace MakeWork.Models
{
    public class HandlerStateMachine
    {
        public int Id { get; set; }
        public int StateMachineId { get; set; }
        public string FileName { get; set; } = null!;
        public string Path { get; set; } = null!;
    }
}
