﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWork.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public DateTime LastUse { get; set; } = DateTime.Now;
        public bool Result { get; set; }
        public string? Data { get; set; }
        public int StateMachineId { get; set; }
    }
}
