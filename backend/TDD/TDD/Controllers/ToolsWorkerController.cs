﻿using Microsoft.AspNetCore.Mvc;
using MakeWork.Models;
using MakeWork.ModelDb;
using MakeWork;

namespace TDD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ToolsWorkerController// : MakeWork.ToolsWorkerController
    {
        [HttpPost("Start")]
        public virtual StateMachineStatus StartWorker(int Id, string? Argument = null)
        {
            if(Argument == null)
                StateMachineDb.RunId(Id);
            else
                StateMachineDb.RunId(Id, Argument.Split(' '));
            return StateMachineStatus.NonActive;
        }
        [HttpPost("File")]
        public bool UploadFile(IFormFile file, int IdStateMachine)
        {
            if (file != null)
            {
                var buff = ToolsModels.GetById<StateMachine>(IdStateMachine);
                var path = "";
                using (var target = new MemoryStream())
                {
                    file.CopyTo(target);
                    path = $"D:\\Drivezeta\\Resource\\{buff.ResultRead[0].Title}\\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    using (var fileStream = new FileStream(path + file.FileName, FileMode.OpenOrCreate))
                    {
                        var s_bytr = target.ToArray();
                        fileStream.WriteAsync(s_bytr, 0, s_bytr.Length);
                    }
                }
                HandlerStateMachine pathForFile = new()
                {
                    Path = path,
                    FileName = file.FileName,
                    StateMachineId = IdStateMachine,
                };
                ToolsModels.Create(pathForFile);
            }

            return true;
        }
    }
}
