﻿using Microsoft.AspNetCore.Mvc;
using MakeWork.Models;
using MakeWork;

namespace TDD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SQLController : Controller
    {
        [HttpGet]
        public List<StateMachine> get()
        {
            using(Context context = new())
            {
                return (
                    from 
                        sm1 in context.StateMachines join
                        sm2 in context.StateMachines on sm1.SuccessId equals sm2.Id
                    where 
                        sm2.Type == StateMachineType.SQL &&
                        (sm1.Type == StateMachineType.Button ||
                        sm1.Type == StateMachineType.Form ||
                        sm1.Type == StateMachineType.Date)
                    select sm1
                    ).ToList();
            }
        }
    }
}
