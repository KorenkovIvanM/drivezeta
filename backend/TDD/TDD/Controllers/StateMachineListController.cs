﻿using Microsoft.AspNetCore.Mvc;
using GenericToolsEntitys;
using MakeWork.Models;
using MakeWork;

namespace TDD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StateMachineListController : Controller
    {
        [HttpGet]
        public List<StateMachine> GetStarter(bool isAll, int NumberPage = 0, int Size = 50)
        {
            using (var context = new Context())
            {
                return
                    (
                    from item in context.StateMachines
                    where 
                        item.Type == StateMachineType.Date ||
                        item.Type == StateMachineType.Form ||
                        item.Type == StateMachineType.Button
                    select item)
                    //.Take(new Range(NumberPage, Size))
                    .ToList();
            }
        }
    }
}
