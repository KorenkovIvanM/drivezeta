﻿using Microsoft.AspNetCore.Mvc;
using GenericToolsEntitys;
using MakeWork.ModelDb;
using MakeWork.Models;
using MakeWork;
using Hangfire;

namespace TDD.Controllers
{
    public class DateDemonController : GenericController<DateDemon, Context>
    {
        public DateDemonController() =>
            OnPredPost += DateDemonController_OnPredPost;

        private void DateDemonController_OnPredPost(ref DateDemon item)
        {
            var sm = ToolsModel.GetById<StateMachine, Context>(item.StartMachneDbId).ResultRead[0];
            RecurringJob.AddOrUpdate($"ID:: {sm.Id} || NAME:: {sm.Title}", () => StateMachineDb.RunId(sm.Id), item.Demon);
        }
    }
}
