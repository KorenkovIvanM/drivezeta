﻿using GenericToolsEntitys;
using Models.Models;
using Models;

namespace TDD.Controllers
{
    public class OrderSoftController : GenericController<OrderSoft, Context>
    {
        public OrderSoftController()
        {
            OnPostGetById += onGetChildrens;
            OnPostGet += onGet;
            OnPredDelete += DeleteOll;
        }

        private void DeleteOll(ref int Id)
        {
            var buff = Models.ToolsModel.GetById<OrderSoft>(Id).ResultRead[0];
            buff.DeleteChildren();
        }

        private void onGet(ref List<OrderSoft>? list)
        {
            Models.MethodActionUpgrede.Upgrede(ref list);
            Models.ModelDb.OrderProjectDb<OrderSoft>.CreateTree(ref list);
        }

        private void onGetChildrens(ref OrderSoft? item)
        {
            //item.CreateTree()
        }
    }
}
