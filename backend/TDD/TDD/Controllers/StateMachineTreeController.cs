﻿using Microsoft.AspNetCore.Mvc;
using GenericToolsEntitys;
using MakeWork.Models;
using MakeWork;
using MakeWork.ModelDb;

namespace TDD.Controllers
{
    public class StateMachineTreeController : GenericController<StateMachine, Context>
    {
        public StateMachineTreeController() => OnPostGet += onGet;
        private void onGet(ref List<StateMachine>? list) =>
            StartStetaMachineDb.CreateTree(ref list);
    }
}
