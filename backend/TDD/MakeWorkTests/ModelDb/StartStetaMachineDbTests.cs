﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MakeWork.ModelDb;
using MakeWork.Models;
using System.IO;

namespace MakeWork.ModelDb.Tests
{
    [TestClass()]
    public class StartStetaMachineDbTests
    {
        [TestMethod()]
        public void CreateTest()
        {
            var buff = new StateMachine()
            {
                Title = "Test",
                Type = StateMachineType.Button
            };
            ToolsModels.Create(buff);
            Assert.IsTrue(buff.Type == StateMachineType.Button);
        }
        [TestMethod()]
        public void CreateTest_chain_of_responsebility()
        {
            var buff = new StateMachine()
            {
                Title = "Test",
                Type = StateMachineType.Button
            };
            ToolsModels.Create(buff);
            var _result = StartStetaMachineDb.Create(buff.Id);
            Assert.IsTrue(_result != null);
        }
        [TestMethod()]
        public void CreateTest_chain()
        {
            StateMachine level_one = new()
            {
                Title = "livel 1",
                Type = StateMachineType.Python
            };
            ToolsModels.Create(level_one);



            StateMachine level_start = new()
            {
                Title = "Test",
                Type = StateMachineType.Button,
                SuccessId = level_one.Id,
            };
            ToolsModels.Create(level_start);



            Argument arg = new()
            {
                Arguments = "",
                StateMachineId = level_start.Id,
            };
            ToolsModels.Create(arg);



            HandlerStateMachine path = new()
            {
                Path = "",
                FileName = "test.py",
                StateMachineId = level_one.Id,
            };
            ToolsModels.Create(path);


            StateMachineDb buff = StateMachineDb.Upgrad(level_start);
            buff.CreaterChain();
            string[] arr = new string[0];
            buff.Run(ref arr);
            Assert.IsTrue(new FileInfo("BabyFile.txt").Exists);
        }
        [TestMethod()]
        public void CreateTest_chain_one_two()
        {
            StateMachine level_one = new()
            {
                Title = "livel 1",
                Type = StateMachineType.Python
            };
            ToolsModels.Create(level_one);



            StateMachine level_start = new()
            {
                Title = "Test",
                Type = StateMachineType.Button,
                SuccessId = level_one.Id,
            };
            ToolsModels.Create(level_start);



            Argument arg = new()
            {
                Arguments = "",
                StateMachineId = level_start.Id,
            };
            ToolsModels.Create(arg);



            HandlerStateMachine path = new()
            {
                Path = "",
                FileName = "test.py",
                StateMachineId = level_one.Id,
            };
            ToolsModels.Create(path);


            StateMachineDb buff = StateMachineDb.Upgrad(level_start);
            buff.CreaterChain();
            Assert.IsTrue(buff.SuccessMachine != null);
        }
        [TestMethod()]
        public void ChainOfResponsebility_Button()
        {
            StateMachine buff = new()
            {
                Title = "Test",
                Type = StateMachineType.Button,
            };
            ToolsModels.Create(buff);
            StateMachineDb _buff = StateMachineDb.Upgrad(buff);
            Assert.IsTrue(_buff is StateMachineDb);
            Assert.IsTrue(_buff is ButtonStartStateMachineDb);
        }
        [TestMethod()]
        public void ChainOfResponsebility_Date()
        {
            StateMachine buff = new()
            {
                Title = "Test",
                Type = StateMachineType.Date,
            };
            ToolsModels.Create(buff);
            StateMachineDb _buff = StateMachineDb.Upgrad(buff);
            Assert.IsTrue(_buff is StateMachineDb);
            Assert.IsTrue(_buff is DateStartStateMachineDb);
        }
        [TestMethod()]
        public void ChainOfResponsebility_Python()
        {
            StateMachine buff = new()
            {
                Title = "Test",
                Type = StateMachineType.Python,
            };
            ToolsModels.Create(buff);
            StateMachineDb _buff = StateMachineDb.Upgrad(buff);
            Assert.IsTrue(_buff is StateMachineDb);
            Assert.IsTrue(_buff is PythonHandlerStateMachineDb);
        }
        [TestMethod()]
        public void ChainOfResponsebility_Form()
        {
            StateMachine buff = new()
            {
                Title = "Test",
                Type = StateMachineType.Form,
            };
            ToolsModels.Create(buff);
            StateMachineDb _buff = StateMachineDb.Upgrad(buff);
            Assert.IsTrue(_buff is StateMachineDb);
            Assert.IsTrue(_buff is FormStartStateMachineDb);
        }
        [TestMethod()]
        public void CreateTest_other_path()
        {
            StateMachine level_one = new()
            {
                Title = "livel 1",
                Type = StateMachineType.Python
            };
            ToolsModels.Create(level_one);



            StateMachine level_start = new()
            {
                Title = "Test",
                Type = StateMachineType.Button,
                SuccessId = level_one.Id,
            };
            ToolsModels.Create(level_start);



            Argument arg = new()
            {
                Arguments = "",
                StateMachineId = level_start.Id,
            };
            ToolsModels.Create(arg);



            HandlerStateMachine path = new()
            {
                Path = "D:\\",
                FileName = "test.py",
                StateMachineId = level_one.Id,
            };
            ToolsModels.Create(path);


            StateMachineDb buff = StateMachineDb.Upgrad(level_start);
            buff.CreaterChain();
            string[] arr = new string[0];
            buff.Run(ref arr);
            Assert.IsTrue(new FileInfo("BabyFile.txt").Exists);
        }
        [TestMethod()]
        public void SQLTest()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "SELECT * FROM StateMachines"
            };
            ToolsModels.Create(arg);
            StateMachineDb.RunId(sm.Id);
            var result1 = ToolsModels.Read<StateMachine>("{}", 0, 1).ResultRead[0].Id.ToString();
            var result2 = ToolsModels.Read<Answer>("{\"StateMachineId\":" + sm.Id + "}", 0, 1).ResultRead[0].Data;

            Assert.IsTrue(result1 == result2);
        }
        [TestMethod()]
        public void SQLTest_GetArgument()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "SELECT * FROM StateMachines"
            };
            ToolsModels.Create(arg);
            var a = ToolsModels.Read<Argument>("{   \"StateMachineId\": " + sm.Id + " }");

            Assert.IsTrue(a.ResultRead.Count != 0);
        }
        [TestMethod()]
        public void SQLTest_Not_argument()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var result = StateMachineDb.RunId(sm.Id);
            Assert.IsTrue(result == "Not argument");
        }
        [TestMethod()]
        public void SQLTest_Exist_argument()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "SELECT * FROM StateMachines"
            };
            ToolsModels.Create(arg);
            var result = StateMachineDb.RunId(sm.Id);
            Assert.IsTrue(result == "1");
        }
        [TestMethod()]
        public void SQLTest_Not_correct_SQL()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "asd"
            };
            ToolsModels.Create(arg);
            var result = StateMachineDb.RunId(sm.Id);
            Assert.IsTrue(result == SQLIntoStateMachineDb.ERROR_MASSEG);
        }

        [TestMethod()]
        public void SQLTest_EXEL()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "SELECT * FROM StateMachines"
            };
            ToolsModels.Create(arg);
            var arg1 = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "1"
            };
            ToolsModels.Create(arg1);
            var result = StateMachineDb.RunId(sm.Id);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void SQLTest_EXEL_exet()
        {
            var sm = new StateMachine
            {
                Title = "SQLTest",
                Type = StateMachineType.SQL
            };
            ToolsModels.Create(sm);
            var arg = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "SELECT * FROM StateMachines"
            };
            ToolsModels.Create(arg);
            var arg1 = new Argument
            {
                StateMachineId = sm.Id,
                Arguments = "1"
            };
            ToolsModels.Create(arg1);
            var result = StateMachineDb.RunId(sm.Id);
            Assert.IsTrue(new FileInfo(result).Exists);
        }
    }
}