﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models;
using Models.ModelDb;

namespace ModelsDb.Tests
{
    [TestClass()]
    public class ModelsDbTests
    {
        [TestMethod()]
        public void Event_Models_ModelsDb_Order()
        {
            var _model = new OrderWeekDb()
            {
                Title = "Test",
            };

            var old = _model.End;
            _model.Status = StatusOrdr.Closed;
            var _new = _model.End;

            Assert.IsTrue(old == null);
            Assert.IsTrue(_new != null);
        }

        [TestMethod()]
        public void PushTests_Month_Week()
        {
            var old_count_month = ToolsModel.Read<OrderMonth>(0, int.MaxValue).ResultRead.Count;
            var old_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;


            var order_month = new OrderMonthDb()
            {
                Title = "1234",
            };
            ToolsModelDb.Create(order_month);

            var miid_count_month = ToolsModel.Read<OrderMonth>(0, int.MaxValue).ResultRead.Count;
            var miid_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;

            order_month.Push();

            var new_count_month = ToolsModel.Read<OrderMonth>(0, int.MaxValue).ResultRead.Count;
            var new_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(old_count_month + 1 == miid_count_month);
            Assert.IsTrue(old_count_week == miid_count_week);
            Assert.IsTrue(miid_count_month - 1 == new_count_month);
            Assert.IsTrue(miid_count_week + 1 == new_count_week);
        }

        [TestMethod()]
        public void PushTests_Week_Soft()
        {
            var old_count_soft = ToolsModel.Read<OrderSoft>(0, int.MaxValue).ResultRead.Count;
            var old_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;


            var order_week = new OrderWeekDb()
            {
                Title = "1234",
            };
            ToolsModelDb.Create(order_week);

            var miid_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;
            var miid_count_soft = ToolsModel.Read<OrderSoft>(0, int.MaxValue).ResultRead.Count;

            order_week.Push();

            var new_count_week = ToolsModel.Read<OrderWeek>(0, int.MaxValue).ResultRead.Count;
            var new_count_soft = ToolsModel.Read<OrderSoft>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(old_count_soft == miid_count_soft);
            Assert.IsTrue(old_count_week + 1 == miid_count_week);
            Assert.IsTrue(miid_count_week - 1 == new_count_week);
            Assert.IsTrue(miid_count_soft + 1 == new_count_soft);
        }

        [TestMethod()]
        public void ClosedTests_for_Orders()
        {
            Target create_target = new TargerDb("My target", null, 100);
            ToolsModel.Create(create_target);

            Target read_target = ToolsModel.Read<Target>("{\"Title\": \"" + create_target.Title + "\"}").ResultRead.Last();

            OrderHard order = new OrderHardDb("My tasks");
            order.TargetId = read_target.Id;
            order.Weight = 110;
            ToolsModel.Create(order);

            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead.Last();


            OrderHardDb work_order = MethodActionUpgrede.Upgrede(read_oreder);
            work_order.Target = ToolsModel.GetById<Target>(read_target.Id).ResultRead.Last();

            work_order.Status = StatusOrdr.Closed;

            Assert.IsTrue(work_order.Status == StatusOrdr.Closed);
            Assert.IsTrue(work_order.Target != null);
            Assert.IsTrue(ToolsModel.GetById<Target>(read_target.Id).ResultRead.Last().Value != 0);
        }

        [TestMethod()]
        public void Search_Target()
        {
            Target create_target = new TargerDb("My target", null, 100);
            ToolsModel.Create(create_target);

            Target read_target = ToolsModel.Read<Target>("{\"Title\": \"" + create_target.Title + "\"}").ResultRead[0];

            Assert.IsTrue(create_target.Title == read_target.Title);
        }

        [TestMethod()]
        public void Search_Target_and_create_order()
        {
            Target create_target = new TargerDb("My target", null, 100);
            ToolsModel.Create(create_target);

            Target read_target = ToolsModel.Read<Target>("{\"Title\": \"" + create_target.Title + "\"}").ResultRead[0];

            OrderHard order = new OrderHardDb("My tasks");
            order.TargetId = read_target.Id;
            ToolsModel.Create(order);

            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead[0];

            Assert.IsTrue(order.Title == read_oreder.Title);
            Assert.IsTrue(read_oreder.TargetId == read_target.Id);
        }

        [TestMethod()]
        public void Search_Target_and_create_order_upgate()
        {
            Target create_target = new TargerDb("My target", null, 100);
            ToolsModel.Create(create_target);

            Target read_target = ToolsModel.Read<Target>("{\"Title\": \"" + create_target.Title + "\"}").ResultRead[0];

            OrderHard order = new OrderHardDb("My tasks");
            order.TargetId = read_target.Id;
            ToolsModel.Create(order);

            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead[0];

            OrderHardDb work_order = MethodActionUpgrede.Upgrede(read_oreder);

            Assert.IsTrue(read_oreder.Id == work_order.Id);
            Assert.IsTrue(read_oreder is OrderHard);
            Assert.IsTrue(work_order is OrderHardDb);
        }

        [TestMethod()]
        public void Search_Target_and_create_order_upgate_status()
        {
            Target create_target = new TargerDb("My target", null, 100);
            ToolsModel.Create(create_target);

            Target read_target = ToolsModel.Read<Target>("{\"Title\": \"" + create_target.Title + "\"}").ResultRead.Last();

            OrderHard order = new OrderHardDb("My tasks");
            order.TargetId = read_target.Id;
            order.Weight = 10;
            ToolsModel.Create(order);

            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead.Last();


            OrderHardDb work_order = MethodActionUpgrede.Upgrede(read_oreder);
            work_order.Target = ToolsModel.GetById<Target>(read_target.Id).ResultRead.Last();

            work_order.Status = StatusOrdr.Closed;

            Assert.IsTrue(work_order.Status == StatusOrdr.Closed);
            Assert.IsTrue(work_order.Target != null);
            Assert.IsTrue(work_order.Target.Value != 0);
        }

        [TestMethod()]
        public void TemplateTests_Create()
        {
            OrderHard order = new OrderHardDb("My tasks");
            ToolsModel.Create(order);
            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead.Last();

            var count_old_template = ToolsModel.Read<Template>(0, int.MaxValue).ResultRead.Count;

            Template template = new TemplateDb(read_oreder.Id);
            ToolsModel.Create(template);

            var count_new_template = ToolsModel.Read<Template>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(count_old_template + 1 == count_new_template);
        }

        [TestMethod()]
        public void TemplateTests_Create_order()
        {
            OrderHard order = new OrderHardDb("My tasks");
            ToolsModel.Create(order);
            OrderHard read_oreder = ToolsModel.Read<OrderHard>("{\"Title\": \"" + order.Title + "\"}").ResultRead.Last();

            var count_old_template = ToolsModel.Read<OrderHard>(0, Int32.MaxValue).ResultRead.Count;

            Template template = new TemplateDb(read_oreder.Id);
            ToolsModel.Create(template);
            ((TemplateDb)template).CreateOrder();
            var count_new_template = ToolsModel.Read<OrderHard>(0, Int32.MaxValue).ResultRead.Count;
            

            Assert.IsTrue(count_old_template + 1 == count_new_template);
        }

        [TestMethod()]
        public void LinkTest_Create()
        {
            int old_value = ToolsModel.Read<Link>(0,int.MaxValue).ResultRead.Count;

            Link link = new Link()
            {
                OrderBein = 1,
                OrderEnd = 2,
                Type = 3,
            };

            ToolsModel.Create(link);

            int new_value = ToolsModel.Read<Link>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(old_value + 1 == new_value);
        }
    }
}