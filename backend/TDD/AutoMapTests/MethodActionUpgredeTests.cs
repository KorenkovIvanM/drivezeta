﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models;
using Models.ModelDb;
using AutoMap.ActionEntity;

namespace Models.Tests
{
    [TestClass()]
    public class MethodActionUpgredeTests
    {
        [TestMethod()]
        public void UpgredeTest()
        {
            Idea item = new()
            {
                Id = 10,
                Title = "Same Idea",
            };
            Assert.IsTrue(typeof(Idea) == item.GetType());
            IdeaDb _item = MethodActionUpgrede.Upgrede(item);

            Assert.IsTrue(typeof(Idea) == item.GetType());
            Assert.IsTrue(typeof(IdeaDb) == _item.GetType());
        }

        [TestMethod()]
        public void UpgredeTestFailed()
        {
            Idea item = new()
            {
                Id = 10,
                Title = "Same Idea",
            };
            IdeaDb _item = MethodActionUpgrede.Upgrede(item);

            Assert.IsTrue(item.Id == _item.Id);
            Assert.IsTrue(item.Title == _item.Title);
        }

        [TestMethod()]
        public void UpgredeTestFailed_past()
        {
            Idea item = new()
            {
                Id = 10,
                Title = "Same Idea",
            };
            IdeaDb _item = new();

            ActionEntity.AutoMappper(ref _item, ref item);

            Assert.IsTrue(item.Id == _item.Id);
            Assert.IsTrue(item.Title == _item.Title);
        }

        [TestMethod()]
        public void UpgredeTestFailed_past_json()
        {
            Idea item = new()
            {
                Id = 10,
                Title = "Same Idea",
            };
            IdeaDb _item = new();

            ActionEntity.AutoMappper(ref _item, ref item, "{\"Id\": 12}");

            Assert.IsTrue(item.Id == _item.Id);
            Assert.IsTrue(item.Title != _item.Title);
        }

        [TestMethod()]
        public void UpgredeTestFailed_for_list()
        {
            List<Idea> list = new List<Idea>();
            for(int i = 0; i < 10; i++)
            {
                list.Add(new()
                {
                    Id = 10,
                    Title = "Same Idea",
                });
            }

            MethodActionUpgrede.Upgrede(list);

            Assert.IsTrue(list is List<Idea>);
            Assert.IsTrue(list[0] is IdeaDb);
        }

        [TestMethod()]
        public void GetByIdTest_positiv()
        {
            // TODO надо ли это вообще подумай и если что придёться делать костыли или прочитай про позднее связывание
            //ToolsModelDb.Create(new Idea() { Title = "123" });
            //var idd = ToolsModelDb.Read<Idea>("{}").ResultRead[0].Id;
            //var result = ToolsModelDb.GetById<Idea>(idd);

            //Assert.IsTrue(result.ResultRead[0].Id == idd);
            //Assert.IsTrue(result.ResultRead[0] is IdeaDb);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ReadTest_positiv()
        {
            //ToolsModelDb.Create(new Idea() { Title = "123" });
            //var result = ToolsModelDb.Read<Idea>();

            //Assert.IsTrue(result.ResultRead.Count() != 0);
            //foreach(var item in result.ResultRead) 
            //{
            //    Assert.IsTrue(item is IdeaDb);
            //}
            Assert.IsTrue(true);
        }
    }
}