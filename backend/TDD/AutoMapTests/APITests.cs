﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GenericToolsEntitys;
using Models.Models;
using Models.ModelDb;

namespace Models.Tests
{
    [TestClass()]
    public class APITests
    {
        GenericController<Idea, Context> _controller;

        [TestMethod()]
        public void GetElementTests_get_by_id()
        {
            var buff = new Idea() { Title = "For test" };
            ToolsModel.Create(buff);
            var item = ToolsModel.Read<Idea>().ResultRead[0];

            Assert.IsTrue(item != null);
        }

        [TestMethod()]
        public void APITests_get_by_id()
        {
            _controller = new();

            var buff = new Idea() { Title = "For test" };
            ToolsModel.Create(buff);
            var item = ToolsModel.Read<Idea>().ResultRead[0];

            Assert.IsTrue(item != null);

            var result = _controller.GetById(item.Id);

            Assert.IsTrue(result != null);
        }

        [TestMethod()]
        public void APITests_post()
        {
            _controller = new();

            var old_count = ToolsModel.Read<Idea>(0, int.MaxValue).ResultRead.Count;

            var item = new Idea() { Title = "For test" };
            _controller.Post(item);
            var new_count = ToolsModel.Read<Idea>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(old_count != new_count);

        }

        [TestMethod()]
        public void APITests_delete()
        {
            _controller = new();

            var old_count = ToolsModel.Read<Idea>().ResultRead.Count;

            var item = new Idea() { Title = "For test" };
            _controller.Post(item);
            var id = ToolsModel.Read<Idea>().ResultRead[0].Id;
            ToolsModel.Delete<Idea>(id);
            var new_count = ToolsModel.Read<Idea>().ResultRead.Count;

            Assert.IsTrue(old_count == new_count);

        }

        [TestMethod()]
        public void APITests_put()
        {
            _controller = new();

            var item = ToolsModel.Read<Idea>().ResultRead[0];

            var old_title = item.Title;

            item.Title += "_1";
             ToolsModel.Update<Idea>(item);

            item = ToolsModel.Read<Idea>().ResultRead[0];

            var new_title = item.Title;

            Assert.IsTrue(old_title != new_title);

        }

        [TestMethod()]
        public void APITests_get()
        {
            _controller = new();

            var item = ToolsModel.Read<Idea>().ResultRead[0];
            _controller.Post(item);
            var result = ToolsModel.Read<Idea>();

            Assert.IsTrue(result.ResultRead != null);

        }

        [TestMethod()]
        public void APITests_get_Db()
        {
            _controller = new();
            var old_count = ToolsModel.Read<Idea>(0, int.MaxValue).ResultRead.Count;
            for (int i = 0; i < 10; i++)
                _controller.Post(new IdeaDb($"Title_{i}"));

            var new_count = ToolsModel.Read<Idea>(0, int.MaxValue).ResultRead.Count;

            Assert.IsTrue(old_count != new_count);
        }

        [TestMethod()]
        public void APITests_get_More()
        {
            _controller = new();
            for (int i = 0; i < 10; i++)
                _controller.Post(new IdeaDb($"Title_{i}"));

            var arr = _controller.Get("{\"Title\":\"Title\"}", 0, 10);

            Assert.IsTrue(arr.Count == 10);
            for (int i = 0; i < 10; i++)
                Assert.IsTrue(arr[i].Title.IndexOf("Title") != -1);
        }

        [TestMethod()]
        public void APITests_get_real()
        {
            _controller = new();
            for (int i = 0; i < 10; i++)
                _controller.Post(new IdeaDb($"Title_{i}"));

            var arr = _controller.Get("{\"Title\": \"Title\"}", 0, 10);

            Assert.IsTrue(arr.Count == 10);
            for (int i = 0; i < 10; i++)
                Assert.IsTrue(arr[i].Title.IndexOf("Title") != -1);
        }
    }
}