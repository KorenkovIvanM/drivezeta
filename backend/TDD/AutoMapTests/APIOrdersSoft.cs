﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GenericToolsEntitys;
using Models.Models;
using Models.ModelDb;
using TDD.Controllers;
using Models;

namespace Models.Tests
{
    [TestClass]
    public class APIOrdersSoft
    {
        OrderSoftController _controller;

        [TestMethod]
        public void APITests_children()
        {
            _controller = new();
            //if (result == null || result.Count == 0)
            _controller.Post(new OrderSoftDb("LAWER"));
            var order = _controller.Get("{\"Title\":\"LAWER\"}")[0];
            var childe = new OrderSoftDb("LAWER");
            childe.ParentId = order.Id;
            _controller.Post(childe);
            var result = _controller.Get("{\"Title\":\"LAWER\"}");
            bool _result = false;
            for (int i = 0; i < result.Count; i++)
                _result = _result || (result[i].Children != null);
            Assert.IsTrue(_result);
        }
        [TestMethod()]
        public void DeleteTest_with_children()
        {
            var count_old = ToolsModel.Read<OrderSoft>().ResultRead.Count;
            OrderSoftDb o1 = new OrderSoftDb();
            OrderSoftDb o2 = new OrderSoftDb();
            ToolsModel.Create<OrderSoft>(o1);
            o2.ParentId = o1.Id;
            ToolsModel.Create<OrderSoft>(o2);
            var buff = ToolsModel.GetById<OrderSoft>(o1.Id).ResultRead[0];
            buff.DeleteChildren();
            var count_new = ToolsModel.Read<OrderSoft>().ResultRead.Count;
            Assert.IsTrue(count_new == count_old);
        }
    }
}
