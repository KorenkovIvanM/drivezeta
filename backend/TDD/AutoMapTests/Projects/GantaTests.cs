﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Models;

namespace Models.Projects.Tests
{
    [TestClass()]
    public class GantaTests
    {
        [TestMethod()]
        public void GantaTest()
        {
            Project project = new Project()
            {
                Title = "Test project",
            };
            ToolsModel.Create(project);
            project = ToolsModel.Read<Project>("{\"Title\":\"" + project.Title + "\"}").ResultRead[0];
            Ganta ganta = new(project.Id);
            Assert.IsTrue(ganta != null);
        }

        [TestMethod()]
        public void GantaTest_not_null_graph()
        {
            var project = ToolsModel.Read<Project>("{\"Title\":\"Test project\"}").ResultRead[0];
            Ganta ganta = new(project.Id);

            var order_1 = new OrderSoft();
            order_1.ProjectId = project.Id;
            var order_2 = new OrderSoft();
            order_2.ProjectId = project.Id;
            
            ToolsModel.Create(order_1);
            ToolsModel.Create(order_2);

            var link = new Link()
            {
                OrderBein = order_1.Id,
                OrderEnd = order_2.Id,
            };

            ToolsModel.Create(link);

            Assert.IsTrue(ganta.Orders != null);
            Assert.IsTrue(ganta.Links != null);
        }

        [TestMethod()]
        public void GantaTest_Not_start_stop()
        {
            var project = ToolsModel.Read<Project>("{\"Title\":\"Test project\"}").ResultRead[0];
            Ganta ganta = new(project.Id);

            var order_1 = new OrderSoft();
            order_1.ProjectId = project.Id;
            var order_2 = new OrderSoft();
            order_2.ProjectId = project.Id;

            ToolsModel.Create(order_1);
            ToolsModel.Create(order_2);

            var link = new Link()
            {
                OrderBein = order_1.Id,
                OrderEnd = order_2.Id,
            };

            ToolsModel.Create(link);

            Assert.IsTrue(ganta.Orders != null);
            Assert.IsTrue(ganta.Links != null);
            Assert.IsTrue(ganta.Orders[0].Status != null);
            Assert.IsTrue(ganta.Orders[0].Stop != null);
        }

        [TestMethod()]
        public void GantaTest_Sort()
        {
            var project = ToolsModel.Read<Project>("{\"Title\":\"Test project\"}").ResultRead[0];
            Ganta ganta = new(project.Id);

            var order_1 = new OrderSoft();
            order_1.ProjectId = project.Id;
            var order_2 = new OrderSoft();
            order_2.ProjectId = project.Id;

            ToolsModel.Create(order_1);
            ToolsModel.Create(order_2);

            var link = new Link()
            {
                OrderBein = order_1.Id,
                OrderEnd = order_2.Id,
            };

            ToolsModel.Create(link);

            var result = ganta.SortGanta();

            Assert.IsTrue(result != null);
        }
    }
}