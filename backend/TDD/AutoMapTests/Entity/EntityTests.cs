﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using AutoMapTests.ForTest;
using actEnt = AutoMap.ActionEntity;
using Models.Models;

namespace AutoMap.Entity.Tests
{
    [TestClass()]
    public class EntityTests
    {
        #region for Test
        [TestMethod()]
        public void for_activ_action_test()
        {
            //Arrange
            int id = 1;
            Entitys entities = new Entitys() { Id = id };
            //Act
            int? r1 = actEnt.ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);
        }
        #endregion
        #region Idea
        [TestMethod()]
        public void Idea_Get_Id()
        {
            //Arrange
            int id = 12;
            Idea idea = new()
            {
                Id = id,
                Description = "123",
                Title = "My same idea",
            };
            //Act
            int? result = actEnt.ActionEntity.GetId(idea);
            //Assert
            Assert.AreEqual(idea.Id, id);
        }

        [TestMethod()]
        public void Idea_Update()
        {
            //Arrange
            Idea idea_in = new()
            {
                Id = 1,
                Description = "123",
                Title = "My same idea",
            };
            Idea idea_out = new()
            {
                Id = 111,
                Title = "title idea out",
            };
            //Act
            actEnt.ActionEntity.Update(ref idea_in, ref idea_out);
            //Assert
            Assert.AreEqual(idea_in.Id, idea_out.Id);
            Assert.AreEqual(idea_in.Title, idea_out.Title);
            Assert.AreEqual(idea_in.Description, "123");
        }

        [TestMethod()]
        public void Idea_Update_with_json()
        {
            //Arrange
            Idea idea_in = new()
            {
                Id = 1,
                
                Title = "My same idea",
            };
            Idea idea_out = new()
            {
                Id = 111,
                Title = "title idea out",
                Description = "123",
            };
            //Act
            actEnt.ActionEntity.Update(ref idea_in, ref idea_out, "{\"Id\": 12, \"Title\":\"123\"}");
            //Assert
            Assert.AreEqual(idea_in.Id, idea_out.Id);
            Assert.AreEqual(idea_in.Title, idea_out.Title);
            Assert.AreEqual(idea_in.Description, null);
        }

        [TestMethod()]
        public void Idea_IsMask()
        {
            //Arrange
            Idea idea_in = new()
            {
                Id = 111,
                Title = "title idea out",
                Description = "123",
            };
            Idea
                mask_1 = new() { Title = "tit", },
                mask_2 = new() { Description = "23", },
                mask_3 = new() { Id = 2, };
            //Act
            //Assert
            Assert.IsTrue(!actEnt.ActionEntity.IsMask(mask_3, idea_in));
            Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_3, idea_in, "{\"Title\":\"123\"}"));
            Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_1, idea_in, "{\"Title\":\"123\"}"));
            Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_2, idea_in, "{\"Description\":\"123\"}"));
        }

        // TODO make where will be modelDb
        //[TestMethod()]
        //public void Idea_AutoMapper()
        //{
        //    //Arrange
        //    Idea idea_in = new()
        //    {
        //        Id = 111,
        //        Title = "title idea out",
        //        Description = "123",
        //    };
        //    Idea
        //        mask_1 = new() { Title = "tit", },
        //        mask_2 = new() { Description = "23", },
        //        mask_3 = new() { Id = 2, };
        //    //Act
        //    //Assert
        //    Assert.IsTrue(!actEnt.ActionEntity.IsMask(mask_3, idea_in));
        //    Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_3, idea_in, "{\"Title\":\"123\"}"));
        //    Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_1, idea_in, "{\"Title\":\"123\"}"));
        //    Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_2, idea_in, "{\"Description\":\"123\"}"));
        //}
        #endregion
        #region Order
        [TestMethod()]
        public void Order_get_id()
        {
            //Arrange
            int id = 12;
            Order order = new()
            {
                Id = id,
                Description = "123",
                Title = "My same idea",
            };
            //Act
            int? result = actEnt.ActionEntity.GetId(order);
            //Assert
            Assert.AreEqual(order.Id, id);
        }
        [TestMethod()]
        public void Order_update()
        {
            //Arrange
            Order order_in = new()
            {
            };
            Order order_out = new()
            {
                Id = 12,
                Description = "123",
                Title = "My same idea",
            };
            //Act
            actEnt.ActionEntity.Update(ref order_in, ref order_out);
            //Assert
            Assert.AreEqual(order_in.Id, order_out.Id);
            Assert.AreEqual(order_in.Description, order_out.Description);
            Assert.AreEqual(order_in.Title, order_out.Title);
        }
        [TestMethod()]
        public void Order_update_with_null()
        {
            //Arrange
            Order order_in = new()
            {
                Description = "123",
            };
            Order order_out = new()
            {
                Id = 12,
                End = DateTime.Today,
                Title = "My same idea",
            };
            //Act
            actEnt.ActionEntity.Update(ref order_in, ref order_out);
            //Assert
            Assert.AreEqual(order_in.Id, order_out.Id);
            Assert.AreEqual(order_in.Description, "123");
            Assert.AreEqual(order_out.Description, null);
            Assert.AreEqual(order_in.Title, order_out.Title);
            Assert.AreEqual(order_in.End, order_out.End);
        }

        [TestMethod()]
        public void Order_mask()
        {

            Order order_in = new()
            {
                Id = 12,
                End = DateTime.Today,
                Title = "My same idea",
            };
            Order 
                mask_1 = new() { Id = 12, },
                mask_2 = new() { Title = "My", };

            Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_1, order_in));
            Assert.IsTrue(!actEnt.ActionEntity.IsMask(mask_2, order_in));
            Assert.IsTrue(actEnt.ActionEntity.IsMask(mask_2, order_in, "{\"Title\":\"123\"}"));
        }
        #endregion
    }
}
