﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapTests.ForTest
{
    public class EntityOut
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Create { get; set; }
        public int MyItemOut { get; set; }
    }
}
