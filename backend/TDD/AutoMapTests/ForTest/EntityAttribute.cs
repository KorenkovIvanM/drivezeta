﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMap.ActionEntity.Settings;

namespace AutoMapTests.ForTest
{
    public class EntityAttribute
    {
        public int Id { get; set; }
        [SQLLike]
        public int? Value { get; set; }
        [SQLEquales]
        public string? Name { get; set; }
        [SQLLike]
        public string? Description { get; set; }
        [SQLNot]
        public string? Histore { get; set; }
        [SQLIgnore]
        public int? Count { get; set; }
        [SQLIncludePeriod(TypeIncludePeriod.Year)]
        public DateTime? _Year { get; set; }
        [SQLIncludePeriod(TypeIncludePeriod.Month)]
        public DateTime? _Month { get; set; }
        [SQLIncludePeriod(TypeIncludePeriod.Week)]
        public DateTime? _Week { get; set; }
        [SQLIncludePeriod(TypeIncludePeriod.Day)]
        public DateTime? _Day { get; set; }
        [SQLIncludePeriod(TypeIncludePeriod.House)]
        public DateTime? _House { get; set; }
    }
}
