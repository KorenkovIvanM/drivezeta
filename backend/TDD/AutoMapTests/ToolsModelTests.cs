﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Models;

namespace Models.Tests
{
    [TestClass()]
    public class ToolsModelTests
    {
        #region Create
        [TestMethod()]
        public void CreateTest()
        {
            bool result = true;
            try
            {
                ToolsModel.Create(new Idea() { Title = "123"});
            }
            catch (Exception ex)
            {
                result = false;
            }
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void CreateTest_not_null_id()
        {
            ToolsModel.Create(new Idea() { Id = 1, Title = "Test idea" });
            try
            {
                ToolsModel.Create(new Idea() { Id = 1, Title = "Test idea" });
            }
            catch(Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod()]
        public void CreateTest_to_list()
        {
            List<Idea> lIdea = new List<Idea>();
            for(int i = 0; i < 10; i++)
                lIdea.Add(new Idea() { Title = "10"});
            Assert.IsTrue(ToolsModel.Create(lIdea).Result);
        }

        [TestMethod()]
        public void CreateTest_not_table()
        {
            List<int> lIdea = new List<int>();
            for (int i = 0; i < 10; i++)
                lIdea.Add(12);
            var result = ToolsModel.Create(lIdea);
           
            Assert.IsTrue(!result.Result);
        }
        #endregion
        #region Rread
        [TestMethod()]
        public void ReadTest_to_list()
        {
            bool result = true;
            try
            {
                var _result = ToolsModel.Read<Idea>("");
            }
            catch(Exception ex)
            {
                result = false;
            }
            
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void ReadTest_on_count()
        {
            ToolsModel.Create<Idea>(new Idea() { Title = "123" });
            var result = ToolsModel.Read<Idea>("{\"Title\":\"123\"}");

            Assert.IsTrue(result.ResultRead.Count() != 0);
        }
        
        [TestMethod()]
        public void ReadTest_on_count_for_id()
        {
            var result = ToolsModel.Read<Idea>("{\"Id\": 1}");

            Assert.IsTrue(result.ResultRead.Count() == 1 || result.ResultRead.Count() == 0);
        }
        [TestMethod()]
        public void ReadTest_page()
        {
            List<Idea> lIdea = new List<Idea>();
            for (int i = 0; i < 100; i++)
                lIdea.Add(new Idea() { Title = "10" });
            ToolsModel.Create(lIdea);
            Assert.IsTrue(ToolsModel.Read<Idea>("{}").ResultRead.Count() <= 15);
        }

        [TestMethod()]
        public void ReadTest_page_oteer_values()
        {
            List<Idea> lIdea = new List<Idea>();
            for (int i = 0; i < 100; i++)
                lIdea.Add(new Idea() { Title = "10" });
            ToolsModel.Create(lIdea);
            Assert.IsTrue(ToolsModel.Read<Idea>(1, 20).ResultRead.Count() == 20);
        }

        #endregion
        #region GetById
        [TestMethod()]
        public void GetByIdTest_positiv()
        {
            ToolsModel.Create(new Idea() { Title = "123" });
            var idd = ToolsModel.Read<Idea>("{}").ResultRead[0].Id;
            var result = ToolsModel.GetById<Idea>(idd);

            Assert.IsTrue(result.ResultRead[0].Id == idd);
        }

        [TestMethod()]
        public void GetByIdTest_not_taable()
        {
            bool result = true;
            try
            {
                ToolsModel.GetById<int>(4);
            }
            catch (Exception ex)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void GetByIdTest_negativ()
        {
            var result = ToolsModel.GetById<Idea>(-4);

            Assert.IsFalse(result.Result);
        }
        #endregion
        #region Update
        [TestMethod()]
        public void UpdateTest_positiv()
        {
            var table = ToolsModel.Read<Idea>("{}").ResultRead[0];
            var idd = table.Id;
            var result = ToolsModel.GetById<Idea>(idd).ResultRead[0];
            result.Title = $"{result.Title}+1";
            var t = ToolsModel.Update(result);

            Assert.IsTrue(t.Result);
        }

        [TestMethod()]
        public void UpdateTest_positiv_json()
        {
            var table = ToolsModel.Read<Idea>("{}").ResultRead[0];
            var idd = table.Id;
            var result = ToolsModel.GetById<Idea>(idd).ResultRead[0];
            var old = result.Title;
            result.Title = $"{result.Title}+1";
            var t = ToolsModel.Update(result, "{}");
            result = ToolsModel.GetById<Idea>(idd).ResultRead[0];
            var neww = result.Title;

            Assert.IsTrue(t.Result);
            Assert.IsTrue(old == neww);
        }

        [TestMethod()]
        public void UpdateTest_not_table()
        {
            var result = ToolsModel.Update(12);

            Assert.IsTrue(!result.Result);
        }

        [TestMethod()]
        public void UpdateTest_move_id()
        {
            var table = ToolsModel.Read<Idea>("{}");
            var idd = table.ResultRead[0].Id;
            var result = ToolsModel.Update(new Idea() {Id = idd, Description = "123"}, "{}");

            Assert.IsTrue(result.Result);
        }

        [TestMethod()]
        public void UpdateTest_move_description()
        {
            var table = ToolsModel.Read<Idea>("{}");
            var idd = table.ResultRead[0].Id;
            var result = ToolsModel.Update(new Idea() { Id = idd, Description = "123" }, "{\"Description\": \"123\"}");

            Assert.IsTrue(result.Result);
        }
        #endregion
        #region Delete
        [TestMethod()]
        public void DeleteTest_delete()
        {
            var result = ToolsModel.Read<Idea>("{}");
            foreach(var item in result.ResultRead)
            {
                Assert.IsTrue(ToolsModel.Delete<Idea>(item?.Id ?? 0).Result);
            }
        }
        #endregion
    }
}