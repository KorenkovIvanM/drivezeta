﻿using AutoMap.ActionEntity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using AutoMapTests.ForTest;
using Models.Models;
using Models.ModelDb;

namespace AutoMap.ActionEntity.Tests
{
    [TestClass()]
    public class ActionEntityTests
    {
        #region GetId
        [TestMethod()]
        public void GetIdTest_Positiv()
        {
            //Arrange
            int id = 1;
            Entitys entities = new Entitys() { Id = id };
            //Act
            int? r1 = ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);
        }
        [TestMethod()]
        public void GetIdTest_Array_Positiv()
        {
            //Arrange
            int Count = 100;
            int[] listId = new int[Count];
            Entitys[] entities = new Entitys[Count];
            Random rnd = new Random();
            for (int i = 0; i < Count; i++)
            {
                listId[i] = rnd.Next(100, 1000);
                entities[i] = new Entitys();
                entities[i].Id = listId[i];
            }

            //Act
            int?[] result = new int?[Count];
            for (int i = 0; i < Count; i++)
                result[i] = ActionEntity.GetId(entities[i]);
            //Assert
            for (int i = 0; i < Count; i++)
                Assert.AreEqual(listId[i], result[i]);

        }

        [TestMethod()]
        public void GetIdTest_Null()
        {
            //Arrange
            string id = "1";
            EntityWhithOutId entities = new EntityWhithOutId() { Id = id };
            bool flag = true;

            // Act
            try
            {
                ActionEntity.GetId(entities);
            }
            catch (Exception e)
            {
                flag = false;
            }


            //Assert
            Assert.AreEqual(flag, false);

        }

        [TestMethod()]
        public void GetIdTest_Not_Id()
        {
            //Arrange
            EntityPast entities = new EntityPast() { Name = "---" };
            string message = "";

            // Act
            try
            {
                ActionEntity.GetId(entities);
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            //Assert
            Assert.AreEqual("Not Id", message);
        }

        [TestMethod()]
        public void GetIdTest_littl_id()
        {
            int id = 7;
            //Arrange
            EntitysLittl entities = new EntitysLittl() { id = id };

            int? r1 = ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);

        }

        [TestMethod()]
        public void GetIdTest_faild_id()
        {
            int id = 7;
            //Arrange
            EntitysFailed entities = new EntitysFailed() { Id = id };

            int? r1 = ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);

        }
        [TestMethod()]
        public void GetIdTest_faild_little_id()
        {
            int id = 7;
            //Arrange
            EntityLittleFaild entities = new EntityLittleFaild() { id = id };

            int? r1 = ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);

        }
        [TestMethod()]
        public void GetIdTest_faild_for_mnull()
        {
            int id = 73346;
            //Arrange
            EntityLittleFaild? entities = new EntityLittleFaild() { id = id };

            int? r1 = ActionEntity.GetId(entities);
            //Assert
            Assert.AreEqual(id, r1);

        }
        #endregion
        #region Update
        // Update
        [TestMethod()]
        public void UpdateTest()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "same name",
            };
            Entitys b = new();

            ActionEntity.Update(ref b, ref a);
            //Assert
            Assert.AreEqual(a.Id, b.Id);
            Assert.AreEqual(a.Name, b.Name);
        }

        [TestMethod()]
        public void UpdateTest_On_NotNull()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "same name",
            };
            Entitys b = new()
            {
                Id = 2,
                dateTime = DateTime.Now,
            };

            ActionEntity.Update(ref b, ref a);
            //Assert
            Assert.AreEqual(a.Id, b.Id);
            Assert.AreEqual(a.Name, b.Name);
            Assert.IsTrue(a.dateTime != b.dateTime);
        }

        [TestMethod()]
        public void UpdateTest_On_Null_Structl()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "same name",
            };
            Entitys b = new()
            {
                Id = 2,
                dateTime2 = DateTime.Now,
            };

            ActionEntity.Update(ref b, ref a);
            //Assert
            Assert.AreEqual(a.Id, b.Id);
            Assert.AreEqual(a.Name, b.Name);
            Assert.IsTrue(a.dateTime2 != b.dateTime2);
        }

        [TestMethod()]
        public void UpdateTest_On_Enum()
        {
            //Arrange
            Entitys a = new()
            {
                ttt = TTT.item2,
            };
            Entitys b = new();

            ActionEntity.Update(ref a, ref b);
            //Assert
            Assert.AreEqual(a.ttt, b.ttt);
        }

        [TestMethod()]
        public void UpdateTest_For_Mask()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
            };
            Entitys b = new();

            ActionEntity.Update(ref a, ref b, "{\"Iddr\":12}");
            //Assert
            Assert.IsTrue(a.Id != b.Id);
        }

        [TestMethod()]
        public void UpdateTest_For_OtherId()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
            };
            Entitys b = new();

            ActionEntity.Update(ref a, ref b, "{\"UserId\":10}");
            //Assert
            Assert.IsTrue(a.Id != b.Id);
        }

        [TestMethod()]
        public void UpdateTest_For_OtherAll()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "123",
                dateTime = DateTime.Now,
                dateTime2 = DateTime.Now.AddDays(1),
                ttt = TTT.item2,
            };
            Entitys b = new();

            ActionEntity.Update(ref a, ref b, "{}");
            //Assert
            Assert.IsTrue(a.Id != b.Id);
            Assert.IsTrue(a.Name != b.Name);
            Assert.IsTrue(a.dateTime != b.dateTime);
            Assert.IsTrue(a.dateTime2 != b.dateTime2);
            Assert.IsTrue(a.ttt != b.ttt);
        }

        [TestMethod()]
        public void UpdateTest_For_not_OtherAll()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "123",
                dateTime = DateTime.Now,
                dateTime2 = DateTime.Now.AddDays(1),
                ttt = TTT.item2,
            };
            Entitys b = new();

            ActionEntity.Update(ref b, ref a, "{\"Id\":1, \"Name\":\"12\", \"dateTime\":\"2022-11-23T06:34:27.038Z\", \"dateTime2\":\"2022-11-23T06:34:27.038Z\", \"ttt\":1}");
            //Assert
            Assert.IsTrue(a.Id == b.Id);
            Assert.IsTrue(a.Name == b.Name);
            Assert.IsTrue(a.dateTime == b.dateTime);
            Assert.IsTrue(a.dateTime2 == b.dateTime2);
            Assert.IsTrue(a.ttt == b.ttt);
        }

        [TestMethod()]
        public void UpdateTest_For_not_id()
        {
            //Arrange
            Entitys a = new()
            {
                Id = 1,
                Name = "123",
                dateTime = DateTime.Now,
                dateTime2 = DateTime.Now.AddDays(1),
                ttt = TTT.item2,
            };
            Entitys b = new();

            ActionEntity.Update(ref a, ref b, "{\"Idddddddd\":12}");
            //Assert
            Assert.IsTrue(a.Id != b.Id);
            Assert.IsTrue(a.Name != b.Name);
            Assert.IsTrue(a.dateTime != b.dateTime);
            Assert.IsTrue(a.dateTime2 != b.dateTime2);
            Assert.IsTrue(a.ttt != b.ttt);
        }

        [TestMethod()]
        public void UpdateTest_For_not_json()
        {
            //Arrange
            string message = "";
            EntityPast a = new()
            {
                Name = "123",
                value = 10,

            };
            EntityPast b = new();

            // Act
            try
            {
                ActionEntity.Update(ref a, ref b, "{Idddddddd}");
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            //Assert
            Assert.AreEqual("Not JSON", message);
        }

        [TestMethod()]
        public void UpdateTest_For_not_little_registr()
        {
            //Arrange
            string message = "";
            EntityPast a = new()
            {
                Name = "123",
                value = 10,

            };
            EntityPast b = new();

            // Act
            ActionEntity.Update(ref b, ref a, "{\"name\": \"\"}");


            //Assert
            Assert.AreEqual(a.Name, b.Name);
        }
        #endregion
        #region IsMask
        [TestMethod()]
        public void IsMaskTest()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whis_null()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                Name = "123"
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whis_string()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                Name = "1",
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                Name = "123"
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                Name = "1",
                EnIItem = En.a10
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                Name = "123",
                EnIItem = En.a1
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_not_enum()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                Name = "1",
                EnIItem = En.a10
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                Name = "123",
                EnIItem = En.a10
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            Console.WriteLine(result);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum_null_for_mask_not_null()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                Name = "1",
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum_null_for_mask_not_null_enum()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                EnIItem = En.a10
            };
            EntitysForMask exaple = new()
            {
                Id = 1,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum_null_for_mask_not_null_date()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,

            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                dt = new(),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum_null_for_mask_not_null_date_in_mask()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                dt = new(),

            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                dt = new(),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_whith_enum_null_for_mask_not_null_date_in_mask_failed()
        {
            //Arrange
            EntitysForMask mask = new()
            {
                Id = 1,
                dt = DateTime.Now,

            };
            EntitysForMask exaple = new()
            {
                Id = 1,
                dt = DateTime.Now.AddDays(100),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }

        [TestMethod()]
        public void IsMaskTest_attribute_like()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Value = 1,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Value = 123,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_attribute_equale()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Name = "1",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Name = "11212",
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }

        [TestMethod()]
        public void IsMaskTest_attribute_not_equale()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Name = "11212",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Name = "11212",
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_attribute_not_equale_not_attribute()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Description = "1",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Description = "11212",
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_not_attribute()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Histore = "1",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Histore = "11212",
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_ignor()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Count = 1,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Count = 2,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_ignor_like()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Count = 1,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Count = 12,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_ignor_null()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Count = 1,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_Date_Year_positive()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Year = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Year = DateTime.Now.AddDays(10),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_Date_Year_negativ()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Year = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Year = DateTime.Now.AddDays(1000),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }

        [TestMethod()]
        public void IsMaskTest_Date_Month_positive()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Month = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Month = DateTime.Now.AddDays(1),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_Date_Month_negativ()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Month = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Month = DateTime.Now.AddDays(100),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }



        [TestMethod()]
        public void IsMaskTest_Date_Week_positive()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Week = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Week = DateTime.Now.AddDays(1),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            // TODO исправь
            Assert.IsTrue(true);
        }
        [TestMethod()]
        public void IsMaskTest_Date_Week_negativ()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Week = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Week = DateTime.Now.AddDays(100),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result); //TODO исправь меня
        }

        [TestMethod()]
        public void IsMaskTest_Date_Day_positive()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Day = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Day = DateTime.Now,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_Date_Day_negativ()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _Day = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _Day = DateTime.Now.AddDays(100),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }



        [TestMethod()]
        public void IsMaskTest_Date_House_positive()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _House = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _House = DateTime.Now,
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void IsMaskTest_Date_House_negativ()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                _House = DateTime.Now,

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                _House = DateTime.Now.AddDays(1),
            };

            bool result = ActionEntity.IsMask(mask, exaple);
            //Assert
            Assert.IsTrue(!result);
        }

        [TestMethod()]
        public void IsMaskTest_Json()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Name = "123",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
            };

            bool result = ActionEntity.IsMask(mask, exaple, "{}");
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void IsMaskTest_Json_positiv()
        {
            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Name = "123",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Name = "12",
            };

            bool result = ActionEntity.IsMask(mask, exaple, "{\"Name\": \"123\"}");
            //Assert
            Assert.IsTrue(!result);
        }

        [TestMethod()]
        public void IsMaskTest_Json_error()
        {
            //Arrange

            //Arrange
            EntityAttribute mask = new()
            {
                Id = 1,
                Name = "123",

            };
            EntityAttribute exaple = new()
            {
                Id = 1,
                Name = "12",
            };
            string message = "";

            // Act
            try
            {
                ActionEntity.IsMask(mask, exaple, "}");
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            //Assert
            Assert.AreEqual(message, "Not JSON");
        }
        #endregion
        #region AutoMapper
        [TestMethod()]
        public void AutoMapperTest()
        {
            //Arrange
            EntityIn _in = new()
            {
                Id = 1,
                Name = "same name",
                MyItemIn = 10,
            };
            EntityOut _out = new()
            {
                Id = 11,
                Name = "same nam123123123123e",
                MyItemOut = 1024,
            };

            ActionEntity.AutoMappper(ref _in, ref _out);
            //Assert
            Assert.AreEqual(_in.Id, _out.Id);
            Assert.AreEqual(_in.Name, _out.Name);
            Assert.AreEqual(_in.MyItemIn, 10);
            Assert.AreEqual(_out.MyItemOut, 1024);
        }

        [TestMethod()]
        public void AutoMapperTest_With_json()
        {
            //Arrange
            EntityIn _in = new()
            {
                Id = 1,
                Name = "same name",
                MyItemIn = 10,
            };
            EntityOut _out = new()
            {
                Id = 11,
                Name = "same nam123123123123e",
                MyItemOut = 1024,
            };

            ActionEntity.AutoMappper(ref _in, ref _out, "{\"Name\": \"12\"}");
            //Assert
            Assert.IsTrue(_in.Id != _out.Id);
            Assert.IsTrue(_in.Name == _out.Name);
            Assert.AreEqual(_in.MyItemIn, 10);
            Assert.AreEqual(_out.MyItemOut, 1024);
        }

        [TestMethod()]
        public void AutoMapperTest_With_json_and_unikitem_out()
        {
            //Arrange
            EntityIn _in = new()
            {
                Id = 1,
                Name = "same name",
                MyItemIn = 10,
            };
            EntityOut _out = new()
            {
                Id = 11,
                Name = "same nam123123123123e",
                MyItemOut = 1024,
            };

            ActionEntity.AutoMappper(ref _in, ref _out, "{\"Name\": \"12\", \"MyItemOut\": 2}");
            //Assert
            Assert.IsTrue(_in.Id != _out.Id);
            Assert.IsTrue(_in.Name == _out.Name);
            Assert.AreEqual(_in.MyItemIn, 10);
            Assert.AreEqual(_out.MyItemOut, 1024);
        }

        [TestMethod()]
        public void AutoMapperTest_With_json_and_unikitem_in()
        {
            //Arrange
            EntityIn _in = new()
            {
                Id = 1,
                Name = "same name",
                MyItemIn = 10,
            };
            EntityOut _out = new()
            {
                Id = 11,
                Name = "same nam123123123123e",
                MyItemOut = 1024,
            };

            ActionEntity.AutoMappper(ref _in, ref _out, "{\"Name\": \"12\", \"MyItemIn\": 2}");
            //Assert
            Assert.IsTrue(_in.Id != _out.Id);
            Assert.IsTrue(_in.Name == _out.Name);
            Assert.AreEqual(_in.MyItemIn, 10);
            Assert.AreEqual(_out.MyItemOut, 1024);
        }

        [TestMethod()]
        public void AutoMapperTest_With_json_care()
        {
            //Arrange
            EntityIn _in = new()
            {
                Id = 1,
                Name = "same name",
                MyItemIn = 10,
            };
            EntityOut _out = new()
            {
                Id = 11,
                Name = "same nam123123123123e",
                MyItemOut = 1024,
            };

            ActionEntity.AutoMappper(ref _in, ref _out, "{}");
            //Assert
            Assert.IsTrue(_in.Id != _out.Id);
            Assert.IsTrue(_in.Name != _out.Name);
            Assert.AreEqual(_in.MyItemIn, 10);
            Assert.AreEqual(_out.MyItemOut, 1024);
        }
        #endregion
    }
}