using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;

namespace AutoCRUD;

[ApiController]
[Route("[controller]")]

/// <summary>
/// Generic Json Controller Get json file for new entity
/// </summary>
/// <typeparam name="T1">Dependent entity</typeparam>
/// <typeparam name="T2">Fundament entity</typeparam>
/// <typeparam name="C">Context</typeparam>
public class GenericJsonController<T1, T2, C> : ControllerBase where C: DbContext, new()   
{
    private (dynamic, dynamic) _setData(ref C context)
    {
        Type 
            itemType1 = typeof(T1),
            itemType2 = typeof(T2),
            contextType = typeof(C);
        FieldInfo 
            field1 = contextType.GetField($"<{itemType1}s>k__BackingField"),
            field2 = contextType.GetField($"<{itemType2}s>k__BackingField");

        foreach (FieldInfo index in contextType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            if(index.Name.IndexOf($"<{itemType1.Name}s>") != -1)
                field1 = index;
        foreach (FieldInfo index in contextType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            if(index.Name.IndexOf($"<{itemType2.Name}s>") != -1)
                field2 = index;

        return (field1?.GetValue(context), field2?.GetValue(context));
    }
    
    [HttpGet("{mask}")]
    public string get(string mask, int numberPage = 0, int sizePage = 15)
    {
        try
        {
            //T? _mask = JsonSerializer.Deserialize<T>(mask);
            C context = new();
            var c_table = _setData(ref context);
            dynamic table1 = c_table.Item1;
            dynamic table2 = c_table.Item2;
            IEnumerable<T1> _tabe1 = table1;
            IEnumerable<T2> _tabe2 = table2;
            // table2.FromSqlRaw().ToList();
            var result = (
                from row1 in _tabe1 join row2 in _tabe2 on true equals true
                where true 
                    && (true)
                    //&& AutoMaper.IsMask<T>(_mask, row)
                select (row1, row2)).ToList();
            foreach (var item in result)
            {
                System.Console.WriteLine(item);
            }
            string json = JsonSerializer.Serialize(result);
                // .Skip(numberPage * sizePage)
                // .Take(sizePage);
            //_Read?.Invoke(ref result);
            // Dictionary<string, string> 
            //     _result = new();

            // for(int i = 0; i < result.Le)
            //     _result
            return json;
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return "";
        }
    }
}