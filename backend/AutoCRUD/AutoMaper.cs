using System.Reflection;

namespace AutoCRUD;

public static class AutoMaper
{
    public static Dictionary<string, string> JoinInJson<T1, T2> (ref T1 _item1, ref T2 _item2)
    {
        Dictionary<string, string> 
            result = new();
        
        Type 
            buffType1 = typeof(T1),
            buffType2 = typeof(T2);

        foreach (FieldInfo field in buffType1.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            result[field.Name] = field.GetValue(_item1).ToString();
        foreach (FieldInfo field in buffType2.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            result[field.Name] = field.GetValue(_item2).ToString();
            return result;
    }
    public static int? GetId<T>(T _item)
    {
        Type buffType = typeof(T);

        FieldInfo fieldId = buffType.GetField("<Id>", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);


        // TODO norm search
        foreach (FieldInfo index in buffType.GetFields(
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            if(index.Name.IndexOf("<Id>") != -1)
            {
                fieldId = index;
            }
        int? Id = (int?)fieldId?.GetValue(_item);
        return Id;
    }
    public static void Update<T>(ref T _in, ref T _out)
    {
        Type buffType = typeof(T);

        foreach (FieldInfo field in buffType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
        {
            
            var value_in = field?.GetValue(_in);
            var value_out = field?.GetValue(_out);

            
            if (value_out != null)
                field?.SetValue(_in, value_out);
        }
    }
    public static bool IsMask<T> (T _mask, T _example)
    {
        Type buffType = typeof(T);

        foreach (FieldInfo field in buffType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
        {
            var value_mask = field?.GetValue(_mask);
            var value_example = field?.GetValue(_example);

            bool 
                isLike = false, 
                isEquals = false;
            foreach (Attribute attr in field.GetCustomAttributes(true))
            {
                if (attr is DbLikeAttribute) isLike = true;
                if (attr is DbEqualsAttribute) isEquals = true;
            }

            // for Id
            // if(field.Name.IndexOf("<Id>") != -1)
            // {
                
            //     if((int)value_mask == 0)
            //         continue;
            //     else
            //         return ((int)value_mask == (int)value_example);
            // } 

            // Date
            if(value_mask is DateTime)
                continue;
                // if(
                //     ((DateTime)value_mask).Year != ((DateTime)value_example).Year || 
                //     ((DateTime)value_mask).Month != ((DateTime)value_example).Month ||
                //     ((DateTime)value_mask).Day != ((DateTime)value_example).Day
                // )
                //     return false;
                // else
                //     continue;

            // Integer
            // if(value_mask is int)
            //     if((int)value_mask != (int)value_example)
            //         return false;
            //     else
            //         continue;


            //System.Console.WriteLine($"name -> {field} mask -> {value_mask} origin ->{value_example}");
            // Enum
            if(value_mask is Enum && value_mask != null && value_example != null)
            {
                if((int)value_mask != (int)value_example)
                    return false;
                else
                    continue;
            }
                
            // if(value_mask is Event)
            // {
            //     System.Console.WriteLine($"mask -> {value_mask} origin ->{value_example}");
            //     if((int)value_mask != (int)value_example)
            //         return false;
            //     else
            //         continue;
            // }  

            // String
            if(value_mask is string)
                if(value_example == null || value_example.ToString().IndexOf(value_mask.ToString()) == -1)
                    return false;
                else
                    continue;
                

            // if((int)value_mask != (int)value_example)
            //         return false;
            //     else
            //         continue;
        }
        return true;
    }
}