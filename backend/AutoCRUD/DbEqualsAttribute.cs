namespace AutoCRUD;

[AttributeUsage(AttributeTargets.Property)]
class DbEqualsAttribute : Attribute
{
    public DbEqualsAttribute() { }
}