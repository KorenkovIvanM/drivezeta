namespace AutoCRUD;

[AttributeUsage(AttributeTargets.Property)]
class DbLikeAttribute : Attribute
{
    public DbLikeAttribute() { }
}