using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;

namespace AutoCRUD;

[ApiController]
[Route("[controller]")]

public class GenericController<T, C> : ControllerBase where C: DbContext, new() 
    
{
    public delegate bool AddItem(ref T item);
    public event AddItem _Create;
    public delegate void Decorator(ref IEnumerable<T> lItem);
    public event Decorator _Read;

    private dynamic _setData(ref C context)
    {
        Type 
            itemType = typeof(T), 
            contextType = typeof(C);
        FieldInfo field = contextType.GetField($"<{itemType}s>k__BackingField");

        foreach (FieldInfo index in contextType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            if(index.Name.IndexOf($"<{itemType.Name}s>") != -1)
                field = index;

        dynamic result = field?.GetValue(context);
        return result;
    }
    
    [HttpGet("{mask}")]
    public virtual IEnumerable<T> get(string mask, int numberPage = 0, int sizePage = 15)
    {
        try
        {
            T? _mask = JsonSerializer.Deserialize<T>(mask);
            C context = new();
            dynamic table = _setData(ref context);
            IEnumerable<T> _tabe = table;
            var result = (
                from row in _tabe
                where
                    AutoMaper.IsMask<T>(_mask, row)
                select row)
                .Skip<T>(numberPage * sizePage)
                .Take<T>(sizePage);
            _Read?.Invoke(ref result);
            return result;
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return new List<T>();
        }
    }

    [HttpGet("Id")]
    public virtual T? getById(int Id)
    {
        try
        {
            C context = new();
            dynamic table = _setData(ref context);
            return table.Find(Id);
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return default(T);
        }
    }

    // [HttpGet("Count")]
    // public int getCount()
    // {
    //     try
    //     {
    //         C context = new();
    //         dynamic table = _setData(ref context);
    //         return 10;//table.Count<T>();
    //     }
    //     catch(Exception ex)
    //     {
    //         System.Console.WriteLine(ex.Message);
    //         if(ex.InnerException != null)
    //             System.Console.WriteLine(ex.InnerException.Message);
    //         return 0;
    //     }
    // }

    [HttpPost]
    public virtual bool post(T item)
    {
        try
        {
            C context = new();
            dynamic table = _setData(ref context);
            if(_Create == null || _Create.Invoke(ref item))
                table.Add(item);
            else
                return false;
            context.SaveChanges();

            return true;
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return false;
        }
    }

    [HttpPut]
    public virtual bool put(T item)
    {
        try
        {
            C context = new();
            dynamic table = _setData(ref context);
            T origin = table.Find(AutoMaper.GetId<T>(item));
            
            AutoMaper.Update<T>(ref origin, ref item);
            context.SaveChanges();

            return true;
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return false;
        }
    }

    [HttpDelete]
    public virtual bool delete(int Id)
    {
        try
        {
            C context = new();
            dynamic table = _setData(ref context);
            table.Remove(table.Find(Id));
            context.SaveChanges();

            return true;
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
            if(ex.InnerException != null)
                System.Console.WriteLine(ex.InnerException.Message);
            return false;
        }
    }
}