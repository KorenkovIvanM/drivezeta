using System.ComponentModel.DataAnnotations;

public class TemplateOrders
{
    [Key]
    public int Id { get; set; }
    public Order? Order { get; set; }
    public int OrderId { get; set; }
    public User? User { get; set; }
    public int UserId { get; set; }
    public bool Actev { get; set; } = true;
    public bool monday { get; set; } = false;
    public bool tuesday { get; set; } = false;
    public bool wednesday { get; set; } = false;
    public bool thursday { get; set; } = false;
    public bool friday { get; set; } = false;
    public bool saturday { get; set; } = false;
    public bool Sunday { get; set; } = false;
    public DateTime? Time { get; set; }
}