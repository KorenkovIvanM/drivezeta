using System.ComponentModel.DataAnnotations;

/// <summary>
/// Class-entitys "Order" for database
/// </summary>
public class Order
{
    #region Id
    public int Id { get; set; }
    #endregion
    #region Discription
    public string Title { get; set; } = null!;
    public string? Discription { get; set; } = "";
    #endregion
    #region Time
    public DateTime Create { get; set; } = DateTime.Now;
    public DateTime? Start { get; set; } = DateTime.Now;
    public DateTime? End { get; set; } = DateTime.Now.AddDays(1);
    public DateTime? FactEnd { get; set; }
    #endregion
    public int Weight { get; set; } = 1;
    #region Parent
    public Order? Paret { get; set; }
    public int? OrderId { get; set; }
    #endregion
    #region Progress
    private int progress = 0;

    public int Progress { 
        get
        {
            return progress;
        }
        set
        {
            if(value <= 0) progress = 0;
            else if(value >= 100) progress = 100;
            else progress = value;
        } 
    }
    #endregion
    #region Status
    [MinLength(0)]
    [MaxLength(6)]
    public StatusOrders Status { get; set; } = StatusOrders.NotActiv;
    public TypeOrders Type { get; set; } = TypeOrders.Soft;
    #endregion
    #region User
    public User? Responsible { get; set; }
    public int? UserId { get; set; }
    #endregion
    #region Prioritet
    public int? Prioritet { get; set; } = null;
    #endregion
    public int? ProjectId { get; set; }
    public int? TargetId { get; set; }
}

public enum StatusOrders
{
    NotActiv,
    Activ,
    Closed,
    Overdue,
    VeryOverdue,
    Aboundoned,
}

public enum TypeOrders
{
    Hard,
    Soft,
    Completed,
    Template,
    Cronos,
    Month,
    Week,
}