public class Project
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string? Discription { get; set; }
    public DateTime Create { get; set; } = DateTime.Now;
    public DateTime? End { get; set; }
    public StatusProject Status { get; set; } = StatusProject.Activ;
    public string? Icon { get; set; }
    public string? Color { get; set; }
}

public enum StatusProject
{
    Activ,
    Closed,
    Overdue,
    VeryOverdue,
    Aboundoned,
}