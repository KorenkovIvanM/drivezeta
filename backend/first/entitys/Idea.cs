public class Idea
{
    public int Id { get; set; }
    //public User? User { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; } = null!;
    public string? Discription { get; set; } = "";
    public DateTime Create { get; set; } = DateTime.Now;
}