public class OrderRCronos
{
    public int Id { get; set; }
    public Order? Order { get; set; }
    public int OrderId { get; set; }
    public CronosTM? Cronos { get; set; }
    public int CronosTMId { get; set; }
}