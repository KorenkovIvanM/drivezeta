public class Target
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public int? UserId { get; set; }
    public int? ProjectId { get; set; }
    public int Value { get; set; }
    public int Max { get; set; }
}