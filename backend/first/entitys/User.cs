using System.ComponentModel.DataAnnotations;

public class User
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public bool IsSuperUser {get; set; } = false;
    public DateTime LastLogin {get; set; } = DateTime.Now;
    public string Email { get; set; } = null!;
    public List<Order> Orders { get; set; } = new();
}