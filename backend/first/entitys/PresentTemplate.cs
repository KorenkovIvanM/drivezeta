public class PresentTemplates
{
    public int Id { get; set; }
    public string Title { get; set; } = null!;
    public Order? Order { get; set; }
    public int OrderId { get; set; }
    public User? Responsible { get; set; }
    public int? UserId { get; set; }
    public DateTime Create { get; set; }
    public int? Weight { get; set; }
    public bool Actev { get; set; }
    public bool monday { get; set; }
    public bool tuesday { get; set; }
    public bool wednesday { get; set; }
    public bool thursday { get; set; }
    public bool friday { get; set; }
    public bool saturday { get; set; }
    public bool Sunday { get; set; }
    public DateTime? Time { get; set; }
}