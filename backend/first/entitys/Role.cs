public class Role
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int ProjectID { get; set; }
    public DateTime IsWorked { get; set; } = DateTime.Now;
    public Post Post { get; set; } = Post.Worker;
}

public enum Post
{
    Worker,
    leader,
}