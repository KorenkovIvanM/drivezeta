public class CronosTM
{
    public int Id { get; set; }
    public User? User { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; } = null!;
}