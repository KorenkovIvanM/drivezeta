using Hangfire;

public static class JobsTemplate
{
    public static void MakeOrders()
    {
        RecurringJob.AddOrUpdate("CreateOrdrFromTemplate",() => JobsTemplate.MakeOrderTemplate(), "1 1 * * *");
    }

    public static void MakeOrderTemplate()
    {
        using(Context cnt = new())
        {
            var orders = 
                from 
                    order in cnt.Orders join 
                    templete in cnt.Template on order.Id equals templete.OrderId
                where 
                    templete.Actev == true &&
                    (
                        (templete.monday && (int)DateTime.Now.DayOfWeek == 1) ||
                        (templete.tuesday && (int)DateTime.Now.DayOfWeek == 2) ||
                        (templete.wednesday && (int)DateTime.Now.DayOfWeek == 3) ||
                        (templete.thursday && (int)DateTime.Now.DayOfWeek == 4) ||
                        (templete.friday && (int)DateTime.Now.DayOfWeek == 5) ||
                        (templete.saturday && (int)DateTime.Now.DayOfWeek == 6) ||
                        (templete.Sunday && (int)DateTime.Now.DayOfWeek == 7)
                    )
                select 
                    new Order(){
                        Title = order.Title,
                        Discription = "Auto task from template",
                        UserId = templete.UserId,
                        Status = StatusOrders.Activ,
                        Type = TypeOrders.Soft,
                    };
            
            cnt.Orders.AddRange(orders);
            cnt.SaveChanges();
        }
    }

    public static void CreateOldTask()
    {
        using(Context cnt = new())
        {
            var orders = 
                from 
                    order in cnt.Orders
                where 1 == 1
                    && (order.End < DateTime.Now)
                    && (order.Status != StatusOrders.Overdue)
                select order;
            foreach (var item in orders)
            {
                item.Status = StatusOrders.Overdue;
            }
            cnt.SaveChanges();
        }
    }
}