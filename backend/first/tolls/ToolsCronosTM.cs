public static class ToolsCronosTM
{
    public static bool Create(CronosTM cronos)
    {
        try
        {
            using(Context cnt = new())
            {
                cnt.Cronos.Add(cronos);
                cnt.SaveChanges();
            }
            return true;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }

    public static List<CronosTM> Read(int UserId)
    {
        try
        {
            using(Context cnt = new())
            {
                return (
                    from cronos in cnt.Cronos
                    where cronos.UserId == UserId
                    select cronos).ToList<CronosTM>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<CronosTM>();
        }
    }

    public static bool Update(
        int Id,
        int? UserId,
        string? Title)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Cronos.Find(Id);
                if (example != null)
                {
                    example.UserId = UserId ?? example.UserId;
                    example.Title = Title ?? example.Title;
                }
                context.SaveChanges();
                return true;
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }

    }

    public static bool Delete(int Id)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Cronos.Find(Id);
                if (example != null)
                {
                    context.Cronos.Remove(example);
                }
                context.SaveChanges();
            }
            return true;
        }

        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
}