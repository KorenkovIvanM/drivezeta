public static class ToolsRole
{
    public static bool Create(int UserId, int ProjectID, Post? post)
    {
        try
        {
            using(Context context = new())
            {
                context.Roles.Add(new Role{
                    UserId = UserId,
                    ProjectID = ProjectID,
                    Post = post ?? Post.Worker,
                    IsWorked = DateTime.Now,
                });
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Create(Role Role)
    {
        try
        {
            using(Context context = new())
            {
                context.Roles.Add(Role);
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static List<Project> ReadProjects(int UserId)
    {
        try
        {
            using(Context context = new())
            {
                return (
                    from 
                        Role in context.Roles join 
                        project in context.Projects 
                            on Role.ProjectID equals project.Id
                    where 
                        Role.UserId == UserId
                    select project)
                    .ToList<Project>();
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return new List<Project>();
        }
    }
    public static List<User> ReadUsers(int ProjectID)
    {
        try
        {
            using(Context context = new())
            {
                return (
                    from 
                        Role in context.Roles join 
                        user in context.Users 
                            on Role.UserId equals user.Id
                    where 
                        Role.ProjectID == ProjectID
                    select user)
                    .ToList<User>();
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return new List<User>();
        }
    }
    public static Role? GetElement(int Id)
    {
        try
        {
            using(Context context = new())
            {
                return context.Find<Role>(Id);
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return null;
        }
    }
    public static bool Update(int Id, string? Title, string? Discription)
    {
        // TODO make to 
        return true;
        // try
        // {
        //     using(Context context = new())
        //     {
        //         var origin = context.Roles.Find(Id);
        //         origin.Title = Title ?? origin.Title;
        //         origin.Discription = Discription ?? origin.Discription;
        //         context.SaveChanges();
        //     }
        //     return true;
        // }
        // catch(Exception excepton)
        // {
        //     Login.LoginErrors(excepton);
        //     return false;
        // }
    }
    // public static bool Update(Role Role)
    // {
    //     try
    //     {
    //         using(Context context = new())
    //         {
    //             var origin = context.Roles.Find(Role.Id);
    //             origin.Title = Role.Title ?? origin.Title;
    //             origin.Discription = Role.Discription ?? origin.Discription;
    //             context.SaveChanges();
    //         }
    //         return true;
    //     }
    //     catch(Exception excepton)
    //     {
    //         Login.LoginErrors(excepton);
    //         return false;
    //     }
    // }
    public static bool Delete(int Id)
    {
        try
        {
            using(Context context = new())
            {
                context.Roles.Remove(context.Find<Role>(Id));
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Delete(int UserId, int ProjectId)
    {
        try
        {
            using(Context context = new())
            {
                context.Roles.RemoveRange(
                    from role in context.Roles
                    where role.UserId == UserId && role.ProjectID == ProjectId
                    select role
                );
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
}