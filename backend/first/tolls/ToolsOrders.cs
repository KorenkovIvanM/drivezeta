public static class ToolsOrders
{
    #region Create    
    public static bool Create(Order order)
    {
        try
        {
            using(Context cnt = new())
            {
                if(order.Type == TypeOrders.Completed)
                {
                    order.End = order.Create;
                    order.Start = order.Create;
                    order.FactEnd = order.Create;
                }
                if(order.Type == TypeOrders.Soft)
                {
                    order.Create = DateTime.Now;
                    order.End = order.Create.AddDays(1);
                    order.Start = order.Create;
                }
                cnt.Orders.Add(order);
                cnt.SaveChanges();
            }
            return true;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
    #endregion

    #region Read
    public static List<Order> Read()
    {
        try
        {
            using(Context cnt = new())
            {
                return cnt.Orders.ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }
    public static List<Order> ReadTime(TimeLine timeLine, int UserId)
    {
        try
        {
            using(Context cnt = new())
            {
                if(timeLine == TimeLine.Did)
                    return (
                        from item in cnt.Orders
                        where 
                            item.End < DateTime.Today &&
                            item.UserId == UserId
                        select item).ToList<Order>();
                else if(timeLine == TimeLine.ToDay)
                    return (
                            from item in cnt.Orders
                            where 
                                (item.Start > DateTime.Today || item.End < DateTime.Today.AddDays(1)) &&
                                item.UserId == UserId
                            select item).ToList<Order>();
                else
                    return (
                            from item in cnt.Orders
                            where 
                                item.Start > DateTime.Today.AddDays(1) &&
                                item.UserId == UserId
                            select item).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }
    public static List<Order> ReadTime(TimeLine timeLine, int UserId, TypeOrders typeOrders, bool isShowComparedTask = true)
    {
        try
        {
            using(Context cnt = new())
            {
                if(timeLine == TimeLine.Did)
                    return (
                            from item in cnt.Orders
                            where 
                                item.End < DateTime.Today &&
                                item.UserId == UserId &&
                                item.Type == typeOrders &&
                                (isShowComparedTask ? item.Status == StatusOrders.Activ : 1 == 1)
                            orderby item.Prioritet
                            select item).ToList<Order>();
                else if(timeLine == TimeLine.ToDay)
                    return (
                            from item in cnt.Orders
                            where 
                                (item.Start > DateTime.Today && item.Start < DateTime.Today.AddDays(1) ||
                                item.End > DateTime.Today && item.End < DateTime.Today.AddDays(1)) &&
                                item.UserId == UserId &&
                                item.Type == typeOrders &&
                                (isShowComparedTask ? item.Status == StatusOrders.Activ : 1 == 1)
                            orderby item.Prioritet
                            select item).ToList<Order>();
                else
                    return (
                            from item in cnt.Orders
                            where 
                                item.Start > DateTime.Today.AddDays(1) &&
                                item.UserId == UserId &&
                                item.Type == typeOrders &&
                                (isShowComparedTask ? item.Status == StatusOrders.Activ : 1 == 1)
                            orderby item.Prioritet
                            select item).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }
    public static List<Order> ReadUser(int UserId)
    {
        try
        {
            using(Context cnt = new())
            {
                return (
                        from item in cnt.Orders
                        where 
                            item.UserId == UserId
                        select item).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }
    public static Order? ReadId(int Id)
    {
        try
        {
            using(Context cnt = new())
            {
                return cnt.Orders.Find(Id);
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return null;
        }
    }
    public static List<Order> ReadType(TypeOrders typeOrders, int UserId)
    {
        try
        {
            using(Context cnt = new())
            {
                return (
                        from item in cnt.Orders
                        where 
                            item.Type == typeOrders &&
                            item.UserId == UserId
                        select item).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }
    #endregion

    public static bool Update(
        int Id,
        string? Title, 
        string? Discription, 
        DateTime? Start, 
        DateTime? End, 
        int? User, 
        int? Weight, 
        int? Project, 
        int? Work, 
        int? Target, 
        int? Parent, 
        int? Progress,
        int? Prioretis,
        TypeOrders? typeOrders,
        StatusOrders? status)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Orders.Find(Id);
                if (example != null)
                {
                    example.Title = Title ?? example.Title;
                    example.Discription = Discription ?? example.Discription;
                    example.Start = Start ?? example.Start;
                    example.End = End ?? example.End;
                    example.UserId = User ?? example.UserId;
                    example.Weight = Weight ?? example.Weight;
                    example.ProjectId = Project ?? example.ProjectId;
                    //example.Title = Title ?? example.Title;
                    //example.Title = Title ?? example.Title;
                    example.UserId = Parent ?? example.UserId;
                    example.Progress = Progress ?? example.Progress;
                    example.Prioritet = Prioretis ?? example.Prioritet;
                    example.Type = (TypeOrders)(typeOrders ?? example.Type);
                    example.Status = status ?? example.Status;

                    if((StatusOrders)(status ?? 0) == StatusOrders.Closed) 
                    { 
                        example.Progress = 100; 
                        example.FactEnd = DateTime.Now;
                    } 
                    if(Progress == 100)
                    { 
                        example.Status = StatusOrders.Closed; 
                        example.FactEnd = DateTime.Now; 
                    }
                    if(example.TargetId != null)
                        ToolsTarget.Update(example.TargetId ?? 0, null, null, example.Weight);
                }
                context.SaveChanges();
                return true;
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }

    }

    public static bool Delete(int Id)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Orders.Find(Id);
                if (example != null)
                {
                    context.Orders.Remove(example);
                }
                context.SaveChanges();
            }
            return true;
        }

        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
}

public enum TimeLine
{
    Did,
    ToDay,
    Will,
}