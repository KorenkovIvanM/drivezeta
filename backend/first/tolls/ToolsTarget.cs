public static class ToolsTarget
{
    public static bool Create(string Name, int? UserId, int? ProjectId, int Max = 100)
    {
        try
        {
            using(Context context = new())
            {
                context.Targets.Add(new Target{
                    Name = Name,
                    UserId = UserId,
                    ProjectId = ProjectId,
                    Max = Max,
                    Value = 0,
                });
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Create(Target Target)
    {
        try
        {
            using(Context context = new())
            {
                context.Targets.Add(Target);
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static List<Target> ReadForUser(int UserId)
    {
        try
        {
            using(Context context = new())
            {
                return (
                    from 
                        Target in context.Targets
                    where 
                        Target.UserId == UserId
                    select Target)
                    .ToList<Target>();
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return new List<Target>();
        }
    }
    public static List<Target> ReadForProject(int ProjectId)
    {
        try
        {
            using(Context context = new())
            {
                return (
                    from 
                        Target in context.Targets
                    where 
                        Target.ProjectId == ProjectId
                    select Target)
                    .ToList<Target>();
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return new List<Target>();
        }
    }
    public static Target? GetElement(int Id)
    {
        try
        {
            using(Context context = new())
            {
                return context.Find<Target>(Id);
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return null;
        }
    }
    public static bool Update(int Id, string? Name, int? Max, int? Value)
    {
        try
        {
            using(Context context = new())
            {
                var origin = context.Targets.Find(Id);
                origin.Name = Name ?? origin.Name;
                origin.Max = Max ?? origin.Max;
                origin.Value = Value ?? origin.Value;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Update(Target Target)
    {
        try
        {
            using(Context context = new())
            {
                var origin = context.Targets.Find(Target.Id);
                origin.Name = Target.Name ?? origin.Name;
                origin.Max = Target.Max == 0 ? origin.Max : Target.Max;
                origin.Value = origin.Value + Target.Value;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Delete(int Id)
    {
        try
        {
            using(Context context = new())
            {
                context.Targets.Remove(context.Find<Target>(Id));
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
}