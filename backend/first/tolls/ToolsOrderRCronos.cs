public static class ToolsOrderRCronos
{
    public static bool Create(OrderRCronos item)
    {
        try
        {
            using(Context cnt = new())
            {
                cnt.OrderRCronos.Add(item);
                cnt.SaveChanges();
            }
            return true;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }

    public static List<Order> ReadOrder(int UserId, int CronosId)
    {
        try
        {
            using(Context cnt = new())
            {
                return (
                    from 
                        relation in cnt.OrderRCronos join 
                        order in cnt.Orders 
                            on relation.OrderId equals order.Id
                    where 
                        relation.CronosTMId == CronosId && 
                        order.UserId == UserId
                    select order).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<Order>();
        }
    }

    public static List<CronosTM> ReadCronos(int UserId, int? OrderId)
    {
        try
        {
            using(Context cnt = new())
            {
                return (
                    from 
                        relation in cnt.OrderRCronos join 
                        cronos in cnt.Cronos 
                            on relation.CronosTMId equals cronos.Id
                    where relation.OrderId == OrderId && cronos.UserId == UserId
                    select cronos).ToList<CronosTM>();
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return new List<CronosTM>();
        }
    }

    public static bool Delete(int Id)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.OrderRCronos.Find(Id);
                if (example != null)
                {
                    context.OrderRCronos.Remove(example);
                }
                context.SaveChanges();
            }
            return true;
        }

        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
}