public static class ToolsTemplate
{
    public static bool Create(TemplateOrders template)
    {
        try
        {
            using (Context cnt = new())
            {
                cnt.Template.Add(template);
                cnt.SaveChanges();
            }
            return true;
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }

    public static List<PresentTemplates>? Read(int? UserId)
    {
        try
        {
            using (Context cnt = new())
            {
                return (
                    from 
                        templat in cnt.Template join 
                        order in cnt.Orders on templat.OrderId equals order.Id
                    where 
                        UserId != null ? templat.UserId == UserId : 1 == 1
                    select 
                        new PresentTemplates{
                            Id = templat.Id,
                            OrderId = templat.OrderId,
                            UserId = order.UserId,
                            Title = order.Title,
                            Create = order.Create,
                            Weight = order.Weight,
                            Actev = templat.Actev,
                            monday = templat.monday,
                            tuesday = templat.tuesday,
                            wednesday = templat.wednesday,
                            thursday = templat.thursday,
                            friday = templat.friday,
                            saturday = templat.saturday,
                            Sunday = templat.Sunday,
                            Time = templat.Time,
                        }).ToList<PresentTemplates>();
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return null;
        }
    }

    public static TemplateOrders? GetElement(int Id)
    {
        try
        {
            using (Context cnt = new())
            {
                return cnt.Template.Find(Id);
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return null;
        }
    }

    public static bool Update(
        int Id,
        int? OrderId,
        int? UserId,
        bool? Actev,
        bool? monday,
        bool? tuesday,
        bool? wednesday,
        bool? thursday,
        bool? friday,
        bool? saturday,
        bool? Sunday,
        DateTime? Time,
        int? weight)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Template.Find(Id);
                if (example != null)
                {
                    example.UserId = UserId ?? example.UserId;
                    example.OrderId = OrderId ?? example.OrderId;
                    example.Actev = Actev ?? example.Actev;
                    example.monday = monday ?? example.monday;
                    example.tuesday = tuesday ?? example.tuesday;
                    example.wednesday = wednesday ?? example.wednesday;
                    example.thursday = thursday ?? example.thursday;
                    example.friday = friday ?? example.friday;
                    example.saturday = saturday ?? example.saturday;
                    example.Sunday = Sunday ?? example.Sunday;
                    example.Time = Time ?? example.Time;
                }
                context.SaveChanges();
                return true;
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }

    }

    public static bool Delete(int Id)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Template.Find(Id);
                if (example != null)
                {
                    context.Template.Remove(example);
                }
                context.SaveChanges();
            }
            return true;
        }

        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
}