public static class ToolsUsers
{
    #region Create
    /// <summary>
    /// Created user
    /// </summary>
    /// <param name="user">User for addation</param>
    /// <returns>true if successfully else false</returns>
    public static int Create(User user)
    {
        try
        {
            User? buff = ToolsUsers.GetElement(user.Email);
            if(buff != null)
                return buff.Id;
            using(Context cnt = new())
            {
                cnt.Users.Add(user);
                cnt.SaveChanges();
            }
            buff = ToolsUsers.GetElement(user.Email);
            return buff?.Id ?? 0;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return 0;
        }
        
    }
    #endregion
    #region Read
    /// <summary>
    /// Возварашает пользователя
    /// </summary>
    /// <param name="Id">Id пользователя</param>
    /// <returns></returns>
    public static User? GetElement(int Id)
    {
        try
        {
            User? buff = new();
            using(Context cnt = new())
            {
                s_activUser(Id);
                buff = cnt.Users.Find(Id);
                return buff;
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return null;
        }
    }

    /// <summary>
    /// Возварашает пользователя
    /// </summary>
    /// <param name="Email">Email пользователя</param>
    /// <returns></returns>
    public static User? GetElement(string Email)
    {
        try
        {
            User? buff = new();
            using(Context cnt = new())
            {
                buff = (
                    from user in cnt.Users
                    where user.Email == Email
                    select user)
                        .First<User>();
                s_activUser(buff.Id);
                return buff;
            }
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return null;
        }
    }
    #endregion
    #region Update
    /// <summary>
    /// Обновляет данные пользователя
    /// </summary>
    /// <param name="Id">Id пользователь</param>
    /// <param name="Name">Имя</param>
    /// <param name="LastLogin">Дата последнег логирования</param>
    /// <param name="Email">Email</param>
    /// <returns>true if successfully else false</returns>
    public static bool Update(int Id, string? Name, DateTime? LastLogin, string? Email)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Users.Find(Id);
                if (example != null)
                {
                    example.Name = Name ?? example.Name;
                    example.LastLogin = LastLogin ?? example.LastLogin;
                    example.Email = Email ?? example.Email;
                }
                context.SaveChanges();
                return true;
            }
        }
        catch (Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }

    }

    /// <summary>
    /// Обновляет дату последнего логирования
    /// </summary>
    /// <param name="Id">Id пользователь</param>
    private static void s_activUser(int Id)
    {
        ToolsUsers.Update(Id, null, DateTime.Now, null);
    }
    #endregion
    #region Delete
    /// <summary>
    /// Deleted Order
    /// </summary>
    /// <param name="Id">Id order</param>
    /// <returns></returns>
    public static bool Delete(int Id)
    {
        try
        {
            using (Context context = new())
            {
                var example = context.Users.Find(Id);
                if (example != null)
                {
                    context.Users.Remove(example);
                }
                context.SaveChanges();
            }
            return true;
        }

        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
    #endregion
}