public static class ToolsIdea
{
    public static bool Create(string Title, string? Discription, int UserId)
    {
        try
        {
            using(Context context = new())
            {
                context.Ideas.Add(new Idea{
                    Title = Title,
                    Discription = Discription,
                    UserId = UserId,
                    Create = DateTime.Now,
                });
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Create(Idea idea)
    {
        try
        {
            using(Context context = new())
            {
                context.Ideas.Add(idea);
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static List<Idea> Read(int UserId)
    {
        try
        {
            using(Context context = new())
            {
                return (
                    from 
                        idea in context.Ideas
                    where 
                        idea.UserId == UserId
                    select idea)
                    .ToList<Idea>();
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return new List<Idea>();
        }
    }
    public static Idea? GetElement(int Id)
    {
        try
        {
            using(Context context = new())
            {
                return context.Find<Idea>(Id);
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return null;
        }
    }
    public static bool Update(int Id, string? Title, string? Discription)
    {
        try
        {
            using(Context context = new())
            {
                var origin = context.Ideas.Find(Id);
                origin.Title = Title ?? origin.Title;
                origin.Discription = Discription ?? origin.Discription;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Update(Idea idea)
    {
        try
        {
            using(Context context = new())
            {
                var origin = context.Ideas.Find(idea.Id);
                origin.Title = idea.Title ?? origin.Title;
                origin.Discription = idea.Discription ?? origin.Discription;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Delete(int Id)
    {
        try
        {
            using(Context context = new())
            {
                context.Ideas.Remove(context.Find<Idea>(Id));
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
}