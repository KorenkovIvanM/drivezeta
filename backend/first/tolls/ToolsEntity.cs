using System.Reflection;

public static class ToolsEntity
{
    public static void Update<T>(ref T _in, ref T _out)
    {
        Type buffType = typeof(T);

        foreach (FieldInfo field in buffType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
        {
            var value_in = field?.GetValue(_in);
            var value_out = field?.GetValue(_out);


            if (value_out != null)
                field?.SetValue(_in, value_out);

            System.Console.WriteLine($"in :: {value_in}  out :: {value_out}");
        }
    }
}