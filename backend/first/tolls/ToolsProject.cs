public static class ToolsProject
{
public static bool Create(string Name, string? Discription, DateTime? End, string? Icon)
    {
        try
        {
            using(Context context = new())
            {
                context.Projects.Add(new Project{
                    Name = Name,
                    Discription = Discription,
                    Create = DateTime.Now,
                    End = End,
                    Status = StatusProject.Activ,
                    Icon = Icon
                });
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Create(Project Project)
    {
        try
        {
            using(Context context = new())
            {
                context.Projects.Add(Project);
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static List<Project> Read(int UserId)
    {
        return ToolsRole.ReadProjects(UserId);
    }
    public static Project? GetElement(int Id)
    {
        try
        {
            using(Context context = new())
            {
                return context.Find<Project>(Id);
            }
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return null;
        }
    }
    public static bool Update(int Id, string? Name, string? Discription, DateTime? End, string? Icon, string? Color)
    {
        try
        {
            using(Context context = new())
            {
                var origin = context.Projects.Find(Id);
                origin.Name = Name ?? origin.Name;
                origin.Discription = Discription ?? origin.Discription;
                origin.End = End ?? origin.End;
                origin.Icon = Icon ?? origin.Icon;
                origin.Color = Color ?? origin.Color;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Update(Project project)
    {
        try
        {
            using(Context context = new())
            {
                var origin = ToolsProject.GetElement(project.Id);
                origin.Name = project.Name ?? origin.Name;
                origin.Discription = project.Discription ?? origin.Discription;
                origin.End = project.End ?? origin.End;
                origin.Icon = project.Icon ?? origin.Icon;
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
    public static bool Delete(int Id)
    {
        try
        {
            using(Context context = new())
            {
                context.Projects.Remove(context.Find<Project>(Id));
                context.SaveChanges();
            }
            return true;
        }
        catch(Exception excepton)
        {
            Login.LoginErrors(excepton);
            return false;
        }
    }
}