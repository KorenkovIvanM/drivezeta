using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class GroupUsersController : ControllerBase
{
    private readonly ILogger<GroupUsersController> _logger;
 
    public GroupUsersController(ILogger<GroupUsersController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GroupUsers")]
    public List<User>? get()
    {
        using(Context cnt = new())
        {
            return (from user in cnt.Users
                    select user).ToList<User>();
        }
    }
}