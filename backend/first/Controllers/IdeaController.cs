using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class IdeaController : ControllerBase
{
    private readonly ILogger<IdeaController> _logger;

    public IdeaController(ILogger<IdeaController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "Idea")]
    public bool post(Idea idea)
    {
        return ToolsIdea.Create(idea);
    }

    private IQueryable<Idea> _getLinqSelect(Idea mask)
    {
        using (Context context = new())
        {
            return
                (
                    from item in context.Ideas
                    where 1 == 1
                        && (mask.Id != 0 ? item.Id == mask.Id : true)
                        && (mask.UserId != null ? mask.UserId == mask.UserId : true)
                        && (mask.Title != null ? mask.Title == mask.Title : true)
                        && (mask.Discription != null ? mask.Discription == mask.Discription : true)
                    // && (mask.Create != null ? mask.Create == mask.Create : true)
                    select item);
        }

    }

    [HttpGet(Name = "Idea")]
    public List<Idea>? get(Idea mask, int? page, int? size)
    {
        using (Context context = new())
        {
            return
                _getLinqSelect(mask)
                .Take<Idea>(
                    size == null || page == null ?
                    new Range(size ?? 0 * page ?? 0, size ?? 0 * (page ?? 0 + 1)) :
                    new Range())
                .ToList<Idea>();
        }
    }

    [HttpPut(Name = "Idea")]
    public bool put([FromBody] Idea idea)
    {
        Idea a = new(){
            Discription = "123",
        };
        Idea b = new(){
            Title = "qwe",
        };
        ToolsEntity.Update<Idea>(ref a, ref b);
        return true;//ToolsIdea.Update(Id, Title, Discription);
    }
    [HttpDelete(Name = "Idea")]
    public bool delete(int Id)
    {
        return ToolsIdea.Delete(Id);
    }
}