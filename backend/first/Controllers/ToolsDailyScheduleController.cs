using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class ToolsDailyScheduleController : ControllerBase
{

    private readonly ILogger<ToolsDailyScheduleController> _logger;
 
    public ToolsDailyScheduleController(ILogger<ToolsDailyScheduleController> logger)
    {
        _logger = logger;
    }
    [HttpPost(Name = "ToolsDownTask")]
    public bool post(int? Id, string? Email)
    {
        try
        {
            if(Id == null)
                Id = ToolsUsers.GetElement(Email ?? "")?.Id ?? Id;

            using(Context cnt = new())
            {
                var tasks = 
                    from task in cnt.Orders
                    where 
                        task.UserId == Id &&
                        task.Status == StatusOrders.Activ &&
                        task.End < DateTime.Today &&
                        (task.Type == TypeOrders.Soft || 
                        task.Type == TypeOrders.Hard)
                    select task;
                foreach(var task in tasks)
                {
                    task.Start = DateTime.Now;
                    task.End = task.Start;
                }
                cnt.SaveChanges();
            }

            return true;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }

    [HttpPut(Name = "ToolsSortTask")]
    public bool put(int? Id, string? Email)
    {
        try
        {
            if(Id == null)
                Id = ToolsUsers.GetElement(Email ?? "")?.Id ?? Id;

            using(Context cnt = new())
            {
                var tasks = 
                    from item in cnt.Orders
                    where 
                        (item.Start > DateTime.Today && item.Start < DateTime.Today.AddDays(1) ||
                        item.End > DateTime.Today && item.End < DateTime.Today.AddDays(1)) &&
                        item.UserId == Id &&
                        item.Type == TypeOrders.Soft &&
                        item.Status == StatusOrders.Activ
                    orderby item.Prioritet
                    select item;
                var index = 1;
                foreach(var task in tasks)
                {
                    task.Prioritet = index;
                    index++;
                }
                cnt.SaveChanges();
            }

            return true;
        }
        catch(Exception exp)
        {
            Login.LoginErrors(exp);
            return false;
        }
    }
}