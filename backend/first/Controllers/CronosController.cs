using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class CronosController : ControllerBase
{
    private readonly ILogger<CronosController> _logger;
 
    public CronosController(ILogger<CronosController> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "CronosTM")]
    public List<CronosTM> get(int UserId)
    {
        return ToolsCronosTM.Read(UserId);
    }

    [HttpPost(Name = "CronosTM")]
    public bool post(string Title, int? UserId)
    {
        return ToolsCronosTM.Create(new CronosTM{
            Title = Title,
            UserId = UserId,
        });
    }

    [HttpPut(Name = "CronosTM")]
    public bool put(int Id, int? UserId, string? Title)
    {
        return ToolsCronosTM.Update(Id, UserId, Title);
    }

    [HttpDelete(Name = "CronosTM")]
    public bool delete(int Id)
    {
        return ToolsCronosTM.Delete(Id);
    }
}