using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class UserInProjectController : ControllerBase
{
    private readonly ILogger<UserInProjectController> _logger;
 
    public UserInProjectController(ILogger<UserInProjectController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "ProjectUser")]
    public bool post(int ProjectId, int UserId)
    {
        return ToolsRole.Create(UserId, ProjectId, Post.Worker);
    }

    [HttpGet(Name = "ProjectUser")]
    public List<Project>? get(int UserId)
    {
        return ToolsProject.Read(UserId);
    }

    // [HttpPut(Name = "Idea")]
    // public bool put(int Id, string? Title, string? Discription)
    // {
    //     return ToolsIdea.Update(Id, Title, Discription);
    // }
    // [HttpDelete(Name = "Idea")]
    // public bool delete(int Id)
    // {
    //     return ToolsIdea.Delete(Id);
    // }
}