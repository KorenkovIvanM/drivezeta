using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class OrderController : ControllerBase
{
    private readonly ILogger<OrderController> _logger;
 
    public OrderController(ILogger<OrderController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "Order")]
    public List<Order> get(TimeLine? timeLine, int? user, TypeOrders? typeOrders, int? id)
    {
        if(id != null)
        { 
            var buff = new List<Order>(); 
            buff.Add(ToolsOrders.ReadId(id ?? 0) ?? new()); 
            return buff;
        }
        if(user != null && timeLine == null && typeOrders == null) return ToolsOrders.ReadUser(user ?? 0);
        if(user != null && timeLine != null && typeOrders == null) return ToolsOrders.ReadTime((TimeLine)timeLine ,user ?? 0);
        if(user != null && timeLine == null && typeOrders != null) return ToolsOrders.ReadType((TypeOrders)typeOrders ,user ?? 0);
        if(user != null && timeLine != null && typeOrders != null) return ToolsOrders.ReadTime((TimeLine)timeLine ,user ?? 0, typeOrders ?? 0);
        if(user == null && timeLine == null && typeOrders == null && id == null) return ToolsOrders.Read();
        return new List<Order>();
    }

    [HttpPost(Name = "Order")]
    public bool post(
        string Title, 
        string? Discription, 
        DateTime? Start, 
        DateTime? End, 
        int? User, 
        int? Weight, 
        int? ProjectId, 
        int? Work, 
        int? Target, 
        int? Parent, 
        int? Prioretis,
        TypeOrders typeOrders,
        int? TargetId,
        StatusOrders status = StatusOrders.Activ)
    {
        return ToolsOrders.Create(new Order{
            Title = Title,
            Discription = Discription,
            Start = Start,
            End = End,
            UserId = User,
            ProjectId = ProjectId,
            Weight = Weight ?? 0,
            OrderId = Parent,
            Prioritet = Prioretis,
            Type = typeOrders,
            Status = status,
            TargetId = TargetId,
        });
    }

    [HttpPut(Name = "Order")]
    public bool put(
        int Id,
        string? Title, 
        string? Discription, 
        DateTime? Start, 
        DateTime? End, 
        int? User, 
        int? Weight, 
        int? ProjectId, 
        int? Work, 
        int? Target, 
        int? Parent, 
        int? Prioretis,
        int? Progress,
        TypeOrders? typeOrders,
        StatusOrders? status)
    {
        return ToolsOrders.Update(
        Id,
        Title, 
        Discription, 
        Start, 
        End, 
        User, 
        Weight, 
        ProjectId, 
        Work, 
        Target, 
        Parent, 
        Progress,
        Prioretis,
        typeOrders,
        status);
    }

    [HttpDelete(Name = "Order")]
    public bool delete(int Id)
    {
        return ToolsOrders.Delete(Id);
    }
}
