using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class OrderCronos : ControllerBase
{
    private readonly ILogger<OrderCronos> _logger;
 
    public OrderCronos(ILogger<OrderCronos> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "OrderCronos")]
    public List<Order> get(int UserId, int CronosId)
    {
        return ToolsOrderRCronos.ReadOrder(UserId, CronosId);
    }

    [HttpPost(Name = "OrderCronos")]
    public bool post(string Title, int? UserId, string? Discription, int CronosId)
    {
        // TODO drop the tresh
        DateTime creat = DateTime.Now;
        ToolsOrders.Create(new Order{
            Title = Title,
            Discription = Discription,
            Type = TypeOrders.Cronos,
            Status = StatusOrders.NotActiv,
            UserId = UserId,
            Create = creat});
        int id = 0;
        using(Context cnt = new())
        {
            id = (from orderb in cnt.Orders
            where 1 == 1
                && orderb.Title == Title
                && orderb.Discription == Discription
                && orderb.UserId == UserId
                && orderb.Create == creat
            select orderb).First<Order>().Id;
        }
        ToolsOrderRCronos.Create(new OrderRCronos{
            OrderId = id,
            CronosTMId = CronosId,
        });
        return true;
    }

    [HttpPut(Name = "OrderCronos")]
    public bool put(int Id, string? Discription, string? Title, bool isDown)
    {
        System.Console.WriteLine("1");
        if(isDown)
        {
            using(Context cnt = new())
            {
                cnt.OrderRCronos.RemoveRange(
                    from 
                        item in cnt.OrderRCronos
                    where 
                        item.OrderId == Id
                    select 
                        item);
                cnt.SaveChanges();
            }
            return ToolsOrders.Update(
                Id,
                Title, 
                Discription, 
                DateTime.Now, 
                DateTime.Now, 
                null, null, null, null, null, null, null, null,
                TypeOrders.Soft,
                StatusOrders.Activ);
        }
        else
            return ToolsOrders.Update(
                Id,
                Title, 
                Discription,
                null,null,null,null,null,null,null,null,null,null,null,null);
    }

    [HttpDelete(Name = "OrderCronos")]
    public bool delete(int Id)
    {
        return ToolsOrders.Delete(Id);
    }
}