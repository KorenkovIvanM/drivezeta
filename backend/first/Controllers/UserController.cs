using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
 
    public UserController(ILogger<UserController> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "User")]
    public User? get(int? Id, string? Email)
    {
        //-----------------------------------------------------------------------
        // List<User> lUser;
        // List<Order> lOrder;
        // List<TemplateOrders> lTemplate;
        // List<CronosTM> lCTM;
        // List<OrderRCronos> lOC;
        // List<Idea> lIdea;
        // // List<Project> lProject;
        // // List<Role> lRole;
        // // List<Target> lTarget;
        // using(Context cnt = new())
        // {
        //     lUser = cnt.Users.ToList();
        //     lOrder = cnt.Orders.ToList();
        //     lTemplate = cnt.Template.ToList();
        //     lCTM = cnt.Cronos.ToList();
        //     lOC = cnt.OrderRCronos.ToList();
        //     lIdea = cnt.Ideas.ToList();
        // }
        // using(ContextTest cnt = new())
        // {
        //     cnt.Users.AddRange(lUser);
        //     cnt.Orders.AddRange(lOrder);
        //     cnt.Template.AddRange(lTemplate);
        //     cnt.Cronos.AddRange(lCTM);
        //     cnt.OrderRCronos.AddRange(lOC);
        //     cnt.Ideas.AddRange(lIdea);
        //     cnt.SaveChanges();
        // }
        //-----------------------------------------------------------------------
        if(Id != null) return ToolsUsers.GetElement(Id ?? 0);
        if(Email != null) return ToolsUsers.GetElement(Email ?? "");
        return null;
    }

    [HttpPost(Name = "User")]
    public int post(string Name, string Email)
    {
        return ToolsUsers.Create(new User{
            Name = Name,
            Email = Email,
        });
    }

    [HttpPut(Name = "User")]
    public bool put(int Id, string? Name, DateTime? LastLogin, string? Email)
    {
        return ToolsUsers.Update(Id, Name, LastLogin, Email);
    }

    [HttpDelete(Name = "User")]
    public bool delete(int Id)
    {
        return ToolsUsers.Delete(Id);
    }
}