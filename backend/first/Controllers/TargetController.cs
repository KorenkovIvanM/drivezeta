using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TargetController : ControllerBase
{
    private readonly ILogger<TargetController> _logger;
 
    public TargetController(ILogger<TargetController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "Target")]
    public bool post(string Name, int? UserId, int? ProjectId, int? Max)
    {
        return ToolsTarget.Create(Name, UserId, ProjectId, Max ?? 100);
    }

    [HttpGet(Name = "Target")]
    public List<Target>? get(int? UserId, int? ProjectId)
    {
        if(UserId == null && ProjectId == null)
            return new List<Target>();
        else if(UserId != null)
            return ToolsTarget.ReadForUser(UserId ?? 0);
        else
            return ToolsTarget.ReadForProject(ProjectId ?? 0);
    }

    [HttpPut(Name = "Target")]
    public bool put(int Id, string? Name, int? Max, int? Value)
    {
        return ToolsTarget.Update(Id, Name, Max, Value);
    }
    [HttpDelete(Name = "Target")]
    public bool delete(int Id)
    {
        return ToolsTarget.Delete(Id);
    }
}