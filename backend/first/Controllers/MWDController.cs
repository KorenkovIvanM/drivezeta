using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class MWDController : ControllerBase
{
    private readonly ILogger<MWDController> _logger;
 
    public MWDController(ILogger<MWDController> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "MWD")]
    public List<Order> get(int UserId, bool isMonth)
    {
        try
        {
            using(Context cnt = new())
            {
                return(
                    from order in cnt.Orders
                    where 1 == 1
                        && order.Status == StatusOrders.NotActiv
                        && (isMonth ? order.Type == TypeOrders.Month : order.Type == TypeOrders.Week)
                        && order.UserId == UserId
                    orderby order.Create
                    select order
                ).ToList<Order>();
            }
        }
        catch(Exception exp)
        {
            _logger.LogError(exp.Message);
            return new List<Order>();
        }
    }

    [HttpPost(Name = "MWD")]
    public bool post(string Title, int? UserId, bool isMonth, string? Discription)
    {
        return ToolsOrders.Create(new Order{
            Title = Title,
            Discription = Discription,
            Start = null,
            End = null,
            UserId = UserId,
            Weight = 1,
            Type = (isMonth ? TypeOrders.Month : TypeOrders.Week),
            Status = StatusOrders.NotActiv,
        });
    }

    [HttpPut(Name = "MWD")]
    public bool put(int Id, string? Title, bool isMonth, string? Discription)
    {
        // using(Context cnt = new())
        // {
        //     Order buff = cnt.Orders.Find(Id);
        //     buff.Title = Title != null ? Title : buff.Title;
        // }
        Order currentOrder = ToolsOrders.ReadId(Id);
        if(isMonth)
            return ToolsOrders.Update(
                Id,
                Title, 
                Discription, 
                (currentOrder.Type == TypeOrders.Week  ? DateTime.Now : null), 
                (currentOrder.Type == TypeOrders.Week  ? DateTime.Now : null), 
                null, null, null, null, null, null, null, null,
                (currentOrder.Type == TypeOrders.Month  ? TypeOrders.Week : TypeOrders.Soft),
                (currentOrder.Type == TypeOrders.Month  ? StatusOrders.NotActiv : StatusOrders.Activ));
        else
            return ToolsOrders.Update(
                Id,
                Title, 
                Discription, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    [HttpDelete(Name = "MWD")]
    public bool delete(int Id)
    {
        return ToolsOrders.Delete(Id);
    }
}