using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class ProjectController : ControllerBase
{
    private readonly ILogger<ProjectController> _logger;
 
    public ProjectController(ILogger<ProjectController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "Project")]
    public bool post(string Name, string? Discription, DateTime? End, string? Icon, int UserId)
    {
        ToolsProject.Create(Name, Discription, End, Icon);
        int ProjectId;
        using(Context cnt = new())
        {
            ProjectId = (from item in cnt.Projects
            where item.Name == Name && item.Discription == Discription && item.Icon == Icon
            select item).First<Project>().Id;
        }
        return ToolsRole.Create(UserId, ProjectId, Post.leader);
    }

    [HttpGet(Name = "Project")]
    public List<Project>? get(int UserId)
    {
        return ToolsProject.Read(UserId);
    }

    [HttpPut(Name = "Project")]
    public bool put(int Id, string? Name, string? Discription, DateTime? End, StatusProject? Status, string? Icon, string? Color)
    {
        return ToolsProject.Update(Id, Name, Discription, End, Icon, Color);
    }
    [HttpDelete(Name = "Project")]
    public bool delete(int Id)
    {
        return ToolsProject.Delete(Id);
    }
}