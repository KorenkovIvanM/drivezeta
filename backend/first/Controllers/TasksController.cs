using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TasksController : ControllerBase
{
    [HttpGet(Name = "Tsak")]
    public List<Order>? get(
        int? Id, 
        int? UserId, 
        int? Parent, 
        StatusOrders? Status, 
        TypeOrders? Type, 
        int? ProjectId, 
        int? TargetId,
        TimeLine? timeLine)
    {
        using( Context context = new())
            return (
                from 
                    order in context.Orders
                where 1 == 1
                    && (Id != null ? order.Id == Id : true)
                    && (UserId != null ? order.UserId == UserId : true)
                    && (Parent != null ? order.OrderId == Parent : true)
                    && (Status != null ? order.Status == Status : true)
                    && (Type != null ? order.Type == Type : true)
                    && (ProjectId != null ? order.ProjectId == ProjectId : true)
                    && (TargetId != null ? order.TargetId == TargetId : true)
                    && (timeLine != null ? (
                        (timeLine == TimeLine.Did && order.End < DateTime.Today) ||
                        (timeLine == TimeLine.ToDay && order.Start < DateTime.Today || order.End > DateTime.Today.AddDays(1)) ||
                        (timeLine == TimeLine.Did && order.Start > DateTime.Today.AddDays(1))) : true)
                orderby order.Prioritet
                select
                    order).ToList<Order>();
    }
}