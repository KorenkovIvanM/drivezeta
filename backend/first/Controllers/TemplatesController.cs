using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TemplatesController : ControllerBase
{
    private readonly ILogger<TemplatesController> _logger;

    public TemplatesController(ILogger<TemplatesController> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "TemplateOrders")]
    public List<PresentTemplates>? get(int? UserId)
    {
        return ToolsTemplate.Read(UserId);
    }

    [HttpPost(Name = "TemplateOrders")]
    public bool post(
        string Title,
        int UserId,
        int? weight,
        bool actev = false,
        bool monday = false,
        bool tuesday = false,
        bool wednesday = false,
        bool thursday = false,
        bool friday = false,
        bool saturday = false,
        bool Sunday = false,
        DateTime? Time = null)
    {
        var order = new Order
        {
            Title = Title,
            UserId = UserId,
            Create = DateTime.Now,
            Weight = weight ?? 1,
            End = null,
            Start = null,
            Status = StatusOrders.NotActiv,
            Type = TypeOrders.Template,
        };
        var template = new TemplateOrders
        {
            Order = order,
            UserId = UserId,
            Actev = actev,
            monday = monday,
            tuesday = tuesday,
            wednesday = wednesday,
            thursday = thursday,
            friday = friday,
            saturday = saturday,
            Sunday = Sunday,
            Time = Time,
        };
        try
        {
            using (Context cnt = new())
            {
                cnt.Orders.Add(order);
                cnt.Template.Add(template);
                cnt.SaveChanges();
            }
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            return false;
        }
    }

    [HttpPut(Name = "TemplateOrders")]
    public bool put(
        int Id,
        string? Title,
        int? OrderId,
        int? UserId,
        bool? actev,
        bool? monday,
        bool? tuesday,
        bool? wednesday,
        bool? thursday,
        bool? friday,
        bool? saturday,
        bool? Sunday,
        DateTime? Time,
        int? weight
    )
    {
        var order = ToolsTemplate.GetElement(Id);
        ToolsOrders.Update(order.OrderId, null,null,null,null,null,weight,null,null,null,null,null,null,null,null);
        return ToolsTemplate.Update(
            Id,
            OrderId,
            UserId,
            actev,
            monday,
            tuesday,
            wednesday,
            thursday,
            friday,
            saturday,
            Sunday,
            Time,
            weight);
    }

    [HttpDelete(Name = "TemplateOrders")]
    public bool delete(int Id)
    {
        return ToolsTemplate.Delete(Id);
    }
}