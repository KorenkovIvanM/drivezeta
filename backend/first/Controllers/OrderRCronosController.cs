using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class OrderRCronosController : ControllerBase
{
    private readonly ILogger<OrderRCronosController> _logger;
 
    public OrderRCronosController(ILogger<OrderRCronosController> logger)
    {
        _logger = logger;
    }
    [HttpGet(Name = "OrderRCronos")]
    public dynamic get(int UserId, int? OrderId, int? CronosId)
    {
        if(OrderId != null) return ToolsOrderRCronos.ReadCronos(UserId, OrderId);
        if(CronosId != null) return ToolsOrderRCronos.ReadOrder(UserId, (CronosId ?? 0));
        return 0;
    }

    [HttpPost(Name = "OrderRCronos")]
    public bool post(int OrderId, int CronosId)
    {
        return ToolsOrderRCronos.Create(new OrderRCronos{
            OrderId = OrderId,
            CronosTMId = CronosId,
        });
    }

    [HttpDelete(Name = "OrderRCronos")]
    public bool delete(int Id)
    {
        return ToolsOrderRCronos.Delete(Id);
    }
}