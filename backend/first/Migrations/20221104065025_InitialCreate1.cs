﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Template_Orders_TempalteId",
                table: "Template");

            migrationBuilder.DropForeignKey(
                name: "FK_Template_Users_userId",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "IdOrder",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "IdUser",
                table: "Template");

            migrationBuilder.RenameColumn(
                name: "userId",
                table: "Template",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "TempalteId",
                table: "Template",
                newName: "OrderId");

            migrationBuilder.RenameIndex(
                name: "IX_Template_userId",
                table: "Template",
                newName: "IX_Template_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Template_TempalteId",
                table: "Template",
                newName: "IX_Template_OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Template_Orders_OrderId",
                table: "Template",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Template_Users_UserId",
                table: "Template",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Template_Orders_OrderId",
                table: "Template");

            migrationBuilder.DropForeignKey(
                name: "FK_Template_Users_UserId",
                table: "Template");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Template",
                newName: "userId");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "Template",
                newName: "TempalteId");

            migrationBuilder.RenameIndex(
                name: "IX_Template_UserId",
                table: "Template",
                newName: "IX_Template_userId");

            migrationBuilder.RenameIndex(
                name: "IX_Template_OrderId",
                table: "Template",
                newName: "IX_Template_TempalteId");

            migrationBuilder.AddColumn<int>(
                name: "IdOrder",
                table: "Template",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdUser",
                table: "Template",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Template_Orders_TempalteId",
                table: "Template",
                column: "TempalteId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Template_Users_userId",
                table: "Template",
                column: "userId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
