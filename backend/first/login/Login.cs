public static class Login
{
    public static void LoginErrors(Exception exp)
    {
        System.Console.WriteLine(exp.Message);
        if(exp.InnerException != null)
            System.Console.WriteLine(exp.InnerException.Message ?? "Внетреннего исключение нет");
    }
}