using Microsoft.EntityFrameworkCore;

public class ContextTest : DbContext
{
    public DbSet<TemplateOrders> Template { get; set; } = null!;
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Order> Orders { get; set; } = null!;
    public DbSet<CronosTM> Cronos {get; set; } = null!;
    public DbSet<OrderRCronos> OrderRCronos { get; set; } = null!;
    public DbSet<Idea> Ideas { get; set; } = null!;
    public DbSet<Project> Projects { get; set; } = null!;
    public DbSet<Role> Roles { get; set; } = null!;
    public DbSet<Target> Targets { get; set; } = null!;
    
    public ContextTest() => Database.EnsureCreated();
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("data source=DRIVEZETA_test.db");
    }
}