using Hangfire;

public static class JobsTemplate
{
    public static void MakeOrders()
    {
        RecurringJob.AddOrUpdate("CreateOrdrFromTemplate",() => JobsTemplate.MakeOrderTemplate(), "1 1 * * *");
        RecurringJob.AddOrUpdate("Overdue",() => JobsTemplate.Overdue(), "0 1 * * *");
    }

    public static void MakeOrderTemplate()
    {
        using(Context context =new())
        {
            var buff = (
                from 
                    template in context.Templates join 
                    order in context.Orders 
                        on template.OrderId equals order.Id
                where 1 == 1 
                    && template.Activ
                select new Order{
                    Title = order.Title,
                    Discription = "Auto task from template",
                    UserId = order.UserId,
                    Status = StatusOrdr.Activ,

                });
            context.Orders.AddRange(buff);
            context.SaveChanges();
        }
    }
    public static void Overdue()
    {
        using(Context context =new())
        {
            var buff = (
                from 
                    order in context.Orders
                where 1 == 1 
                    && order.Status != StatusOrdr.Overdue
                    && order.Type == TypeOrder.Hard
                    && order.End < DateTime.Now
                select order);
            foreach(var item in buff)
                item.Status = StatusOrdr.Overdue;
            context.Orders.AddRange(buff);
            context.SaveChanges();
        }
    }
}