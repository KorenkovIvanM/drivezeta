using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class IdeaController : GenericController<Idea, Context>
{
    private readonly ILogger<IdeaController> _logger;
 
    public IdeaController(ILogger<IdeaController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<Idea> get(
        int? Id, 
        int? UserId,
        string? Title, 
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Ideas
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (UserId != null ? item.UserId == UserId : true)
                    && (Title != null ? item.Title == Title : true)
                select
                    item)
                .Skip<Idea>(numberPage * sizePage)
                .Take<Idea>(sizePage)
                .ToList<Idea>();
    }

    [HttpGet("/count")]
    public int getCount()
    {
        using( Context context = new())
            return context.Ideas.Count<Idea>();
    }
}
