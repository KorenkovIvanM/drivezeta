using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class ProjectController : GenericController<Project, Context>
{
    private readonly ILogger<ProjectController> _logger;
 
    public ProjectController(ILogger<ProjectController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<Project> get(
        int? Id, 
        string? Name,
        string? Discription,
        DateTime? Create,
        DateTime? End,
        StatusProject? Status,
        string? Icon,
        string? Color, 
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Projects
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (Name != null ? item.Name == Name : true)
                    && (Discription != null ? item.Discription == Discription : true)
                    && (Create != null ? item.Create == Create : true)
                    && (End != null ? item.End == End : true)
                    && (Status != null ? item.Status == Status : true)
                    && (Icon != null ? item.Icon == Icon : true)
                    && (Color != null ? item.Color == Color : true)
                select
                    item)
                .Take<Project>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Project>();
    }
}
