using Microsoft.AspNetCore.Mvc;
using AutoCRUD;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using System;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TimingController : GenericController<Order, Context>
{
    private readonly ILogger<TimingController> _logger;
 
    public TimingController(ILogger<TimingController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public override List<Order> get(string KategoryId, int numberPage = 0, int sizePage = 15)
    {
        int rt = Convert.ToInt32(KategoryId);
        using( Context context = new())
            return (
                from 
                    item in context.Orders join kategory in context.TimingKategorys on item.Id equals kategory.OrderId
                where true
                    && (kategory.Id == rt)
                select
                    item)
                //.Take<Order>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Order>();
    }
}
