using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TimingKategoryController : GenericController<TimingKategory, Context>
{
    private readonly ILogger<TimingKategoryController> _logger;
 
    public TimingKategoryController(ILogger<TimingKategoryController> logger)
    {
        _logger = logger;
    }
}