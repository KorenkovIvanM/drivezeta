using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TargetController : GenericController<Target, Context>
{
    private readonly ILogger<TargetController> _logger;
 
    public TargetController(ILogger<TargetController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<Target> get(
        int? Id, 
        int? UserId,
        string? Title, 
        int? ProjectId,
        int? Value,
        int? Max,
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Targets
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (UserId != null ? item.UserId == UserId : true)
                    && (Title != null ? item.Title == Title : true)
                    && (ProjectId != null ? item.ProjectId == ProjectId : true)
                    && (Value != null ? item.Value == Value : true)
                    && (Max != null ? item.Max == Max : true)
                select
                    item)
                .Take<Target>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Target>();
    }
}
