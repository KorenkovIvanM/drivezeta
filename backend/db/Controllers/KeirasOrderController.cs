using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class KeirasOrderController : GenericController<KeirasOrder, Context>
{
    private readonly ILogger<KeirasOrderController> _logger;
 
    public KeirasOrderController(ILogger<KeirasOrderController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public override List<Order> get(string KategoryId, int numberPage = 0, int sizePage = 15)
    {
        int rt = Convert.ToInt32(KategoryId);
        using( Context context = new())
            return (
                from 
                    item in context.Orders join keiras in context.Keirass on item.Id equals keiras.OrderId
                where true
                    && (keiras.Id == rt)
                select
                    item)
                //.Take<Order>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Order>();
    }
}