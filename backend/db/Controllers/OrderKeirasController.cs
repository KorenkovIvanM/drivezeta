using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class OrderKeirasController : GenericController<OrderKeiras, Context>
{
    private readonly ILogger<OrderKeirasController> _logger;
 
    public OrderKeirasController(ILogger<OrderKeirasController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<OrderKeiras> get(
        int? Id, 
        int? OrderId,
        int? KeirasId,
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.OrderKeirass
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (OrderId != null ? item.OrderId == OrderId : true)
                    && (KeirasId != null ? item.KeirasId == KeirasId : true)
                select
                    item)
                .Take<OrderKeiras>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<OrderKeiras>();
    }
}
