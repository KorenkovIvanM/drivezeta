using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class RoleProjectController : GenericController<RoleProject, Context>
{
    private readonly ILogger<RoleProjectController> _logger;
 
    public RoleProjectController(ILogger<RoleProjectController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<RoleProject> get(
        int? Id, 
        int? UserId,
        int? ProjectId,
        TypeRoleProject? Type,
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.RoleProjects
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (UserId != null ? item.UserId == UserId : true)
                    && (ProjectId != null ? item.ProjectId == ProjectId : true)
                    && (Type != null ? item.Type == Type : true)
                select
                    item)
                .Take<RoleProject>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<RoleProject>();
    }
}
