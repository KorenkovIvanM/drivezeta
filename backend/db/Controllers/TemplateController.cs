using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class TemplateController : GenericController<Template, Context>
{
    private readonly ILogger<TemplateController> _logger;
 
    public TemplateController(ILogger<TemplateController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<Template> get(
        int? Id, 
        int? UserId,
        int? OrderId,
        bool? Activ,
        int? Mask,
        DateTime? Time,
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Templates
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (UserId != null ? item.UserId == UserId : true)
                    && (OrderId != null ? item.OrderId == OrderId : true)
                    && (Activ != null ? item.Activ == Activ : true)
                    && (Mask != null ? item.Mask == Mask : true)
                    && (Time != null ? item.Time == Time : true)
                select
                    item)
                .Take<Template>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Template>();
    }
}
