using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class KeirasController : GenericController<Keiras, Context>
{
    private readonly ILogger<KeirasController> _logger;
 
    public KeirasController(ILogger<KeirasController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<Keiras> get(
        int? Id, 
        int? UserId,
        string? Title, 
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Keirass
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (UserId != null ? item.UserId == UserId : true)
                    && (Title != null ? item.Title == Title : true)
                select
                    item)
                .Take<Keiras>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<Keiras>();
    }
}
