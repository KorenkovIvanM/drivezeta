using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class OrderController : GenericController<Order, Context>
{
    private readonly ILogger<OrderController> _logger;
 
    private bool ValidationItem(ref Order item)
    {
        return GenerationOrder.Validation(ref item);
    }
    private void makeChild(ref IEnumerable<Order> lItem)
    {
        TreeOrder.Create(ref lItem);
    }

    public OrderController(ILogger<OrderController> logger)
    {
        this._Create += ValidationItem;
        this._Read += makeChild;
        _logger = logger;
    }

    // private Order getRootOrder(Order item)
    // {
    //     item.Parent = null;
    //     return item;
    // }

    // [HttpGet]
    // public List<Order> get()
    // {
    //     using( Context context = new())
    //         return (
    //             from 
    //                 item in context.Orders
    //             where true
    //                 // && (Id != null ? item.Id == Id : true)
    //                 // && (UserId != null ? item.UserId == UserId : true)
    //                 // && (Title != null ? item.Title == Title : true)
    //             select
    //                 getRootOrder(item))
    //             // .Skip<Order>(numberPage * sizePage)
    //             // .Take<Order>(sizePage)
    //             .ToList<Order>();
    // }
}