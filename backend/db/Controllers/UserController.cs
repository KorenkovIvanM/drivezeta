using Microsoft.AspNetCore.Mvc;
using AutoCRUD;

namespace backend.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : GenericController<User, Context>
{
    private readonly ILogger<UserController> _logger;
 
    public UserController(ILogger<UserController> logger)
    {
        _logger = logger;
    }
    
    [HttpGet]
    public List<User> get(
        int? Id, 
        string? Login,
        DateTime? LastLogin,
        string? Email, 
        int numberPage = 0, 
        int sizePage = 15)
    {
        using( Context context = new())
            return (
                from 
                    item in context.Users
                where 1 == 1
                    && (Id != null ? item.Id == Id : true)
                    && (Login != null ? item.Login == Login : true)
                    && (LastLogin != null ? item.LastLogin == LastLogin : true)
                    && (Email != null ? item.Email == Email : true)
                select
                    item)
                .Take<User>(new Range(numberPage*sizePage, (numberPage + 1)*sizePage))
                .ToList<User>();
    }
}
