using Hangfire;
using Hangfire.LiteDB;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddCors();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHangfire(configuration =>
{
    configuration.UseLiteDbStorage("./../../DataBase/JOBS.db");
});
builder.Services.AddHangfireServer();


var app = builder.Build();
app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseHangfireDashboard("/jobs");
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
MakeJobs.Run();
app.Run();
