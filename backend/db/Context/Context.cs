using Microsoft.EntityFrameworkCore;

public class Context : DbContext
{
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Project> Projects {get; set;} = null!;
    public DbSet<Idea> Ideas { get; set; } = null!;
    public DbSet<Keiras> Keirass { get; set; } = null!;
    public DbSet<Target> Targets { get; set; } = null!;
    public DbSet<RoleProject> RoleProjects { get; set; } = null!;
    public DbSet<Template> Templates { get; set; } = null!;
    public DbSet<OrderKeiras> OrderKeirass { get; set; } = null!;
    public DbSet<Order> Orders { get; set; } = null!;
    public DbSet<TimingKategory> TimingKategorys { get; set; } = null!;
    public Context() => Database.EnsureCreated();
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("data source=./../../DataBase/DRIVEZETA.db");
    }

}