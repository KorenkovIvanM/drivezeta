using AutoCRUD;
public class User
{
    public int Id { get; set; }
    public string Login { get; set; } = null!;
    public DateTime LastLogin { get; set; } = DateTime.Now;
    public string? Email { get; set; }
}