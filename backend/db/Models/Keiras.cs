public class Keiras
{
    public int Id { get; set; }
    public string Title { get; set; } = null!;
    public int UserId {get; set; }
    public override string ToString()
    {
        return $"{Id} {Title} {UserId}";
    }
}