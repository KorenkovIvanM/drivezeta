public class Project
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string? Discription { get; set; }
    public DateTime Create { get; set; } = DateTime.Now;
    public DateTime? End { get; set; }
    public StatusProject Status { get; set; } = StatusProject.Fine;
    public string Icon { get; set; } = "https://www.pinclipart.com/picdir/big/230-2302507_project-cycle-icon-clipart.png";
    public string Color { get; set; } =null!;
}

public enum StatusProject
{
    Fine,
    UnderThreat,
    Overdue,
    Closed,
    Failed,
}