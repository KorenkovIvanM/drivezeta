public class Target
{
    public int Id { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; } = null!;
    public int? ProjectId { get; set; }
    public int? TargetId { get; set; }
    public int Value { get; set; } = 0;
    public int Max { get; set; } = 100;
}