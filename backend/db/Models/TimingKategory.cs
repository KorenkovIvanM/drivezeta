public class TimingKategory
{
    public int Id { get; set; }
    public string Title { get; set; } = null!;
    public double Weight { get; set; } = 0;
    public int OrderId { get; set; }
}