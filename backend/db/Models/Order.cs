using System.ComponentModel.DataAnnotations;

public class Order
{
    public int Id { get; set; }
    public int? ProjectId { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; } = null!;
    public string? Discription { get; set; }
    public DateTime Create { get; set; } = DateTime.Now;
    public DateTime? End { get; set; } 
    private DateTime? start;
    public DateTime? Start 
    { 
        get
        {
            return start;
        }
        set
        {
            refreshDate?.Invoke(start, value);
            start = value;
        }
    }
    public DateTime? Stop { get; set; }
    public bool IsTemplate { get; set; }
    private StatusOrdr? status;
    public StatusOrdr? Status 
    { 
        get
        {
            return status;
        }
        set
        {
            status = value;
            if(value == StatusOrdr.Closed && progress != 100)
                ReachesEnd?.Invoke();
        }
    }
    public TypeOrder? Type { get; set; }
    public int Weight { get; set; }
    // public Order? Parent { get; set; }
    public int? ParentId { get; set; }
    public List<Order>? Children { get; set; }
    public int Prioritet { get; set; }
    public int TargetId { get; set; }
    private int progress = 0;
    public int Progress
    {
        get
        {
            return progress;
        }
        set
        {
            progress += value;
            if(progress > 100) progress = 100;
            if(progress == 100 && Status != StatusOrdr.Closed) ReachesEnd?.Invoke();
        }
    }

    delegate void ClosedOrder();
    event ClosedOrder ReachesEnd;
    private void _forClosed()
    {
        End = DateTime.Now;
        Prioritet = -1;
        if(Weight != 0)
        try
        {
            using(Context context = new())
            {
                (from item in context.Targets
                where item.Id == TargetId
                select item).First<Target>().Value += Weight;
                context.SaveChanges();
            }
        }
        catch(Exception ex)
        {
            System.Console.WriteLine(ex.Message);
        }
    }
    private void _forRefreshDate(DateTime? oldDate, DateTime? newDate)
    {
        newDate = newDate ?? DateTime.Now;
        oldDate = ((DateTime)newDate).AddHours(1);
        this.Stop = this.Stop ?? DateTime.Now;
        this.Stop += ((DateTime)newDate - (DateTime)oldDate);
    }
    delegate void RefreshDate(DateTime? oldDate, DateTime? newDate);
    event RefreshDate refreshDate;
    public Order()
    {
        this.ReachesEnd += _forClosed;
        this.refreshDate += _forRefreshDate;
    }

}

public enum StatusOrdr
{
    NonActiv,
    Activ,
    Closed,
    Overdue,
    Filed,
}

public enum TypeOrder
{
    Hard,
    Soft,
    Resolved,
    Cronos,
    Mounth,
    Week,
}