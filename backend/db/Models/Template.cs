public class Template
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int OrderId { get; set; }
    public bool Activ { get; set; }
    public int Mask { get; set; }
    public DateTime Time { get; set; }
}