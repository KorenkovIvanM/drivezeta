public class RoleProject
{
    public int Id { get; set; }
    public int? ProjectId { get; set; }
    public int? UserId { get; set; }
    public TypeRoleProject Type { get; set; }
}

public enum TypeRoleProject
{
    Developer,
    TimeLeder,
}