public abstract class Generation
{
    protected abstract bool GenerationOrder(ref Order item);
    public bool GetOrder(ref Order item)
    {
        return GenerationOrder(ref item);
    }
}