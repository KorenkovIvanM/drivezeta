public class GenerationHardOrder : Generation
{
    protected override bool GenerationOrder(ref Order item)
    {
        if(
            item.Start == null ||
            item.Title == null)
            return false;
        if(item.Stop == null)
            item.Stop = ((DateTime)item.Start).AddHours(1);
        return true;
    }
}