public class GenerationSoftOrder : Generation
{
    protected override bool GenerationOrder(ref Order item)
    {
        if(item.Title == null)
            return false;
        if(item.Stop == null)
        {
            item.Start = item.Start ?? DateTime.Today;
            item.Stop = item.Stop ?? DateTime.Today.AddDays(1);
        }
        return true;
    }
}