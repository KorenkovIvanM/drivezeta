public static class GenerationOrder
{
    public static bool Validation(ref Order item)
    {
        if(item.Type == TypeOrder.Hard)
        {
            var generation = new GenerationHardOrder();
            return generation.GetOrder(ref item);
        }
        if(item.Type == TypeOrder.Soft)
        {
            var generation = new GenerationSoftOrder();
            return generation.GetOrder(ref item);
        }
        if(item.Type == TypeOrder.Week)
        {
            var generation = new GenerationWeek();
            return generation.GetOrder(ref item);
        }
        if(item.Type == TypeOrder.Mounth)
        {
            var generation = new GenerationMonth();
            return generation.GetOrder(ref item);
        }
        return false;
    }
}