public class Component<T>
{
    public T Item { get; set; }
    public Component<T> Parent { get; set; }
    protected int _myIndex;
    public List<T> Child { get; set; }
    public virtual void Add(T component) => Child.Add(component);
    public virtual void RemoveChild(int index) => Child.Remove(Child[index]);
    public virtual void RemoveAllChild() => Child = new();
    public virtual void RemoveThis() => Parent.RemoveChild(_myIndex);
}