public static class TreeOrder
{
    public static void Create(ref IEnumerable<Order> list)
    {
        Dictionary<int, Order> buff = new();

        foreach (var item in list)
            buff[item.Id] = item;

        foreach (var item in buff)
            if(item.Value.ParentId != null && (int)item.Value.ParentId != 0)
            {
                if(buff[(int)item.Value.ParentId].Children == null)
                    buff[(int)item.Value.ParentId].Children = new();
                buff[(int)item.Value.ParentId].Children.Add(item.Value);
            }

        list = list.Where(item => item.ParentId == null || item.ParentId == 0);
    }
}