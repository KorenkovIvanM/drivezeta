public class Idea
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public string Title { get; set; } = null!;
    public string? Discription { get; set; }
    public DateTime Create { get; set; }
    public override string ToString()
    {
        return $"{Id} UserId: {UserId} Title: {Title} Discription: {Discription} Create: {Create}";
    }
}