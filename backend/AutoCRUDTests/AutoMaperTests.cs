﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoCRUD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCRUD.Tests
{
    public enum tenm
    {
        type1,
        type2
    }
    public class cA
    {
        public int Id {  get; set; }
    }
    public class cB
    {
        public int Idd { get; set; }
    }
    public class cC
    {
        public int? Id { get; set; }
        public int? Ida { get; set; }
        public int? bId { get; set; }
    }
    public class cD
    {
        public string Id { get; set; } = "";
    }

    public class cE
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public tenm _tenm { get; set; }
    }

    [TestClass()]
    public class AutoMaperTests
    {
        [TestMethod()]
        public void GetIdTest_past_id_1()
        {
            //Arrange
            cA calc = new cA { Id = 1 };
            //Act
            int? r1 = AutoMaper.GetId<cA>(calc);
            //Assert
            Assert.AreEqual(1, r1);
        }

        [TestMethod()]
        public void GetIdTest_past_id_11()
        {
            //Arrange
            cA calc = new cA { Id = 11 };
            //Act
            int? r1 = AutoMaper.GetId<cA>(calc);
            //Assert
            Assert.AreEqual(11, r1);
        }

        [TestMethod()]
        public void GetIdTest_past_id_null()
        {
            //Arrange
            cB calc = new cB { Idd = 17 };
            //Act
            int? r1 = AutoMaper.GetId<cB>(calc);
            //Assert
            Assert.AreEqual(null, r1);
        }

        [TestMethod()]
        public void GetIdTest_past_id_samid()
        {
            //Arrange
            cC calc = new cC { Id = 1, Ida = 2, bId = 3 };
            //Act
            int? r1 = AutoMaper.GetId<cC>(calc);
            //Assert
            Assert.AreEqual(1, r1);
        }

        //[TestMethod()]
        //public void GetIdTest_past_id_other()
        //{
        //    //Arrange
        //    cD calc = new cD { Id = "123" };
        //    //Act
        //    int? r1 = AutoMaper.GetId<cD>(calc);
        //    //Assert
        //    Assert.AreF(1, r1);
        //}












        [TestMethod()]
        public void UpdateTest()
        {
            //Arrange
            cC item1 = new cC { Id = 1, Ida = 2, bId = 3 };
            cC item2 = new();
            //Act
            AutoMaper.Update(ref item2, ref item1);
            //Assert
            Assert.AreEqual(item1.Id, item2.Id);
            Assert.AreEqual(item1.Ida, item2.Ida);
            Assert.AreEqual(item1.bId, item2.bId);
        }


        [TestMethod()]
        public void isMaskTest_enum()
        {
            //Arrange
            cE c1 = new() { Id = 1, Name = "1", _tenm = tenm.type1 };
            cE c2 = new() { Id = 1, Name = "2", _tenm = tenm.type1 };
            cE c3 = new() { Id = 1, Name = "3", _tenm = tenm.type2 };
            cE c4 = new() { Id = 1, Name = "4", _tenm = tenm.type2 };
            cE c5 = new() { Id = 1, Name = "5", _tenm = tenm.type2 };
            //Act
            bool t1 = AutoMaper.IsMask(c1, c1);
            bool t2 = AutoMaper.IsMask(c1, c2);
            bool t3 = AutoMaper.IsMask(c1, c3);
            bool t4 = AutoMaper.IsMask(c1, c4);
            bool t5 = AutoMaper.IsMask(c1, c5);
            //Assert
            Assert.AreEqual(t1, true);
            Assert.AreEqual(t2, true);
            Assert.AreEqual(t3, false);
            Assert.AreEqual(t4, false);
            Assert.AreEqual(t5, false);
        }
    }
}