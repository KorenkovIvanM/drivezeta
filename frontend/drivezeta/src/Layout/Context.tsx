import { Routes, Route } from "react-router-dom"
import State from "./../redux/_delet_me_place_"
import React, { useState } from 'react'


export default function PersistentDrawerLeft() {
    return (
        <>
            <Routes>
                {State.PageContext.map(page => <Route key={page.URL} path={page.URL} element={page.Page} />)}
            </Routes>
        </>

    );
}
