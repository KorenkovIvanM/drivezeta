import React, { useState } from 'react'
import type { MenuProps } from 'antd'
import { Menu } from 'antd'
import { useNavigate } from 'react-router-dom'
import State from "./../redux/_delet_me_place_"

const App: React.FC = () => {
  const items = State.Pages

  const [current, setCurrent] = useState('1');

  const navigate = useNavigate();
  const onClick: MenuProps['onClick'] = (e) => { 
    setCurrent(e.key); 
    navigate(e.key, { replace: true })
  };

  return (
    <Menu
      theme={'dark'}
      onClick={onClick}
      // TODO move module css
      style={{ width: "100%", height: "calc(100vh - 50px)" }}
      defaultOpenKeys={['sub1']}
      selectedKeys={[current]}
      mode="inline"
      items={items as any}
    />
  )
}

export default App