import React, { useState, useEffect } from 'react'
import { Table, Card, Button, message } from 'antd'
import type { ColumnsType, TableProps } from 'antd/es/table'
import { Get } from '../Axios/EntitysCRUD'
import { useNavigate, Link } from 'react-router-dom'
import ButtonForm from './ButtonStart/ButtonForm'
import {
    PlayCircleOutlined,
    StopOutlined,
    DeploymentUnitOutlined,
    SearchOutlined,
    InfoOutlined,
} from '@ant-design/icons'
import TypeStateMachine from '../Libraries/TypeStateMachine'
import ButtonButton from './ButtonStart/ButtonButton'
import ButtonAddationalFuncton from './ButtonAddationalFuncton'

interface PropsListWorkerPage {
    all: boolean
}

const columns: ColumnsType<any> = [
    {
        title: 'title',
        dataIndex: 'title'
    },
    {
        title: 'description',
        dataIndex: 'description',
    },
    {
        title: 'status',
        dataIndex: 'status',
        width: 40,
        render: (item, example) => {
            if (example.status === 0) return <PlayCircleOutlined style={{ color: "#d4ed31" }} />
            if (example.status === 1) return <StopOutlined style={{ color: "#f59794" }} />
            if (example.status === 2) return <DeploymentUnitOutlined style={{ color: "#bccefb" }} />
        }
    },
];

const onChange: TableProps<any>['onChange'] = (pagination, filters, sorter, extra) => {
    console.log('params', pagination, filters, sorter, extra);
};

const ListWorkerPage = (props: PropsListWorkerPage) => {
    const [data, setData] = useState<Array<any>>([])
    useEffect(() => {
        if (props.all)
            Get((result) => setData(result), "StateMachine", {}, 0, 50)
        else
            Get((result) => setData(result), "StateMachine", { "Type": 7 }, 0, 50)
    }, [])
    if (columns[columns.length - 1].key !== 'info')
        columns.push({
            title: 'Старт/Стоп', dataIndex: '', key: 'createChaldren', width: 40,
            render: (item) => {
                if (item.type === TypeStateMachine.Button) return <ButtonButton item={item} />
                else if (item.type === TypeStateMachine.Form) return <ButtonForm item={item} />
                else if (item.type === TypeStateMachine.Date) return <ButtonButton item={item} />
                else return <></>
            }
        });
    if (columns[columns.length - 1].key !== 'info')
        columns.push({
            title: 'Мониторинг', dataIndex: '', key: 'monitoring', width: 40,
            render: (item) =>
                <ButtonAddationalFuncton item={item} />
        });
    if (columns[columns.length - 1].key !== 'info')
        columns.push({
            title: 'Инвормация', dataIndex: '', key: 'info', width: 40,
            render: (item) => <Button style={{ color: "#bccefb" }} icon={<InfoOutlined />} />
        });

    return <Table columns={columns} dataSource={data} onChange={onChange} />
}

export default ListWorkerPage