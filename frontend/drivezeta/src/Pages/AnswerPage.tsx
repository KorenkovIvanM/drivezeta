import React from 'react';
import { Card } from 'antd';
import FullTable from './../UI/FullTable'
import "./../index.css"
import TypeColumns from '../Libraries/TypeColumns';

const AnswerTablePage: React.FC = () => {
    return (
        <Card className='cardPage'>
            <FullTable 
            Columns={[
                { 
                    title: "lastUse", 
                    dataIndex: "lastUse", 
                    key: "lastUse", 
                    type: TypeColumns.Date, 
                    isUpdate: true, },
                { 
                    title: "result", 
                    dataIndex: "result", 
                    key: "result", 
                    type: TypeColumns.String, 
                    isUpdate: true, },
                { 
                    title: "data", 
                    dataIndex: "data", 
                    key: "data", 
                    type: TypeColumns.String, 
                    isUpdate: true, },
                { 
                    title: "statusMachineId", 
                    dataIndex: "statusMachineId", 
                    key: "statusMachineId", 
                    type: TypeColumns.String, 
                    isUpdate: true, },
            ]}
            Entity={"Answer"} />
        </Card>
    )
}

export default AnswerTablePage