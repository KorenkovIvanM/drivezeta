import PropsStateMachine from '../Libraries/Props/PropsStateMachine'
import FullTable from '../UI/FullTable'
import TypeColumns from '../Libraries/TypeColumns'
import ColumnCrud from '../Libraries/Column'

const NameEntity: string = "Argument";
const _columns: Array<ColumnCrud> = [
    {
        title: "Arguments",
        dataIndex: "arguments",
        key: "arguments",
        type: TypeColumns.String,
        isUpdate: true,
        sorter: (a, b) => a.title.length - b.title.length
    },
]

const _argumentCreatestatemachine = (props: PropsStateMachine) => {
    if (props.item !== null)
        return (
            <FullTable
                Height={400}
                Columns={_columns}
                Mask={{ StateMachineId: props.item?.id }}
                Entity={NameEntity} />
        )
    else
        return <></>
}

export default _argumentCreatestatemachine