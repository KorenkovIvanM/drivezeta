import { Input, Col, Row, Card, Button, message } from 'antd'
import PropsStateMachine from "../../Libraries/Props/PropsStateMachine"
import TypeStateMachine from "../../Libraries/TypeStateMachine"
import Handler from "./Handler"
import FormHandler from './FormHandler'
import React, { useState } from 'react'
import { UploadOutlined } from '@ant-design/icons'
import type { UploadProps } from 'antd'
import { Post } from '../../Axios/EntitysCRUD'
import Argument from '../../Libraries/Entitys/Argument'

const { TextArea } = Input;

class SQLHandler extends Handler {

    protected NextHandler = null
    public GetResult(props: PropsStateMachine) {
        let sql = ""
        if (props.item?.type === TypeStateMachine.SQL) {
            return (
                <div style={{padding: 10}}>
                    <Row>
                        <Col span={24} >
                            <TextArea
                                placeholder="SQL"
                                onInput={(e) => sql = (e.target as any).value}
                                autoSize={{ minRows: 3, maxRows: 5 }}
                            />
                            <Button
                                onClick={(e) => {
                                    Post((data) => { message.success(data) }, "Argument", {
                                        stateMachineId: props.item?.id,
                                        arguments: sql
                                    })
                                }}
                                style={{ marginTop: 10 }}>Save</Button>
                        </Col>
                    </Row>
                </div>
            )
        }
        else {
            if (this.NextHandler !== null)
                return (this.NextHandler as Handler).GetResult(props)
            else
                return <p>Not data</p>
        }
    }
}

export default SQLHandler