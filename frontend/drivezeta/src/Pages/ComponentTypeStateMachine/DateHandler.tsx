import { Input, Col, Row, Card, Button, message } from 'antd'
import PropsStateMachine from "../../Libraries/Props/PropsStateMachine"
import TypeStateMachine from "../../Libraries/TypeStateMachine"
import Handler from "./Handler"
import FormHandler from './FormHandler'
import React, { useState } from 'react'
import { UploadOutlined } from '@ant-design/icons'
import type { UploadProps } from 'antd'
import { Post } from '../../Axios/EntitysCRUD'

class DateHandler extends Handler {

    protected NextHandler = new FormHandler()
    public GetResult(props: PropsStateMachine) {
        let Demon = ""
        if (props.item?.type === TypeStateMachine.Date) {
            return (
                <Card style={{ margin: 4, width: "calc(100% - 6px)", height: "calc(100vh - 512px)" }}>
                    <Row>
                        <Col span={24} >
                            <Input 
                                placeholder="Demon"
                                onInput={(e) => {Demon = (e.target as any).value}} />
                            <Button 
                                onClick={ (e) => {
                                    Post((date) => { message.success(date) }, "DateDemon", {
                                        StartMachneDbId: props.item?.id,
                                        Demon: Demon,
                                        Type: 0
                                    })
                                }}
                                style={{marginTop: 10}}>Save</Button>
                        </Col>
                    </Row>
                </Card>
            )
        }
        else {
            if (this.NextHandler !== null)
                return (this.NextHandler as Handler).GetResult(props)
            else
                return <p>Not data</p>
        }
    }
}

export default DateHandler