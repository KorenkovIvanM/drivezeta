import React from "react"
import PropsStateMachine from "../../Libraries/Props/PropsStateMachine"
import PythonHandler from "./PythonHandler"

const start: PythonHandler = new PythonHandler()

const _componentStateMachine = (props: PropsStateMachine) => start.GetResult(props)

export default _componentStateMachine