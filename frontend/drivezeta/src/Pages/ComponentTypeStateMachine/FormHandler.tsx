import PropsStateMachine from "../../Libraries/Props/PropsStateMachine"
import TypeStateMachine from "../../Libraries/TypeStateMachine"
import Handler from "./Handler"

import React from 'react'
import FullTable from '../../UI/FullTable'
import TypeColumns from '../../Libraries/TypeColumns'
import ColumnCrud from '../../Libraries/Column'
import SQLHandler from "./SQLHandler"

const NameEntity: string = "Input";
const _columns: Array<ColumnCrud> = [
    {
        title: "Name",
        dataIndex: "name",
        key: "name",
        type: TypeColumns.String,
        isUpdate: true,
        sorter: (a, b) => a.title.length - b.title.length
    },
]


class FormHandler extends Handler {

    protected NextHandler = new SQLHandler()
    public GetResult(props: PropsStateMachine) {

        if (props.item?.type === TypeStateMachine.Form) {
            debugger
            return (
                <FullTable
                    Height={400}
                    Columns={_columns}
                    Mask={{ startStateMachineDbId: props.item?.id }}
                    Entity={NameEntity} />
            )
        }
        else {
            if (this.NextHandler !== null)
                return (this.NextHandler as Handler).GetResult(props)
            else
                return <p>Not data</p>
        }
    }
}

export default FormHandler