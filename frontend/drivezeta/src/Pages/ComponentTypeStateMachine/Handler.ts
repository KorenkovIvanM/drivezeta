import React from "react";
import PropsStateMachine from "../../Libraries/Props/PropsStateMachine";

abstract class Handler {
    protected abstract NextHandler: Handler | null
    // TODO исправь тип возврошаемого значения
    public abstract GetResult(props: PropsStateMachine): any
}

export default Handler