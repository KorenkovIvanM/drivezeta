import { Upload, Button, message, Col, Row, Card } from 'antd'
import PropsStateMachine from "../../Libraries/Props/PropsStateMachine"
import TypeStateMachine from "../../Libraries/TypeStateMachine"
import Handler from "./Handler"

import React, { useState } from 'react'
import { UploadOutlined } from '@ant-design/icons'
import type { UploadProps } from 'antd'
import DateHandler from './DateHandler'

class PythonHandler extends Handler {

    protected NextHandler = new DateHandler()
    public GetResult(props: PropsStateMachine) {
        const _props: UploadProps = {
            name: 'file',
            action: `https://localhost:7177/ToolsWorker/File?IdStateMachine=${props.item?.id}`,
            onChange(info) {
                if (info.file.status === 'done') {
                    message.success(`${info.file.name} file uploaded successfully`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} file upload failed.`);
                }
            },
            progress: {
                strokeColor: {
                    '0%': '#108ee9',
                    '100%': '#87d068',
                },
                strokeWidth: 3,
                format: (percent) => percent && `${parseFloat(percent.toFixed(2))}%`,
            },
        };
        if (props.item?.type === TypeStateMachine.Python) {
            return (
                <Card style={{ margin: 4, width: "calc(100% - 6px)", height: "calc(100vh - 512px)" }}>
                    <Row>
                        <Col span={24} >
                            <Upload {..._props} style={{ margin: 10, width: '100%' }} >
                                <Button icon={<UploadOutlined />} style={{ margin: 10, width: '100%' }} >Click to Upload</Button>
                            </Upload>
                        </Col>
                        <Col span={24} >

                        </Col>
                    </Row>
                </Card>
            )
        }
        else {
            if (this.NextHandler !== null)
                return (this.NextHandler as Handler).GetResult(props)
            else
                return <p>Not data</p>
        }
    }
}

export default PythonHandler