import { Table, Card, Button, message } from 'antd'
import axios from 'axios'
import PropsStateMachine from '../../Libraries/Props/PropsStateMachine'

const ButtonButton = (props: any) => {
    return (
        <Button
            onClick={
                (e) => {
                    // TODO убери в axios
                    axios({
                        method: 'post',
                        url: `https://localhost:7177/ToolsWorker/Start?Id=${props.item.id}`,
                    }).then((respomce) => { message.success(`result ${respomce.data}`)
                    }).catch((errors) => { message.error(`errors ${errors}`) })
                }
            }
            style={{ color: props.item.status === 2 ? "#f59794" : "#d4ed31" }}>
            {props.item.status === 1 ? "Стоп" : "Старт"}
        </Button>
    )
}

export default ButtonButton