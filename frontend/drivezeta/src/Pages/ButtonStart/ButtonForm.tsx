import { useState } from "react"
import { Get, Post } from "../../Axios/EntitysCRUD"
import ButtonDialog from "../../UI/ButtonDialog"
import { message } from "antd"
import axios from "axios"

const ButtonForm = (props: any) => {
    const [listPlaceholder, setListPlaceholder] = useState<Array<any>>([])
    if (listPlaceholder.length === 0)
        Get(setListPlaceholder, "Input", { FormStartStateMachineDbId: props.item?.id })
    return (
        <ButtonDialog
            ListPlaceholder={listPlaceholder.map((item) => item.name)}
            CallBack={(data) => {
                let result: string = ''
                for (let i in data)
                    result += `${i}=${data[i]} `
                // TODO убери в axios
                console.log(`https://localhost:7177/ToolsWorker/Start?Id=${props.item.id}&Argument=${result}`)
                axios({
                    method: 'post',
                    url: `https://localhost:7177/ToolsWorker/Start?Id=${props.item.id}&Argument=${result}`,
                }).then((respomce) => { message.success(`result ${respomce.data}`)
                }).catch((errors) => { message.error(`errors ${errors}`) })
            }} />
    )
}

export default ButtonForm