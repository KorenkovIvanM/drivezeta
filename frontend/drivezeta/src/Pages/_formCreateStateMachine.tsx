import { Card, Input, Select, Button, message } from 'antd'
import TypeStateMachine from '../Libraries/TypeStateMachine'
import { useEffect, useState } from 'react'
import ItemSearch from '../Libraries/ItemSearch'
import StateMachine from '../Libraries/StateMachine'
import PropsStateMachine from '../Libraries/Props/PropsStateMachine'
import { Get, Put } from '../Axios/EntitysCRUD'

const _formCreateStateMachine = (props: PropsStateMachine) => {

    const [stateMachines, setStateMachines] = useState<Array<ItemSearch<number>>>([]);
    useEffect(() => {
        Get((data) => setStateMachines(data.map((e) => new ItemSearch(e.id, e.title))), "StateMachine", {}, 0, 100);
    }, [])

    const [title, setTitle] = useState<string | null>(null)
    const [description, setDescription] = useState<string | null>(null)
    const [typeSM, setTypeSM] = useState<TypeStateMachine | null>(null)

    const dataForType: Array<ItemSearch<number>> = []
    for (let index = 0, max = Object.keys(TypeStateMachine).length / 2; index < max; index++)
        dataForType[index] = new ItemSearch(index, TypeStateMachine[index])
    const onChange = (value: TypeStateMachine) => {
        setTypeSM(value)
    }
    return (
        <div style={{padding: 10}}>
            <Select
                showSearch
                style={{ marginBottom: 6, width: "100%" }}
                placeholder="Id"
                optionFilterProp="children"
                onChange={
                    (value: number) => {
                        Get((data) => { props.setItem(data[0]) }, "StateMachine", { Id: value }, 0, 100);
                    }
                }
                filterOption={(input, option) =>
                    (option?.value.toString() ?? '').toLowerCase().includes(input.toLowerCase())
                }
                options={stateMachines} />
            <Input
                //value={props.item?.id}
                style={{ marginBottom: 10 }}
                disabled
                placeholder="Id" />
            <Input
                //value={props.item?.title}
                onChange={(e) => setTitle(e.target.value)}
                status={title === null ? "error" : ""}
                style={{ marginBottom: 10 }}
                placeholder="Title" />
            <Input
                //value={props.item?.description || ""}
                onChange={(e) => setDescription(e.target.value)}
                style={{ marginBottom: 10 }}
                placeholder="description" />
            <Select
                //value={props.item?.type || typeSM}
                showSearch
                status={typeSM === null ? "error" : ""}
                style={{ width: "100%", marginBottom: 10 }}
                placeholder="Type state machin"
                optionFilterProp="children"
                onChange={onChange}
                filterOption={(input, option) =>
                    (option?.value.toString() ?? '').toLowerCase().includes(input.toLowerCase())
                }
                options={dataForType} />
            <Input
                value={props.item?.successId}
                style={{ marginBottom: 10 }}
                disabled
                placeholder="Success" />
            <Input
                value={props.item?.failedId}
                style={{ marginBottom: 10 }}
                disabled
                placeholder="Failed" />
            <Input
                value={props.item?.errorId}
                style={{ marginBottom: 10 }}
                disabled
                placeholder="Errors" />
            <Button
                block
                onClick={() => {
                    if (props.item == null) {
                        if (title !== null && typeSM != null) {
                            const _buff = new StateMachine(title as string, typeSM as TypeStateMachine, props.setItem, description)
                            setTitle(null)
                            setTypeSM(null)
                            setDescription(null)
                            _buff.SaveDb(props.setItem)
                        }
                    }
                    else
                    {
                        Put(props.setItem, "StateMachine", {
                            ...props.item,
                            title: title || props.item.title,
                            description: description || props.item.description,
                            typeSM: typeSM || props.item.type,
                        })
                    }
                }}
                style={{ marginTop: 50 }}>{props.item === null ? "Create" : "Update"}</Button>
        </div>
    )
}

export default _formCreateStateMachine