import { Card, Col, Row } from "antd"
import StateMachine from "../Libraries/StateMachine"
import { useEffect, useState } from "react"
import { Select, message } from 'antd'
import { Get } from "../Axios/EntitysCRUD"
import axios from "axios"
import FormDialog from "../UI/FormDialog"

const onSearch = (value: string) => {
    console.log('search:', value);
};

const SQLDataPage = () => {
    const [item, setItem] = useState({} as StateMachine)
    const [requests, setRequests] = useState<Array<StateMachine>>([])
    // const []
    const onChange = (value: string) => {
        setItem(requests.filter((e) => e.id === Number(value))[0])
    };
    useEffect(() => {
        axios({
            method: 'get',
            url: "https://localhost:7177/SQL",
        }).then((respomce) => {
            if (respomce.data)
                setRequests(respomce.data)
        })
            .catch((errors) => { message.error(`errors ${errors}`) })
    }, [])
    return (
        <Row style={{ height: "100%" }}>
            <Col span={8} style={{ height: "100%", padding: 10 }}>
                <Select
                    style={{ width: "100%" }}
                    showSearch
                    placeholder="Select SQL request"
                    optionFilterProp="children"
                    onChange={onChange}
                    onSearch={onSearch}
                    filterOption={(input, option) =>
                        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    options={requests.map((e) => { return { value: e.id, label: e.title } })} />
                <FormDialog
                    ListPlaceholder={[]}
                    CallBack={(data) => console.log(data)} />
            </Col>
            <Col span={16} style={{ height: "100%", padding: 10 }}>
                History
            </Col>
        </Row>
    )
}

export default SQLDataPage