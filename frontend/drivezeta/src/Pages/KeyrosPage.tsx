import React from 'react';
import { Card } from 'antd';
import FullTable from './../UI/FullTable'
import "./../index.css"
import type { ColumnsType } from 'antd/es/table'
import TypeColumns from '../Libraries/TypeColumns';

export default () => {
    return (
        <Card className='cardPage'>
            <FullTable 
            Columns={[
                { 
                    title: "title", 
                    dataIndex: "title", 
                    key: "title", 
                    type: TypeColumns.String, 
                    isUpdate: true,
                    sorter: (a, b) => a.title.length - b.title.length },
                { title: "description", dataIndex: "description", key: "description", type: TypeColumns.String, isUpdate: true },
            ]}
            Entity={"Keyros"} />
        </Card>
    )
}