import React, { useState } from 'react'
import { Button, Dropdown, Space } from 'antd'
import StateMachine from '../Libraries/StateMachine'
import { EyeOutlined } from '@ant-design/icons'
import AddationalFunctions from '../Libraries/Entitys/AddationalFunctions'
import { Get } from '../Axios/EntitysCRUD'
import { useNavigate } from 'react-router-dom'
import type { MenuProps } from 'antd'

interface PropsButtonAddationalFuncton {
    item: StateMachine
}


const ButtonAddationalFuncton = (props: PropsButtonAddationalFuncton) => {
    const navigate = useNavigate();
    const [addational, setAddational] = useState<AddationalFunctions>()
    
    return (
        <Button
            onClick={(e) => {
                Get(
                    (data: any) => {setAddational(data[0]) },
                    "AddationalFunction",
                    { StateMachineId: props.item.id } as AddationalFunctions)
                    console.log(addational)
                navigate(addational?.url as string, { replace: true })}}
        >Addational</Button>
    )
}

export default ButtonAddationalFuncton;