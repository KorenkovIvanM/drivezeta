import React from 'react'
import { Card } from 'antd'
import FullTable from './../UI/FullTable'
import "./../index.css"
import type { ColumnsType } from 'antd/es/table'
import TypeColumns from '../Libraries/TypeColumns'

export default () => {
    return (
        <Card className='cardPage'>
            <FullTable 
            Columns={[
                { 
                    title: "Placeholder", 
                    dataIndex: "placeholder", 
                    key: "placeholder", 
                    type: TypeColumns.String, 
                    isUpdate: true,
                    sorter: (a, b) => a.title.length - b.title.length 
                },
                { 
                    title: "type", 
                    dataIndex: "type", 
                    key: "type", 
                    type: TypeColumns.Number, 
                    isUpdate: true 
                },
                { 
                    title: "status", 
                    dataIndex: "status", 
                    key: "status", 
                    type: TypeColumns.Number, 
                    isUpdate: true 
                },
                { 
                    title: "type", 
                    dataIndex: "type", 
                    key: "type", 
                    type: TypeColumns.Number, 
                    isUpdate: true 
                },
            ]}
            Entity={"Inputs"} />
        </Card>
    )
}