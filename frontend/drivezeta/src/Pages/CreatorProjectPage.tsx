import { Card, Avatar, Row, Col } from 'antd'
import { SmileOutlined } from '@ant-design/icons'
import CardProject from './../UI/CardProject'
const { Meta } = Card


const CreatorProjectPage: React.FC = () => {
    return (
        <Row gutter={[24, 24]} style={{padding: 30, width: 'calc(100% - 60px)'}}>
            <Col span={6}>
                <CardProject />
            </Col>
            <Col span={6}>
                <CardProject />
            </Col>
        </Row>
    )
}

export default CreatorProjectPage;