import { Card, Select, Button } from 'antd'
import { useEffect, useState } from 'react'
import ItemSearch from '../Libraries/ItemSearch'
import PropsStateMachine from '../Libraries/Props/PropsStateMachine'
import { Get, Put } from '../Axios/EntitysCRUD'


const _stateCreateStateMachine = (props: PropsStateMachine) => {
    const onChange = (value: string) => {
        console.log(`selected ${value}`)
    }

    const [stateMachines, setStateMachines] = useState<Array<ItemSearch<number>>>([]);
    useEffect(() => {
        Get((data) => setStateMachines(data.map((e) => new ItemSearch(e.id, e.title))), "StateMachine", {}, 0, 100);
    }, [])
    if (props.item !== null)
        return (
            <div style={{padding: 10}}>
                <Select
                    showSearch
                    style={{ marginBottom: 6, width: "100%" }}
                    placeholder="on Success"
                    optionFilterProp="children"
                    onChange={
                        (value: number) => {
                            if (props.item !== null)
                            {
                                props.item.successId = value
                                props.setItem({...props.item})
                            }
                        }
                    }
                    filterOption={(input, option) =>
                        (option?.value.toString() ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    options={stateMachines} />
                <Select
                    showSearch
                    style={{ marginBottom: 6, width: "100%" }}
                    placeholder="on Failed"
                    optionFilterProp="children"
                    onChange={
                        (value: number) => {
                            if (props.item !== null)
                            {
                                props.item.failedId = value
                                props.setItem({...props.item})
                            }
                        }
                    }
                    filterOption={(input, option) =>
                        (option?.value.toString() ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    options={stateMachines} />
                <Select
                    showSearch
                    style={{ marginBottom: 6, width: "100%" }}
                    placeholder="on Error"
                    optionFilterProp="children"
                    onChange={
                        (value: number) => {
                            if (props.item !== null)
                            {
                                props.item.errorId = value
                                props.setItem({...props.item})
                            }
                        }
                    }
                    filterOption={(input, option) =>
                        (option?.value.toString() ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    options={stateMachines} />
                <Button
                    block
                    onClick={() => {
                        Put((data) => console.log(`result :: ${data}`), "StateMachine", props.item)
                        //props.item?.SaveDb((data) => console.log(`result :: ${data}`))
                    }}
                    style={{ marginTop: 50 }}>Save</Button>
                    <hr />
            </div>
        )
    else
        return <></>
}

export default _stateCreateStateMachine