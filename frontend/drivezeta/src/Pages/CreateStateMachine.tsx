import { Row, Col } from 'antd'
import { useState } from 'react'
import StateMachine from '../Libraries/StateMachine'
import S_formCreateStateMachine from './_formCreateStateMachine'
import S_argumentCreatestatemachine from './_argumentCreatestatemachine'
import S_stateCreateStateMachine from './_stateCreateStateMachine'
import S_componentStateMachine from './ComponentTypeStateMachine/ComponentTypeStateMachine'
import { TableAddationalfunction } from './Tables/TableAddationalfunction'

const CteateStateMachine = () => {
    const [stateMachine, setStateMachine] = useState<StateMachine | null>(null)
    return (
        <Row>
            <Col span={6}>
                <S_formCreateStateMachine
                    item={stateMachine}
                    setItem={setStateMachine} />
            </Col>
            <Col span={12} style={{ paddingTop: 10 }}>
                <Row>
                    <Col span={24}>
                        <S_argumentCreatestatemachine
                            item={stateMachine}
                            setItem={setStateMachine} />
                    </Col>
                    <Col span={24}>
                        <TableAddationalfunction
                            item={stateMachine}
                            setItem={setStateMachine} />
                    </Col>
                </Row>
            </Col>
            <Col span={6}>
                <Row>
                    <Col span={24}>
                        <S_stateCreateStateMachine
                            item={stateMachine}
                            setItem={setStateMachine} />
                    </Col>
                    <Col span={24}>
                        <S_componentStateMachine
                            item={stateMachine}
                            setItem={setStateMachine} />
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}

export default CteateStateMachine