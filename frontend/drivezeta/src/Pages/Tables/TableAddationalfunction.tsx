import React from 'react'
import FullTable from '../../UI/FullTable'
import TypeColumns from '../../Libraries/TypeColumns'
import PropsStateMachine from '../../Libraries/Props/PropsStateMachine'

const TableAddationalfunction = (props: PropsStateMachine) => {
    if (props.item !== null)
        return (
            <FullTable
                Columns={[
                    {
                        title: "title",
                        dataIndex: "title",
                        key: "title",
                        type: TypeColumns.String,
                        isUpdate: true,
                        sorter: (a, b) => a.title.length - b.title.length
                    },
                    {
                        title: "uRL",
                        dataIndex: "uRL",
                        key: "uRL",
                        type: TypeColumns.String,
                        isUpdate: true
                    },
                    {
                        title: "namePage",
                        dataIndex: "namePage",
                        key: "namePage",
                        type: TypeColumns.String,
                        isUpdate: true
                    },
                ]}
                Mask={{stateMachineId: props.item.id}}
                Entity={"AddationalFunction"} />

        )
    else return <></>
}

export {
    TableAddationalfunction
}