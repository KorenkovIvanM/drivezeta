import React from 'react'
import FullTable from '../../UI/FullTable'
import TypeColumns from '../../Libraries/TypeColumns'

const TableStateMachine = () => {
    return (
        <FullTable 
        Columns={[
            { 
                title: "title", 
                dataIndex: "title", 
                key: "title", 
                type: TypeColumns.String, 
                isUpdate: true,
                sorter: (a, b) => a.title.length - b.title.length 
            },
            { 
                title: "description", 
                dataIndex: "description", 
                key: "description", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
            { 
                title: "success", 
                dataIndex: "success", 
                key: "success", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
            { 
                title: "failed", 
                dataIndex: "failed", 
                key: "failed", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
            { 
                title: "error", 
                dataIndex: "error", 
                key: "error", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
            { 
                title: "type", 
                dataIndex: "type", 
                key: "type", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
            { 
                title: "status", 
                dataIndex: "status", 
                key: "status", 
                type: TypeColumns.String, 
                isUpdate: true 
            },
        ]}
        Entity={"StateMachine"} />
)
}

export {
    TableStateMachine
}