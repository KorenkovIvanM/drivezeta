import Calendar from './../UI/Calendar'
import { Card } from 'antd'

const CalendarWorker = () => {
    return (
        <Card style={{margin: 16, width: "calc(100% - 32px)", height: "calc(100% - 32px)"}}>
            <Calendar  />
        </Card>
    )
}

export default CalendarWorker