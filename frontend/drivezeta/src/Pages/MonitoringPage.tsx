import React, { useEffect, useState } from 'react'
import { Col, Row, Card, Avatar } from 'antd'
import Chart from '../Charts/Chart'
import PartialCharts from '../Libraries/PartialChrts'
import LoreSigreestr from '../Charts/LoreSigReestr'
import Queue from '../Libraries/Queue'
import { Get } from '../Axios/EntitysCRUD'
import {
    PlayCircleOutlined,
    StopOutlined,
    DeploymentUnitOutlined,
    SearchOutlined,
    InfoOutlined,
} from '@ant-design/icons'
const { Meta } = Card;

interface MonitoringPageProps {
    data: Array<PartialCharts>
    entity: String
    mask: any
}

const MonitoringPage = (props: MonitoringPageProps) => {
    const [queues, setQueue] = useState<Array<Queue>>([])
    useEffect(() => {
        Get((result) => setQueue(result.map((item) => {
            let result = { ...item, valueQueue: 0 }
            if (item.description === "Очередь 1") result.valueQueue = 282
            return result
        })), props.entity as string, props.mask)
    }, [])
    return (
        <Row style={{ width: "calc(100% - 20px)", padding: 10 }} gutter={[16, 16]}>
            <Col className="gutter-row" span={24}>
                <Card>
                    <Row style={{ width: "calc(100% - 32px)", height: "100%", padding: 10 }} gutter={[16, 16]}>
                        <Col className="gutter-row" span={4}>
                            <Chart data={props.data} />
                        </Col>
                        <Col className="gutter-row" span={8}>
                            <LoreSigreestr data={props.data} />
                        </Col>
                        <Col className="gutter-row" span={12}>
                        </Col>
                    </Row>
                </Card>
            </Col>
            {queues.map(item => {
                return (
                    <Col className="gutter-row" span={6}>
                        <Card
                            actions={[
                                <PlayCircleOutlined style={{ color: item.status !== 0 ? "#f59794" : "#d4ed31" }} />,
                                <StopOutlined style={{ color: item.status === 0 ? "#f59794" : "#d4ed31" }} />,
                                <InfoOutlined style={{ color: "#bccefb" }} />,
                            ]}>
                            <Meta title={item.title} description={item.description} />
                            <hr />
                            <Meta description={<p>В очереди: {item.valueQueue}</p>} />
                        </Card>
                    </Col>
                )
            })}
        </Row>
    )
}

export default MonitoringPage