import { PieChart } from 'react-minimal-pie-chart'
import PartialCharts from '../Libraries/PartialChrts'

interface SignProps {
  data: Array<PartialCharts>
}

export default (prop: SignProps) => <PieChart
  data={prop.data}
/>