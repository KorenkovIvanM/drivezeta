import { Col, Row, Card, Typography } from 'antd'
import PartialCharts from '../Libraries/PartialChrts'
const { Title } = Typography


interface SignProps {
    data: Array<PartialCharts>
}

const LoreSigreestr = (props: SignProps) => {
    return (
        <div style={{ width: "100%", height: "100%" }}>
            {
                props.data.map((item) => {
                    return <Title style={{ color: item.color }} level={3} key={item.title}>{item.title}: {item.value}</Title>
                })
            }
        </div>)
}

export default LoreSigreestr