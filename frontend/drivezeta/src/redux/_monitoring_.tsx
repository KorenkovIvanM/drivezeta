import Page from '../Libraries/Page'
import DefaultState from '../Libraries/DefaultState'

// Page
import HomePage from "../Pages/HomePage"
import TableTestPage from '../Pages/TableTestPage'
import CreatorProjectPage from '../Pages/CreatorProjectPage'
import WorkerTable from '../Pages/WorkerTable'
import TableTest from '../Pages/TableTest'
import ListWorkerPage from '../Pages/ListWorkerPage'
import MonitoringPage from '../Pages/MonitoringPage'

// Interface
import PartialCharts from '../Libraries/PartialChrts'

const MonitoringProps = {
    data: [
        { title: 'Проверенные', value: 1727, color: '#d4ed31' },
        { title: 'С ошибкой', value: 143, color: '#f59794' },
        { title: 'В очереди', value: 282, color: '#bccefb' },
    ] as Array<PartialCharts>,
    entity: 'Worker' as String,
    mask: { Title: "Проверка подписей Реестров" } as any
}

const setMonitoringData = (data: Array<PartialCharts>) => MonitoringProps.data = data
const setMonitoringEntity = (entity: String) => MonitoringProps.entity = entity
const setMonitoringMask = (mask: any) => MonitoringProps.mask = mask

export {
    setMonitoringData,
    setMonitoringEntity,
    setMonitoringMask,
    MonitoringProps,
}