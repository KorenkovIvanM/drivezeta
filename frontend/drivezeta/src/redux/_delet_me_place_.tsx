import Page from '../Libraries/Page'
import DefaultState from '../Libraries/DefaultState'

// Page
import HomePage from "../Pages/HomePage"
import ListWorkerPage from './../Pages/ListWorkerPage'
import MonitoringPage from '../Pages/MonitoringPage'
import CalendarWorker from '../Pages/CalendarWorker'
import CreateStatemachine from './../Pages/CreateStateMachine'

// Interface
import AnswerTablePage from '../Pages/AnswerPage'

import { MonitoringProps } from './_monitoring_'
import Tree from '../UI/Tree'
import IDE from '../UI/IDE'
import SQLDataPage from '../Pages/SQLDataPage'
import React, { useState } from 'react'
import createMenu from '../Libraries/CreateState/CreateState'
import GroupLinkMenu from '../Libraries/GroupLinkMenu'
import { TableStateMachine } from '../Pages/Tables/TableStateMachine'

function getPage(
    Name: string | null,
    URL: string,
    Page: any,
    Parent: string | null | undefined = undefined,
    Image?: string | null,
    IsShow: boolean = true,
    Access: boolean = true,
): Page {
    return {
        Name,
        URL,
        Image,
        Page,
        IsShow,
        Access,
        Parent
    } as Page
}

const defaultPage: Array<Page> = [
    getPage("Home", "/Home", <HomePage />, "Home"),
    getPage("List Workers", "/ListWprkers", <ListWorkerPage all={false} />, "Process"),
    getPage("List all Workers", "/ListAllWprkers", <ListWorkerPage all={true} />, "Admin"),
    getPage("Calendary", "/CalendarWorker", <CalendarWorker />, "Process"),
    getPage("State Machine", "/CreateStatemachine", <CreateStatemachine />, "Admin"),
    getPage("Answer", "/AnswerTablePage", <AnswerTablePage />, "Admin"),
    getPage("SQL", "/SqldatePage", <SQLDataPage />, "Process"),
    getPage("Tree", "/Tree", <Tree />, "Admin"),
    getPage("IDE", "/Ide", <IDE />, "Admin"),
    getPage("State Machine", "/stateMachine", <TableStateMachine />, "Admin"),
    //------------------------------------------------------------
    getPage("Moitoring", "/monitoringPage", <MonitoringPage data={MonitoringProps.data} entity={MonitoringProps.entity} mask={MonitoringProps.mask} />, "Monitoring", null, false),
]
let PageMenu: Array<GroupLinkMenu> = createMenu(defaultPage)

const defaultState: DefaultState = {
    User: {
        IsActeve: false,
        Id: 0,
    },
    Site: {
        Name: "DRIVEZETA",
    },
    PageContext: defaultPage,
    Pages: PageMenu //defaultPage.filter((page) => page.IsShow).map((page) => getItem(page.Name, page.URL))
}

export default defaultState