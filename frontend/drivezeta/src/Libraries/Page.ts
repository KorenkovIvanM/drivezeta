type Page = 
{
    Name: string | null 
    URL: string
    Image: string | null
    Page: any
    IsShow: boolean
    Access: boolean
    Parent?: string
}

export default Page