class ItemSearch<T> {
    public value: T
    public label: string
    public key: T
    constructor(value: T, label: string)
    {
        this.value = value
        this.label = label
        this.key = value
    }
}

export default ItemSearch