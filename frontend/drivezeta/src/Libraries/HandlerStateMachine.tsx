import { Post, Put } from "../Axios/EntitysCRUD"

class HandlerStateMachine {
    private NameEntity: string = "HandlerStateMachine";

    public id: number
    public stateMachineId: number
    public fileName: string
    public path: string
    public constructor(
        id: number,
        stateMachineId: number,
        fileName: string,
        path: string,
    ) {
        this.id = id
        this.stateMachineId = stateMachineId
        this.fileName = fileName
        this.path = path
    }
    public SaveDb(callback: (data: any) => void): HandlerStateMachine
    {
        if(this.id === 0)
        {
            Post(callback, this.NameEntity, this)
        }
        else
        {
            Put(callback, this.NameEntity, this)
        }
        return this
    }
}

export default HandlerStateMachine