import React from 'react'
import GroupLinkMenu from "../GroupLinkMenu"
import Page from "../Page"
import { MenuProps } from 'antd';

type MenuItem = Required<MenuProps>['items'][number];
let items: MenuItem[] = [];
function getItem(
    label: React.ReactNode,
    key?: React.Key | null,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}

const createMenu = (pages: Array<Page>) => {
    const buff: any = {}
    for (let index = 0, max = pages.length; index < max; index++)
        if (pages[index].Parent != null || pages[index].Parent !== undefined) {
            if (buff[pages[index].Parent as string] === undefined)
                buff[pages[index].Parent as string] = []
            buff[pages[index].Parent as string].push(index)
        }

    const result: Array<GroupLinkMenu> = [] as Array<GroupLinkMenu>
    for (let index in buff)
    {
        let item = {
            key: index,
            //icon: React.createElement(UserOutlined),
            label: index,
            children: [] as Array<any>
        }

        for(let j = 0, max = buff[index].length; j < max; j++)
        {
            let a = pages[buff[index][j]]
            item.children.push(
                getItem(a.Name, a.URL))
        }

        result.push(item as GroupLinkMenu)
    }
    return result
}

export default createMenu