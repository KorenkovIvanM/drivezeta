interface PartialCharts {
    title: string
    value: number
    color: string
}

export default PartialCharts