interface Queue {
    title: string
    valueClose: number
    valueQueue: number
    status: number
    description?: string
}

export default Queue