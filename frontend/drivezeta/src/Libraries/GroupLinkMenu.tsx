import Page from "./Page"

interface GroupLinkMenu
{
    key: string
    //icon: React.FC
    label: string
    children: Array<any>
}

export default GroupLinkMenu