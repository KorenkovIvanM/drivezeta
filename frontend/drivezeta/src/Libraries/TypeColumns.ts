enum TypeColumns {
    String = "text",
    Number = "Number",
    Date = "Date",
    Boolean = "Boolean",
}

export default TypeColumns;