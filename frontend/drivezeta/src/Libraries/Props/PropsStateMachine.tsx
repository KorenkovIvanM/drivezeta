import StateMachine from "../StateMachine"

interface PropsStateMachine
{
    item: StateMachine | null
    setItem: (data: any) => void
}

export default PropsStateMachine