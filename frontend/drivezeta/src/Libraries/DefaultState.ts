import Page from '../Libraries/Page'
import User from '../Libraries/User'
import Site from '../Libraries/Site'
import Project from '../Libraries/Project'
import GroupLinkMenu from './GroupLinkMenu'

type DefaultState = {
    User: User
    Site: Site
    PageContext: Array<Page>
    Pages: Array<GroupLinkMenu>
    Project?: Project | null
}

export default DefaultState;