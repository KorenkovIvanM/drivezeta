import { Post, Put } from "../Axios/EntitysCRUD"
import type TypeStateMachine from "./TypeStateMachine"

class StateMachine {
    private NameEntity: string = "StateMachine";
    //#region Failed
    public id: number
    public title: string
    public description: string | null
    public successId?: number
    public failedId?: number
    public errorId?: number
    public type: TypeStateMachine
    private _refresh: (item: any) => void
    //#endregion
    //#region Constructor
    public constructor(
        title: string,
        type: TypeStateMachine,
        callBack: (item: any) => void,
        description: string | null,
        successId?: number,
        failedId?: number,
        errorId?: number,) {
        this.id = 0
        this.title = title
        this.description = description
        this.successId = successId
        this.failedId = failedId
        this.errorId = errorId
        this.type = type
        this._refresh = callBack

    }
    //#endregion
    public SaveDb(callback: (data: any) => void): StateMachine {
        if (this.id === 0) {
            Post(callback, this.NameEntity, this)
        }
        else {
            Put(callback, this.NameEntity, this)
        }
        return this
    }
}

export default StateMachine