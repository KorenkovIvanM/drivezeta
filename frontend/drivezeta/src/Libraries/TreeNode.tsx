interface TreeNode
{
    name: string
    children: Array<TreeNode>
}

export default TreeNode