interface Argument
{
    id: number
    stateMachineId: number
    arguments: string
}

export default Argument