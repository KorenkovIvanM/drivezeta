interface AddationalFunctions
{
    id: number
    title: string
    url: string
    namePage: string
    StateMachineId: number
}

export default AddationalFunctions