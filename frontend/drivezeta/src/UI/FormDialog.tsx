import { Input } from 'antd'

interface PropsButtonDialog {
    ListPlaceholder: Array<string>
    CallBack: (data: any) => void
}

const FormDialog = (props: PropsButtonDialog) => {
    const result: any = {}

    return (
        <>
            {props.ListPlaceholder.map((item) => <Input
                placeholder={item}
                onInput={e => {
                    // TODO костыль исправь
                    result[item] = (e.target as any).value
                }} />)}
        </>
    )
}

export default FormDialog