import React, { useState } from 'react'
import { Button, Modal, Input } from 'antd'
import Column from "./../Libraries/Column"
import { Post, Put } from '../Axios/EntitysCRUD'
import TypeColumns from '../Libraries/TypeColumns'

type PropsButtonCreateUpdate = {
    columns: Array<Column>
    entity: string
    type: "create" | "update"
    callBack: (result: boolean) => void
    mask?: any
    example?: any // TODO исправь
}

const ButtonCreateUpdate = (props: PropsButtonCreateUpdate) => {
    const data: any = {}
    const [open, setOpen] = useState(false)
    const [confirmLoading, setConfirmLoading] = useState(false)

    const showModal = () => {
        setOpen(true)
    };

    const handleOk = () => {
        if (props.type === "create")
        {
            if(props.example != null)
                data["ParentId"] = props.example.id
            Post(props.callBack, props.entity, {...props.mask, ...data})
        }
        else
            Put(props.callBack, props.entity, { ...data, id: props?.example.id })
        setOpen(false)
    };

    const handleCancel = () => {
        setOpen(false)
    };

    return (
        <>
            <Button type="primary" onClick={showModal}>
                {props.type}
            </Button>
            <Modal
                title={`${props.type} ${props.entity}`}
                open={open}
                onOk={handleOk}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                {props.columns.map((item) => {
                    if (item.isUpdate)
                        return <Input
                            style={{ marginTop: 10 }}
                            onChange={(e) => data[item.title] = item.type === TypeColumns.Number? Number(e.target.value) : e.target.value }
                            type={item.type}
                            placeholder={item.title} />
                }
                )}
            </Modal>
        </>
    );
};

export default ButtonCreateUpdate