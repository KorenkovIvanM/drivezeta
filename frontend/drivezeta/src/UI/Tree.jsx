import { useEffect, useState } from 'react'
import { AnimatedTree } from 'react-tree-graph'
import 'react-tree-graph/dist/style.css'
import { Get } from '../Axios/EntitysCRUD'
// import TreeNode from '../Libraries/TreeNode'
let _data = {}
const decor = (item) => {
    item.name = item.title
    if(item.success) decor(item.success)
    if(item.failed) decor(item.failed)
    if(item.error) decor(item.error)
    if(item.success || item.failed || item.error)
    {
        item.children = []
        if(item.success) item.children.push(item.success)
        if(item.failed) item.children.push(item.failed)
        if(item.error) item.children.push(item.error)
    }
    item = {name: item.title, children: item.children, key: item.id}
    _data = {name: item.title, children: item.children, key: item.id}
}

export default () => {
    const [data, setData] = useState({})
    useEffect(() => {
        Get((tdata) => {
            decor(tdata[0])
            setData(_data)
        }, 'StateMachineTree', {})
    }, [])
    return (
        <AnimatedTree
            data={data}
            height={850}
            width={1500} />
    )
}