import React, { useEffect, useState } from "react"
import { Calendar, momentLocalizer } from "react-big-calendar"
import moment from "moment"
import 'moment/locale/ru'
import "react-big-calendar/lib/css/react-big-calendar.css"

moment.locale("ru");
const localizer = momentLocalizer(moment);

// function formatDate(dat) {
//     return `${dat.getFullYear()}.${dat.getMonth() + 1}.${dat.getDate()}`;
// }


export default function ReactBigCalendar() {

    // const calendarStyle = (date) => {
    //     let currentDate = `${new Date().getDate()} ${new Date().getMonth() + 1} ${new Date().getFullYear()}`
    //     let allDate = `${date.getDate()} ${date.getMonth() + 1} ${date.getFullYear()}`

    //     if (allDate === currentDate)
    //         return {
    //             style: {
    //                 backgroundColor: '#90caf9',
    //             }
    //         }
    //     else if (date.getMonth() !== new Date().getMonth())
    //         return {
    //             style: {
    //                 backgroundColor: '#121212',
    //             }
    //         }
    //     else
    //         return {
    //             style: {
    //                 backgroundColor: '#262b32',
    //             }
    //         }
    // }

    // const eventStyleGetter = function (event, start, end, isSelected) {
    //     // event
    //     var backgroundColor = colorProject[event.projectId].color || "#9e9e9e";//proect.color;

    //     var style = {
    //         backgroundColor: backgroundColor,
    //         borderRadius: '1px',
    //         border: '0px',
    //         display: 'block'
    //     };
    //     return {
    //         style: style
    //     };
    // }

    const handleSelect = () => {
        // const title = window.prompt("Name task");
        // PostTaskPostfix(getOrders, profile.id, 0, `&Title=${title}&Start=${formatDate(start)}&End=${formatDate(end)}`, 0);
    };
    return (
        <Calendar
            views={["day", "agenda", "work_week", "month"]}
            selectable
            localizer={localizer}
            defaultDate={new Date()}
            defaultView="month"
            // dayPropGetter={calendarStyle}
            // events={eventsData.filter(e => e.projectId).filter(e => colorProject[e.color ? e.projectId : 0].isShow)}
            style={{ height: "calc(100vh - 140px)", stopColor: "#f00" }}
            //onSelectEvent={(event) => alert(event.title)}
            onSelectSlot={handleSelect}
        />
    )
}
