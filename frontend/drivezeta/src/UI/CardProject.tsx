import React from "react"
import { Card, Avatar, Row, Col } from 'antd'
import { SmileOutlined } from '@ant-design/icons'
const { Meta } = Card;

const CardProject: React.FC = () => {
    return (
        <Card
            style={{ width: 300 }}
            cover={
                <img
                    alt="example"
                    src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                />
            }
            actions={[
                <SmileOutlined type="setting" key="setting" />,
                <SmileOutlined type="edit" key="edit" />,
                <SmileOutlined type="ellipsis" key="ellipsis" />,
            ]}
        >
            <Meta
                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title="Card title"
                description="This is the description"
            />
        </Card>
    )
}

export default CardProject;