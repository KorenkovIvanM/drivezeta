import React, { useEffect, useState } from 'react'
import { Table, Button, Space } from 'antd'
import type { ColumnsType, TableProps } from 'antd/es/table'
import type { TableRowSelection } from 'antd/es/table/interface'
import { Get, Delete, Post } from "./../Axios/EntitysCRUD"
import ButtonModal from "./_buttonCreateUpdate"
import Column from "./../Libraries/Column"

interface FullTableProps {
    Columns: Array<Column>
    Entity: string
    Mask?: any
    Height?: string | number
}

const FullTable = (props: FullTableProps) => {
    const columns: ColumnsType<any> = props.Columns.map((item) => { return ({ ...item, sorter: item?.sorter }) })
    if (columns[columns.length - 1].key !== 'createChaldren')
        columns.push({
            title: 'Update', dataIndex: '', key: 'u', width: 40,
            render: (item) => <ButtonModal
                callBack={(result: boolean) => refresh()}
                columns={props.Columns}
                entity={props.Entity}
                example={item}
                type={"update"} />,
        });

    if (columns[columns.length - 1].key !== 'createChaldren')
        columns.push({
            title: 'Create', dataIndex: '', key: 'createChaldren', width: 40,
            render: (item) => <ButtonModal
                callBack={(result: boolean) => refresh()}
                columns={props.Columns}
                entity={props.Entity}
                example={item}
                type={"create"} />,
        });

    const rowSelection: TableRowSelection<any> = {
        onChange: (selectedRowKeys) => { setRowsForDelete(selectedRowKeys); },
        onSelect: (record, selected, selectedRows) => { setRowsForDelete(selectedRows.map(item => item.key as React.Key)) },
        onSelectAll: (selected, selectedRows, changeRows) => { setRowsForDelete(changeRows.map(item => item.key as React.Key)) },
    };

    const [rowsForDelete, setRowsForDelete] = useState<React.Key[]>([])
    const [data, setData] = useState<Array<any> | null>(null)
    let refresh = () => Get(setData, props.Entity, props.Mask || {})
    useEffect(() => { if (data == null) refresh() }, [])

    return (
        <>
            <Space style={{ paddingBottom: 10 }}>
                <Button onClick={() => {
                    for (let index = 0; index < rowsForDelete.length; index++)
                        Delete((result: boolean) => console.log(result), props.Entity, rowsForDelete[index] as number)
                    refresh()
                }} danger>Delete</Button>
                <ButtonModal
                    callBack={(result: boolean) => refresh()}
                    columns={props.Columns}
                    entity={props.Entity}
                    mask={props.Mask}
                    type={"create"} />
            </Space>
            <Table
                style={{ height: props.Height }}
                columns={columns}
                bordered
                rowSelection={{ ...rowSelection, checkStrictly: false }}
                dataSource={data || []} />
        </>
    )
}

export default FullTable;