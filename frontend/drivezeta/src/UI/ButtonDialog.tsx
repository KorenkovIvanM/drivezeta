import React, { useState } from 'react'
import { Button, Modal, Input } from 'antd'

interface PropsButtonDialog {
    ListPlaceholder: Array<string>
    CallBack: (data: any) => void
}

const ButtonDialog = (props: PropsButtonDialog) => {
    const result: any = {};
    const [open, setOpen] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [modalText, setModalText] = useState('Content of the modal');

    const showModal = () => setOpen(true)

    const handleOk = () => {
        props.CallBack(result);
        setOpen(false);
    }

    const handleCancel = () => setOpen(false);

    return (
        <>
            <Button style={{ color: "#d4ed31" }} onClick={showModal}>
                Start
            </Button>
            <Modal
                title="Title"
                open={open}
                onOk={handleOk}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                {props.ListPlaceholder.map((item) => <Input
                    placeholder={item}
                    onInput={e => {
                        // TODO костыль исправь
                        result[item] = (e.target as any).value
                    }} />)}
            </Modal>
        </>
    )
}

export default ButtonDialog