import * as React from 'react';
import { useSelector } from 'react-redux';
import { Routes, Route, Link } from "react-router-dom";

export default function PersistentDrawerLeft() {
    const Pages = useSelector(state => state.Pages);
    const PagesTimeManagment = useSelector(state => state.PagesTimeManagment);

    return (
        <>
            <Routes>{Pages.map(item => <Route key={item.url} path={item.url} element={item.page} />)}</Routes>
            <Routes>{PagesTimeManagment.map(item => <Route key={item.url} path={item.url} element={item.page} />)}</Routes>
        </>
    );
}
