import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import { Menu, Button } from 'antd';
import { useSelector } from 'react-redux';
import { Routes, Route, Link } from "react-router-dom";
import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';

function getItem(label, key, icon, children, type) {
    return {
        key,
        icon,
        children,
        label,
        type,
    };
}

const App = () => {

    const Pages = useSelector(state => state.Pages);
    const PagesTimeManagment = useSelector(state => state.PagesTimeManagment);

    const items = [
        getItem('Main', 'sub1', <AppstoreOutlined />, [
            ...Pages
                .filter((e) => {
                    return e.isShow
                })
                .map((page) => getItem(page.name, page.url))
        ]),
        getItem('Time Management', 'sub2', <AppstoreOutlined />, [
            ...PagesTimeManagment.map((page) => getItem(page.name, '5'))
                .filter((e) => {
                    return e.isShow
                })
                .map((page) => getItem(page.name, page.id))
        ]),
    ];
    const navigate = useNavigate();
    const onClick = (e) => {
        navigate(e.key, { replace: true })
    };
    return (
        <Menu
            onClick={onClick}
            style={{
                width: "100%",
                height: "100%"
            }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            items={items}
        />
    );
};
export default App;