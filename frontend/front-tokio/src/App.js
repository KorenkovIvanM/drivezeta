import React from 'react';
import { Col, Row } from 'antd';
import LeftMenu from "./Components/LeftMenu"
import Context from "./Components/Context"
import Header from './UI/Header';

const App = () => (
    <Row>
      <Col span={24} style={{height: "5vh"}}><Header /></Col>
      <Col span={4} style={{height: "95vh"}}><LeftMenu /></Col>
      <Col span={20} style={{height: "95vh"}}><Context /></Col>
    </Row>
);
export default App;