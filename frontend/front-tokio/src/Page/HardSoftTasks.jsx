import React from 'react';
import { Col, Row } from 'antd';
import TableSoftTasks from '../Table/TableSoftTasks';
import TableHardTasks from '../Table/TableHardTasks';

const HardSoftTasks = () => (
    <Row>
        <Col span={12} style={{ height: "100%" }}>
            <TableSoftTasks />
        </Col>
        <Col span={12} style={{ height: "100%" }}>
            <TableHardTasks />
        </Col>
    </Row>
);
export default HardSoftTasks;