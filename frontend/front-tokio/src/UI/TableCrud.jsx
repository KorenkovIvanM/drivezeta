import { SearchOutlined } from '@ant-design/icons';
import { Button, Input, Space, Table, Form, InputNumber } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import Highlighter from 'react-highlight-words';
import { Delete, Get, Put } from '../Ajax/Entity';
import ButtonAjax from './ButtonAjax';
import ButtonDialogAnt from "./ButtonDialogAnt";
import Counter from './Counter';
// import CheckOutlined from ""



const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

const App = (props) => {
  let columnsName = {};
  for (let i = 0, max = props.columns.length; i < max; i++)
    columnsName[props.columns[i].title] = "";
  const entity = props.entity;
  const [selectedRows, setSelectedRows] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);


  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
      setSelectedRows(selectedRows);
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      setSelectedRows(selectedRows);
      console.log(selected, selectedRows, changeRows);
    },
  };


  const [data, setData] = useState([]);
  const refresh = () => {
    if (props.get)
      props.get((data) => setData(data), entity, props.mask, 0, 500)
    else
      Get((data) => setData(data), entity, props.mask, 0, 500);
  };
  useEffect(() => {
    refresh();
  }, []);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({
                closeDropdown: false,
              });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1890ff' : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });
  const [checkStrictly, setCheckStrictly] = useState(false);

  return <Table
    style={{ width: "100%" }}
    components={{
      body: {
        cell: EditableCell,
      },
    }}
    columns={[
      ...props.columns
        .map((item) => {
          if (item.type === 'counter')
            return {
              title: item.title,
              key: item.key,
              render: (row) => (
                <Counter
                  value={row[item.dataIndex]}
                  onPlus={() => {
                    let buff = {};
                    buff.Title = row.title;
                    buff.Id = row.id || 0;
                    buff[item.dataIndex] = (row[item.dataIndex] || 0) - 10;
                    Put(refresh, props.entity, buff)
                  }}
                  onMinus={() => {
                    let buff = {};
                    buff.Title = row.title;
                    buff.Id = row.id || 0;
                    buff[item.dataIndex] = (row[item.dataIndex] || 0) + 10;
                    Put(refresh, props.entity, buff)
                  }}
                />
              ),
            }
          else if (item.type === "ba")
            return {
              title: item.title,
              key: item.key,
              render: (row) => (
                <ButtonAjax
                  name={"+"}
                  event={() => {
                    let buff = {};
                    buff.Title = row.title;
                    buff.Id = row.id || 0;
                    buff[item.target || item.dataIndex] = item.value;
                    Put(refresh, props.entity, buff)
                  }} />
              ),
            }
          else
            return { ...item }
        }),
      {
        title: 'Status',
        key: 'state',
        render: (row) => (
          <ButtonDialogAnt
            entity={entity}
            callback={refresh}
            item={row}
            isEdit={true}
            columns={props.columns}
            post={props.post} />
        ),
      },
      {
        title: 'Create',
        key: 'create',
        render: (row) => (
          <ButtonDialogAnt
            entity={entity}
            callback={refresh}
            item={row}
            isEdit={false}
            columns={props.columns}
            post={props.post}
            mask={{ ...props.mask, parentId: row.id }} />
        ),
      },]}
    rowSelection={{
      ...rowSelection,
      checkStrictly,
    }}
    bordered
    dataSource={data}
    title={() => {
      return (
        <>
          <ButtonDialogAnt
            entity={entity}
            callback={refresh}
            columns={props.columns}
            post={props.post}
            mask={props.mask} />
          <Button danger style={{ marginLeft: 10 }} onClick={() => {
            for (let i = 0, max = selectedRows.length; i < max - 1; i++)
              Delete(() => console.log(`true deleted`), entity, selectedRows[i].id)
            Delete(refresh, entity, selectedRows[selectedRows.length - 1].id)
          }}>Delete</Button>
        </>
      )
    }} />;
};
export default App;