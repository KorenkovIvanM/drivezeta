import React from 'react';
import { Button } from 'antd';
const Counter = (props) => {

    return (
        <Button.Group>
            <Button onClick={props.onPlus}>-</Button>
            <Button>{props.value}</Button>
            <Button onClick={props.onMinus}>+</Button>
        </Button.Group>
    );
};
export default Counter;