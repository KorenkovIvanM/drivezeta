import { Button, Modal, Input, DatePicker } from 'antd';
import React, { useState } from 'react';
import { Post, Put } from '../Ajax/Entity';
const App = (props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    let columnsName = {};
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        if (props.isEdit) {
            Put(props.callback, props.entity, { ...props.mask,...columnsName, Id: props.item.id })
        }
        else {
            Post(props.callback, props.entity, { ...props.mask, ...columnsName })
        }
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };
    let listInput = [];
    let index = 0;

    for (let i = 0, max = props.columns.length; i < max; i++) {
        if (props.columns[i].isShow) {
            if (props.columns[i].type == "str")
                listInput[index] = <Input
                    key={index}
                    placeholder={props.columns[i].title}
                    onChange={
                        (e) => {
                            columnsName[props.columns[i].title] = e.target.value;
                        }
                    }
                    style={{ marginTop: 10 }} />
            else if (props.columns[i].type == "date")
                listInput[index] = <DatePicker 
                key={index} 
                placeholder={props.columns[i].title}
                onChange={
                    (date, dateString) => {
                        columnsName[props.columns[i].title] = dateString;
                    }
                }
                style={{ marginTop: 10 }} />
            index += 1;
        }

    }

    return (
        <>
            <Button type="primary" onClick={showModal} ghost>
                {props.isEdit ? "Edit" : "Create"}
            </Button>
            <Modal title={props.isEdit ? "Edit" : "Create"} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                {listInput}
            </Modal>
        </>
    );
};
export default App;