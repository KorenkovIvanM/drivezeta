import React from "react";

// Libraries
import Page from "../Libraries/Page";

// icon 
import Home from '@mui/icons-material/Home';
import AccessTime from "@mui/icons-material/AccessTime";
import SettingsIcon from '@mui/icons-material/Settings';
import AvTimer from '@mui/icons-material/AvTimer';
import DateRange from '@mui/icons-material/DateRange';
import Lightbulb from '@mui/icons-material/Lightbulb';
import AccountTree from '@mui/icons-material/AccountTree';
import Apps from '@mui/icons-material/Apps';
import CalendarToday from '@mui/icons-material/CalendarToday';

// Page
import TableIdea from "../Table/TableIdea"; 
import TableSoftTasks from "../Table/TableSoftTasks";
import TableHardTasks from "../Table/TableHardTasks";
import HardSoftTasks from "../Page/HardSoftTasks";
import RepeatTasks from "../Table/RepeatTasks";
import TableWeek from "../Table/TableWeek";
import TableMonth from "../Table/TableMonth";

const defaultState = {
    profile: {
        isActev: false,
        id: 0,
    },
    Site: {
        Name: "DRIVEZETA",
    },
    Pages: [
        new Page("", "/", <Home />, <div></div>, false, true),
        new Page("Home", "/home", <Home />, <div></div>, true, true),
        new Page("Idea table", "/settings", <SettingsIcon />, <TableIdea />, true, false),
        new Page("Soft tasks", "/softTasks", <Apps />, <TableSoftTasks />, true, false),
        new Page("Hard tasks", "/hardTasks", <Apps />, <TableHardTasks />, true, false),
        new Page("Hard&Soft tasks", "/hardsofttasks", <AccountTree />, <HardSoftTasks />, true, false),
        new Page("Repaet tasks", "/repeattasks", <AccountTree />, <RepeatTasks />, true, false),
        new Page("Week", "/week", <AccountTree />, <TableWeek />, true, false),
        new Page("month", "/month", <AccountTree />, <TableMonth />, true, false),
        // new Page("Page Project", "/pageProject", <Apps />, <PageProject />, false, false),
    ],
    PagesTimeManagment: [
        new Page("Setings1", "/settings1", <SettingsIcon />, <TableIdea />, true, false),
        new Page("Setings2", "/settings2", <SettingsIcon />, <TableIdea />, true, false),
        // new Page("Daily schedule", "/dailySchedule", <AccessTime />, <DailySchedule />, true, false),
        // new Page("Cronos", "/cronos", <AvTimer />, <Coros />, true, false),
        // new Page("Month Week Day", "/mwd", <DateRange />, <MWD />, true, false),
        // new Page("Idea", "/idea", <Lightbulb />, <Idea />, true, false),
        // new Page("Hard&Soft tasks", "/hardsofttasks", <AccountTree />, <HardSoftTasks />, true, false),
        // new Page("Calendar", "/calendar", <CalendarToday />, <Calendar />, true, false),
    ],
    Project: {

    },
    Group: [
        
    ],
}

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        // case "SET_USER_ID":
        //     return { ...state, profile: { ...state.profile, id: action.id } };
        // case "ACTIV_USER":
        //     Autorisation(action.callback, action.profile.name, action.profile.email);
        //     return { ...state, profile: { ...action.profile, isActev: true } };
        // case "NON_ACTIV_USER":
        //     return { ...state, profile: { isActev: false } };
        // case "SET_PROJECT":
        //     return { ...state, Project: { ...action.project } };
        case "SET_GROUP":
            return { ...state, Group: { ...action.group } };
        default:
            return { ...state };
    }

}

export default reducer;
