import TableCrud from "./../UI/TableCrud";

export default () => {
  return (
    <TableCrud
    entity={"Order"}
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 0
      }}
      columns={
        [
          {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: '5%',
            isShow: true,
            type: "ba",
            value: 2,
          },
          {
            title: 'Discription',
            dataIndex: 'discription',
            key: 'discription',
            width: '30%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Start',
            dataIndex: 'start',
            key: 'start',
            isShow: true,
            type: "date",
          },
          {
            title: 'Stop',
            dataIndex: 'stop',
            key: 'stop',
            isShow: true,
            type: "date",
          },
        ]
      } />
  );
}