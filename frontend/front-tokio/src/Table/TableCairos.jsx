import TableCrud from "./../UI/TableCrud";

export default () => {
  return (
    <TableCrud
    entity={"Keiras"}
      pageSize={5}
      columns={
        [
          {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            isShow: true,
          },
          {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            width: '30%',
            isShow: false,
          },
          {
            title: 'UserId',
            dataIndex: 'userId',
            key: 'userId',
            isShow: false,
          },
        ]
      } />
  );
}