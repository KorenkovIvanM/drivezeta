import TableCrud from "./../UI/TableCrud";

export default () => {
  return (
    <TableCrud
    entity={"Idea"}
      pageSize={5}
      columns={
        [
          {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Discription',
            dataIndex: 'discription',
            key: 'discription',
            width: '30%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Create',
            dataIndex: 'create',
            key: 'create',
            isShow: false,
          },
        ]
      } />
  );
}