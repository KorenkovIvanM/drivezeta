import TableCrud from "./../UI/TableCrud";

export default () => {
  return (
    <TableCrud
    entity={"Order"}
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 4
      }}
      columns={
        [
          {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            isShow: true,
            type: "str",
          },
          {
            title: 'status',
            dataIndex: 'status',
            key: 'status',
            width: '5%',
            isShow: true,
            type: "ba",
            value: 5,
            target: "Type",
          },
          {
            title: 'Discription',
            dataIndex: 'discription',
            key: 'discription',
            width: '30%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Progress',
            dataIndex: 'progress',
            key: 'progress',
            isShow: false,
            type: "counter",
          },
        ]
      } />
  );
}