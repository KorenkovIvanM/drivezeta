import TableCrud from "./../UI/TableCrud";

export default () => {
  return (
    <TableCrud
    entity={"Order"}
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 1
      }}
      columns={
        [
          {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            width: '20%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: '5%',
            isShow: true,
            type: "ba",
            value: 2,
          },
          {
            title: 'Discription',
            dataIndex: 'discription',
            key: 'discription',
            width: '30%',
            isShow: true,
            type: "str",
          },
          {
            title: 'Progress',
            dataIndex: 'progress',
            key: 'progress',
            isShow: false,
            type: "counter",
          },
        ]
      } />
  );
}