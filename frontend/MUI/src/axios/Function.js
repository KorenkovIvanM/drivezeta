import axios from "axios";

//#region POST
const FunctionPostTasks = (callBack, Title, Discription, User, typeOrders, Priority, Start, End) => {
    console.log(`https://localhost:7135/Order?Title=${Title}${Discription ? `Discription=${Discription}` : ""}&User=${User}&typeOrders=${typeOrders}${Priority ? "&Prioretis=" + Priority : ""}${Start ? "&Start=" + Start : ""}${End ? "&End=" + End : ""}`);
    axios.post(`https://localhost:7135/Order?Title=${Title}${Discription ? `Discription=${Discription}` : ""}&User=${User}&typeOrders=${typeOrders}${Priority ? "&Prioretis=" + Priority : ""}${Start ? "&Start=" + Start : ""}${End ? "&End=" + End : ""}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
// TODO delete me
// const FunctionDownTask = (callBack, Id) => {
//     axios.post(`https://localhost:7135/ToolsDailySchedule?Id=${Id}`)
//         .then((respomce) => {
//             if (respomce.data)
//                 callBack();
//         })
//         .catch((errors) => {
//             console.log(errors);
//         });
// };

//#endregion
//#region GET
const FunctionGetTasks = (callBack) => {
    axios.get(`https://localhost:7135/Order`)
        .then((respomce) => {
            callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const FunctionGetTasksType = (user, type, callBack, project) => {
    console.log(project);
    console.log(`https://localhost:7135/Tasks?UserId=${user}&Type=${type}${project ? `&ProjectId=${project}` : ""}&timeLine=1`)
    axios.get(`https://localhost:7135/Tasks?UserId=${user}&Type=${type}${project ? `&ProjectId=${project}` : ""}&timeLine=1&Status=1`)
        .then((respomce) => {
            callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};
//#endregion
//#region PUT
const FunctionPutTasks = (callBack, id, title, discription, user, status, prioretis, time, end) => {
    console.log(`https://localhost:7135/Order?Id=${id}${discription ? "&Discription=" + discription : ""}${title ? "&Title=" + title : ""}${status ? "&Status=" + status : ""}${prioretis ? "&Prioretis=" + prioretis : ""}${time ? "&Start=" + time : ""}${end ? "&End=" + end : ""}`);
    axios.put(`https://localhost:7135/Order?Id=${id}${discription ? "&Discription=" + discription : ""}${title ? "&Title=" + title : ""}${status ? "&Status=" + status : ""}${prioretis ? "&Prioretis=" + prioretis : ""}${time ? "&Start=" + time : ""}${end ? "&End=" + end : ""}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
const FunctionPutTasksStatus = (callBack, id, status) => {
    axios.put(`https://localhost:7135/Order?Id=${id}&status=${status}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
const FunctionPutTasksProgress = (callBack, id, value) => {
    axios.put(`https://localhost:7135/Order?Id=${id}&Progress=${value}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
const FunctionPutTasksPrioritet = (callBack, id, value) => {
    axios.put(`https://localhost:7135/Order?Id=${id}&Prioretis=${value}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
//#endregion
//#region DELETE
const FunctionDeketeTasks = (callBack, id) => {
    axios.delete(`https://localhost:7135/Order?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};
//#endregion
export {
    FunctionGetTasks,
    FunctionGetTasksType,
    FunctionDeketeTasks,
    FunctionPostTasks,
    FunctionPutTasksStatus,
    FunctionPutTasksProgress,
    FunctionPutTasks,
    // FunctionDownTask,
    FunctionPutTasksPrioritet,
};