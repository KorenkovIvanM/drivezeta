import axios from "axios";

const GetProject = (callBack, Id) => {
    axios.get(`https://localhost:7135/Project?UserId=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostProject = (callBack, id, Name, Discription, End, Icon) => {
    axios.post(`https://localhost:7135/Project?UserId=${id}&Name=${Name}${Discription && `&Discription=${Discription}`}${End && `&End=${End}`}${Icon && `&Icon=${Icon}`}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteProject = (callBack, id) => {
    axios.delete(`https://localhost:7135/Project?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateProject = (callBack, id, Name, Discription, End, Icon, Color) => {
    console.log(`https://localhost:7135/Project?Id=${id}${Name && `&Name=${Name}`}${Discription && `&Discription=${Discription}`}${End && `&End=${End}`}${Icon && `&Icon=${Icon}`}${Color && `&Color=${Color}`}`);
    axios.put(`https://localhost:7135/Project?Id=${id}${Name && `&Name=${Name}`}${Discription && `&Discription=${Discription}`}${End && `&End=${End}`}${Icon && `&Icon=${Icon}`}${Color && `&Color=%23${Color}`}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetProject,
    DeleteProject,
    PostProject,
    UpdateProject,
};