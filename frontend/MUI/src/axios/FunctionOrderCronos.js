import axios from "axios";

const GetOrderCronos = (callBack, Id, cronos) => {
    axios.get(`https://localhost:7135/OrderCronos?UserId=${Id}&CronosId=${cronos}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostOrderCronosPostfix = (callBack, id, cronos, postfix) => {
    axios.post(`https://localhost:7135/OrderCronos?UserId=${id}&CronosId=${cronos}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteOrderCronos = (callBack, id) => {
    axios.delete(`https://localhost:7135/OrderCronos?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateOrderCronosPostfix = (callBack, id, postfix, isDown = false) => {
    axios.put(`https://localhost:7135/OrderCronos?Id=${id}${postfix}&isDown=${isDown}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetOrderCronos,
    DeleteOrderCronos,
    PostOrderCronosPostfix,
    UpdateOrderCronosPostfix,
};