import axios from "axios";

const GetIdea = (callBack, Id) => {
    axios.get(`https://localhost:7135/Idea?UserId=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostIdeaPostfix = (callBack, id, postfix) => {
    axios.post(`https://localhost:7135/Idea?UserId=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteIdea = (callBack, id) => {
    axios.delete(`https://localhost:7135/Idea?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateIdeaPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Idea?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetIdea,
    DeleteIdea,
    PostIdeaPostfix,
    UpdateIdeaPostfix,
};