import axios from "axios";

const GetGroupUser = (callBack) => {
    axios.get(`https://localhost:7135/GroupUsers`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetGroupUser
};