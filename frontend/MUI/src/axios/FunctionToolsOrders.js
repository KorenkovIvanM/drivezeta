import axios from "axios";

const FunctionDownTask = (callBack, Id) => {
    axios.post(`https://localhost:7135/ToolsDailySchedule?Id=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const FunctionSortOrders = (callBack, Id) => {
    axios.put(`https://localhost:7135/ToolsDailySchedule?Id=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack();
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    FunctionDownTask,
    FunctionSortOrders,
};