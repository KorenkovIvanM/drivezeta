import axios from "axios";

//#region POST
const Autorisation = (callBack, Name, Email) => {
    axios.post(`https://localhost:7135/User?Name=${Name}&Email=${Email}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    Autorisation
};