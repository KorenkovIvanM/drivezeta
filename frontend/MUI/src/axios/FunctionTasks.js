import axios from "axios";

const GetTask = (callBack, UserId, ProjectId, Status = 1) => {
    console.log("_-------------------------------------------------------------------")
    console.log(`https://localhost:7135/Tasks?UserId=${UserId}${ProjectId ? `&ProjectId=${ProjectId}` : ""}&Status=${Status}`)
    axios.get(`https://localhost:7135/Tasks?UserId=${UserId}${ProjectId ? `&ProjectId=${ProjectId}` : ""}&Status=${Status}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostTaskPostfix = (callBack, UserId, ProjectId, postfix, typeOrders = 1) => {
    console.log(`https://localhost:7135/Order?User=${UserId}&typeOrders=${typeOrders}&ProjectId=${ProjectId}${postfix}`);
    axios.post(`https://localhost:7135/Order?User=${UserId}&typeOrders=${typeOrders}&ProjectId=${ProjectId}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteTask = (callBack, id) => {
    axios.delete(`https://localhost:7135/Order?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateTaskPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Order?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetTask,
    DeleteTask,
    PostTaskPostfix,
    UpdateTaskPostfix,
};