import axios from "axios";

const GetTemplates = (callBack, Id) => {
    axios.get(`https://localhost:7135/Templates?UserId=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostTemplates = (callBack, title, id) => {
    axios.post(`https://localhost:7135/Templates?Title=${title}&UserId=${id}&Actev=false&monday=false&tuesday=false&wednesday=false&thursday=false&friday=false&saturday=false&Sunday=false`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostTemplatesPostfix = (callBack, id, postfix) => {
    axios.post(`https://localhost:7135/Templates?UserId=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteTemplates = (callBack, id) => {
    axios.delete(`https://localhost:7135/Templates?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateTemplates = (callBack, id, title, actev) => {
    axios.put(`https://localhost:7135/Templates?Id=${id}&Title=${title}&Actev=${actev}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateTemplatesPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Templates?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetTemplates,
    PostTemplates,
    DeleteTemplates,
    UpdateTemplates,
    PostTemplatesPostfix,
    UpdateTemplatesPostfix,
};