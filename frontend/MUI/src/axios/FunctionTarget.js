import axios from "axios";

const GetTarget = (callBack, UserId, ProjectId) => {
    axios.get(`https://localhost:7135/Target?${UserId ? `UserId=${UserId}` : `ProjectId=${ProjectId}`}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostTargetPostfix = (callBack, UserId, ProjectId, postfix) => {
    axios.post(`https://localhost:7135/Target?UserId=${UserId}&ProjectId=${ProjectId}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteTarget = (callBack, id) => {
    axios.delete(`https://localhost:7135/Target?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateTargetPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Target?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetTarget,
    DeleteTarget,
    PostTargetPostfix,
    UpdateTargetPostfix,
};