import axios from "axios";

const GetCronos = (callBack, Id) => {
    axios.get(`https://localhost:7135/Cronos?UserId=${Id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostCronosPostfix = (callBack, id, postfix) => {
    axios.post(`https://localhost:7135/Cronos?UserId=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteCronos = (callBack, id) => {
    axios.delete(`https://localhost:7135/Cronos?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateCronosPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Cronos?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetCronos,
    DeleteCronos,
    PostCronosPostfix,
    UpdateCronosPostfix,
};