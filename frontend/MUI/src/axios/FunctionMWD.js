import axios from "axios";

const GetMWD = (callBack, Id, isMonth) => {
    axios.get(`https://localhost:7135/MWD?UserId=${Id}&isMonth=${isMonth}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const PostMWDPostfix = (callBack, id, postfix, isMonth) => {
    axios.post(`https://localhost:7135/MWD?UserId=${id}&isMonth=${isMonth}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const DeleteMWD = (callBack, id) => {
    axios.delete(`https://localhost:7135/MWD?Id=${id}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateMWDPostfix = (callBack, id, postfix, isDown = false) => {
    axios.put(`https://localhost:7135/MWD?Id=${id}${postfix}&isMonth=${isDown}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    GetMWD,
    DeleteMWD,
    PostMWDPostfix,
    UpdateMWDPostfix,
};