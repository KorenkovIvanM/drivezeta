class Page {
    constructor(name, url, image, page, isShow, access) {
        this.name = name;
        this.url = url;
        this.image = image;
        this.page = page;
        this.isShow = isShow;
        this.access = access;
    }
}

export default Page;