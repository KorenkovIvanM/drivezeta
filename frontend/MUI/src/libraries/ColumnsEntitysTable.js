class ColumnsEntitysTable {
    constructor(field, headerName, width, type, isUpdate) {
        this.field = field;
        this.headerName = headerName;
        this.width = width;
        this.type = type;
        this.isUpdate = isUpdate;
    }
}

export default ColumnsEntitysTable;