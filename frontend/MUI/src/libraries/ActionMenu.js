class ActionMenu {
    constructor(name, image, action) {
        this.name = name;
        this.image = image;
        this.action = action;
    }
}

export default ActionMenu;