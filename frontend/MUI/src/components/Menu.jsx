import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useSelector } from 'react-redux';
import { Routes, Route, Link } from "react-router-dom";
import RightPanel from "./ui/RightPanel";
import "./style.css";
import GoogleButton from "./ui/GoogleButton";
import MenuIconForSite from "./ui/MenuIcon";
import Collapse from '@mui/material/Collapse';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';


const drawerWidth = 320;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function PersistentDrawerLeft() {
    let profile = useSelector(state => state.profile);
    const Site = useSelector(state => state.Site);
    const Pages = useSelector(state => state.Pages);
    const PagesTimeManagment = useSelector(state => state.PagesTimeManagment);

    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [openTimeManagment, setOpenTimeManagment] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed" open={open}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
                        {Site.Name}
                    </Typography>
                    <Routes>
                        <Route path="/dailySchedule" element={<RightPanel />} />
                        <Route path="/hardsofttasks" element={<RightPanel />} />
                    </Routes>
                    <MenuIconForSite />
                    <GoogleButton />
                </Toolbar>
            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />

                <List
                    component="nav"
                    aria-labelledby="nested-list-subheader"
                >
                    {Pages.filter((e) => {
                        return e.isShow && (profile.isActev || e.access)
                    }).map(page => (
                        <ListItem key={page.url} disablePadding>
                            <Link to={page.url} style={{ width: "100%" }}>
                                <ListItemButton>
                                    <ListItemIcon>
                                        {page.image}
                                    </ListItemIcon>
                                    <ListItemText primary={page.name} />
                                </ListItemButton>
                            </Link>
                        </ListItem>
                    ))}

                    <ListItemButton onClick={() => { setOpenTimeManagment(!openTimeManagment) }}>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="TimeMenegmant" />
                        {openTimeManagment ? <ExpandLess /> : <ExpandMore />}
                    </ListItemButton>
                    <Collapse in={openTimeManagment} timeout="auto" unmountOnExit>
                        <List>
                            {PagesTimeManagment.filter((e) => {
                                return e.isShow && (profile.isActev || e.access)
                            }).map(page => (
                                <ListItem key={page.url} disablePadding>
                                    <Link to={page.url} style={{ width: "100%", paddingLeft: 30 }}>
                                        <ListItemButton>
                                            <ListItemIcon>
                                                {page.image}
                                            </ListItemIcon>
                                            <ListItemText primary={page.name} />
                                        </ListItemButton>
                                    </Link>
                                </ListItem>
                            ))}
                        </List>
                    </Collapse>
                </List>
            </Drawer>
            <Main open={open}>
                <DrawerHeader />
                <Routes>{Pages.map(item => <Route key={item.url} path={item.url} element={item.page} />)}</Routes>
                <Routes>{PagesTimeManagment.map(item => <Route key={item.url} path={item.url} element={item.page} />)}</Routes>
            </Main>
        </Box>
    );
}
