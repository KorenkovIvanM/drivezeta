import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { GetProject } from '../../axios/FunctionProject';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import CardProject from "./Cardproject";
import FormCreateProject from "./FormCreateProject";
import { Link } from "react-router-dom";


const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function BasicGrid() {
    const dispatch = useDispatch();
    const profile = useSelector(state => state.profile);
    const [projects, setProjects] = useState([]);
    useEffect(() => { GetProject(setProjects, profile.id) }, [])
    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <Item style={{ height: 300 }}>
                        <FormCreateProject callback={() => { GetProject(setProjects, profile.id) }} UserId={profile.id} />
                    </Item>
                </Grid>
                {projects.map(
                    item => <Grid item xs={3}>
                        <Link to={`/pageProject`} onClick={() => {
                            dispatch({ type: "SET_PROJECT", project: item })}}>
                            <CardProject project={item} />
                        </Link>
                    </Grid>)}
            </Grid>
        </Box>
    );
}