import React, { useEffect, useState } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useSelector } from "react-redux";
import {
    GetTask,
    DeleteTask,
    PostTaskPostfix,
    UpdateTaskPostfix,
} from '../../axios/FunctionTasks';
import { FunctionPostTasks } from "../../axios/Function";
import 'moment/locale/en-gb';

moment.locale("en-GB");
const localizer = momentLocalizer(moment);

function formatDate(dat)
{
    return `${dat.getFullYear()}.${dat.getMonth() + 1}.${dat.getDate()}`;
}


export default function ReactBigCalendar() {
    const [eventsData, setEventsData] = useState([]);
    const profile = useSelector(state => state.profile);
    const proect = useSelector(state => state.Project)
    useEffect(() => getOrders(), []);


    const getOrders = () => {
        GetTask(setEventsData, profile.id, proect.id);
    };

    const calendarStyle = (date) => {
        let currentDate = `${new Date().getDate()} ${new Date().getMonth() + 1} ${new Date().getFullYear()}`
        let allDate = `${date.getDate()} ${date.getMonth() + 1} ${date.getFullYear()}`

        if (allDate === currentDate)
            return {
                style: {
                    backgroundColor: '#90caf9',
                }
            }
        else if (date.getMonth() !== new Date().getMonth())
            return {
                style: {
                    backgroundColor: '#121212',
                }
            }
        else
            return {
                style: {
                    backgroundColor: '#262b32',
                }
            }
    }

    const eventStyleGetter = function(event, start, end, isSelected) {
        console.log(event);
        var backgroundColor = proect.color;

        var style = {
            backgroundColor: backgroundColor,
            borderRadius: '1px',
            border: '0px',
            display: 'block'
        };
        return {
            style: style
        };
    }

    const handleSelect = ({ start, end }) => {
        const title = window.prompt("Name task");
        PostTaskPostfix(getOrders, profile.id, proect.id, `&Title=${title}&Start=${formatDate(start)}&End=${formatDate(end)}`, 0);
    };
    return (
        <div className="App">
            <Calendar
                views={["day", "agenda", "work_week", "month"]}
                selectable
                localizer={localizer}
                defaultDate={new Date()}
                defaultView="month"
                dayPropGetter={calendarStyle}
                events={eventsData.map((item) => { return {...item, style: {backgroundColor: 'black'}}})}
                style={{ height: "78vh", stopColor: "#f00" }}
                onSelectEvent={(event) => alert(event.title)}
                onSelectSlot={handleSelect}
                eventPropGetter={(eventStyleGetter)}
            />
        </div>
    );
}
