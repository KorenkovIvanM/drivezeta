import React from "react";
import TableEntity from "./../ui/TableEntity";
import {
    GetTarget,
    DeleteTarget,
    PostTargetPostfix,
    UpdateTargetPostfix,
} from "../../axios/FunctionTarget";
import { useSelector } from "react-redux";
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';

export default () => {
    var project = useSelector((state) => state.Project);
    return (
        <TableEntity
            header={[
                new ColumnsEntitysTable("name", "name", 10, "str", true),
                new ColumnsEntitysTable("value", "value", 10, "num", true),
                new ColumnsEntitysTable("max", "max", 10, "num", true),
            ]}
            get={(callback) => { GetTarget(callback, null, project.id) }}
            post={(callBack, UserId, postfix) => { PostTargetPostfix(callBack, UserId, project.id, postfix) }}
            put={UpdateTargetPostfix}
            delete={DeleteTarget}
        />
    );
}