import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import Add from '@mui/icons-material/Add';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { PostProject } from '../../axios/FunctionProject';

export default function AlertDialog(props) {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [Name, setName] = React.useState("");
    const [Discription, setDiscription] = React.useState("");
    const [End, setEnd] = React.useState(null);
    const [Icon, setIcon] = React.useState(null);

    return (
        <div>
            <IconButton color="primary" aria-label="upload picture" component="label" variant="outlined" onClick={handleClickOpen}>
                <Add sx={{ fontSize: 240 }} />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Create project"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description" sx={{ width: 500, height: 300 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={7}>
                                <Avatar
                                    variant="square"
                                    src={Icon}
                                    sx={{ width: 256, height: 256 }}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <TextField
                                    label="Name"
                                    variant="standard"
                                    value={Name}
                                    onChange={(e) => setName(e.target.value)} />
                                <TextField
                                    label="Discription"
                                    variant="standard"
                                    multiline
                                    rows={4}
                                    value={Discription}
                                    onChange={(e) => setDiscription(e.target.value)} />
                                <TextField
                                    variant="standard"
                                    type="datetime-local"
                                    value={End}
                                    onChange={(e) => setEnd(e.target.value)} />
                                <TextField
                                    label="Icon"
                                    variant="standard"
                                    value={Icon}
                                    onChange={(e) => setIcon(e.target.value)} />
                            </Grid>
                        </Grid>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    <Button onClick={() => {
                        PostProject(props.callback, props.UserId, Name, Discription, End, Icon);
                        handleClose();
                    }}>Create</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}