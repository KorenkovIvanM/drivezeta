import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { UpdateProject, DeleteProject } from "./../../axios/FunctionProject";
import Stack from '@mui/material/Stack';
import { color } from '@mui/system';
import { Link } from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default (props) => {
    const project = useSelector(state => state.Project);
    const [Name, setName] = React.useState(project.name);
    const [Discription, setDiscription] = React.useState(project.discription);
    const [End, setEnd] = React.useState(project.end);
    const [Icon, setIcon] = React.useState(project.icon);
    const [Color, setColor] = React.useState(project.color);
    console.log(project);
    return (
        <Item style={{ width: "100%", height: "400px" }}>
            <Stack spacing={4} direction="row" sx={{ color: 'action.active' }}>
                <Avatar
                    variant="square"
                    src={project.icon}
                    sx={{ width: "380px", height: "380px" }}
                />
                <div style={{ width: "100%", height: "100%" }}>
                    <TextField
                        label="Name"
                        variant="standard"
                        value={Name}
                        style={{ width: "100%" }}
                        onChange={(e) => setName(e.target.value)} />
                    <TextField
                        label="Discription"
                        variant="standard"
                        multiline
                        rows={4}
                        value={Discription}
                        style={{ width: "100%" }}
                        onChange={(e) => setDiscription(e.target.value)} />
                    <TextField
                        variant="standard"
                        type="datetime-local"
                        value={End}
                        style={{ width: "100%", paddingTop: 40 }}
                        onChange={(e) => setEnd(e.target.value)} />
                    <TextField
                        label="Icon"
                        variant="standard"
                        value={Icon}
                        style={{ width: "100%" }}
                        onChange={(e) => setIcon(e.target.value)} />
                    <TextField
                        label="Color"
                        variant="standard"
                        value={Color}
                        style={{ width: "100%" }}
                        onChange={(e) => setColor(e.target.value)} />
                    <div style={{ paddingTop: 40, height: 20 }}>
                        <Button
                            color="success"
                            variant="text"
                            onClick={() => {
                                UpdateProject(() => { console.log(true) }, project.id, Name, Discription, End, Icon, Color);
                            }}>Update</Button>
                        <Link to={'/home'}>
                        <Button
                            color="error"
                            variant="text"
                            onClick={() => {
                                DeleteProject(() => { console.log(true) }, project.id, Name);
                            }}>Delete</Button>
                        </Link>
                    </div>
                </div>
            </Stack>
            {/* <Grid container spacing={2} >
                <Grid item xs={2}></Grid>
                <Grid item xs={3}>
                    <Avatar
                        variant="square"
                        src={project.icon}
                        sx={{ width: "20vw", height: "20vw" }}
                    />
                </Grid>
                <Grid item xs={5}>
                    <TextField
                        label="Name"
                        variant="standard"
                        value={Name}
                        style={{ width: "100%" }}
                        onChange={(e) => setName(e.target.value)} />
                    <TextField
                        label="Discription"
                        variant="standard"
                        multiline
                        rows={4}
                        value={Discription}
                        style={{ width: "100%" }}
                        onChange={(e) => setDiscription(e.target.value)} />
                    <TextField
                        variant="standard"
                        type="datetime-local"
                        value={End}
                        style={{ width: "100%" }}
                        onChange={(e) => setEnd(e.target.value)} />
                    <TextField
                        label="Icon"
                        variant="standard"
                        value={Icon}
                        style={{ width: "100%" }}
                        onChange={(e) => setIcon(e.target.value)} />
                    <Button variant="text" onClick={() => {
                        UpdateProject(() => { console.log(true) }, project.id, Name, Discription, End, Icon);
                    }}>Update</Button>
                    <Button variant="text" onClick={() => {
                        DeleteProject(() => { console.log(true) }, project.id, Name);
                    }}>Delete</Button>
                </Grid>
                <Grid item xs={2}></Grid>
            </Grid> */}
        </Item>
    );
}