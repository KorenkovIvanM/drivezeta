import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useSelector } from 'react-redux';
import Profile from "./Profile";
import PageTasks from "./PageTasks";
import Calendar from "./Calendar";
import Targets from './Targets';
import HardSoft from "./../DailySchedule/HardSoftTasks";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function BasicTabs() {
    const project = useSelector(state => state.Project);
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="Profile" {...a11yProps(0)} />
                    <Tab label="Hard&Soft" {...a11yProps(1)} />
                    <Tab label="Tasks" {...a11yProps(2)} />
                    <Tab label="Target" {...a11yProps(3)} />
                    <Tab label="Calendar" {...a11yProps(4)} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <Profile />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <HardSoft height={"79vh"} project={project.id} />
            </TabPanel>
            <TabPanel value={value} index={2}>
                <PageTasks />
            </TabPanel>
            <TabPanel value={value} index={3}>
                <Targets />
            </TabPanel>
            <TabPanel value={value} index={4}>
                <Calendar />
            </TabPanel>
        </Box>
    );
}