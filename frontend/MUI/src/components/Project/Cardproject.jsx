import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

export default function ActionAreaCard(props) {
    return (
        <Card sx={{ height: 300 }}>
            <CardActionArea sx={{ height: "100%" }}>
                <CardMedia
                    component="img"
                    height="140"
                    image={props.project.icon}
                />
                <CardContent sx={{ height: 160 }}>
                    <Typography gutterBottom variant="h5" component="div">
                        {props.project.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {props.project.discription}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}