import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import TableEntity from "./../ui/TableEntity";

import {
    GetTask,
    DeleteTask,
    PostTaskPostfix,
    UpdateTaskPostfix,
} from '../../axios/FunctionTasks';
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';
import { GetGroupUser } from '../../axios/FunctionGroupUser';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default () => {
    const project = useSelector(state => state.Project);
    const group = useSelector(state => state.Group);
    const dispatch = useDispatch();
    if (group == undefined || group.length == 0)
    {
        GetGroupUser((e) => {
            console.log(e);
            dispatch({ type: "SET_GROUP", project: e })
        })
    }
    const respons = [];
    for(let index = 0, max = group.length; index < max; index++)
        respons[group[index].id] = group[index].icon;

    return (
        <Item style={{ width: "100%" }}>
            <TableEntity
                header={[
                    new ColumnsEntitysTable("title", "title", 10, "str", true),
                    new ColumnsEntitysTable("discription", "discription", 10, "str", true),
                    new ColumnsEntitysTable("create", "create", 10, "date", false),
                    new ColumnsEntitysTable("start", "start", 10, "date", true),
                    new ColumnsEntitysTable("end", "end", 10, "date", true),
                    new ColumnsEntitysTable("factEnd", "factEnd", 10, "date", false),
                    new ColumnsEntitysTable("weight", "weight", 10, "num", true),
                    new ColumnsEntitysTable("progress", "progress", 10, "num", true),
                    new ColumnsEntitysTable("status", "status", 10, "num", true),
                    new ColumnsEntitysTable("type", "type", 10, "num", true),
                    new ColumnsEntitysTable("responsibility", "responsibility", 10, "ico", true),
                ]}
                get={(callback, UserId) => GetTask(callback, UserId, project.id)}
                post={(callback, UserId, postfix) => PostTaskPostfix(callback, UserId, project.id, postfix)}
                delete={DeleteTask}
                put={UpdateTaskPostfix} />
        </Item>
    );
}