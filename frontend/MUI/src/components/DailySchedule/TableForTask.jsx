import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import ButtonMenu from "./../ui/ButtonMenu";
import ActionMenu from "./../../libraries/ActionMenu";
import EditIcon from '@mui/icons-material/Edit';
import Add from '@mui/icons-material/Add';
import Settings from '@mui/icons-material/Settings';
import DeleteOutline from '@mui/icons-material/DeleteOutline';
import IconButton from '@mui/material/IconButton';
import {
    FunctionDeketeTasks,
    FunctionPostTasks,
    FunctionPutTasksStatus,
    FunctionPutTasks,
    FunctionPutTasksProgress, 
    FunctionPutTasksPrioritet
} from "./../../axios/Function";
import PanoramaFishEyeIcon from '@mui/icons-material/PanoramaFishEye';
import TaskAlt from '@mui/icons-material/TaskAlt';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import DateInput from "./../ui/DateInput";
import Schet from "./../ui/Schet";

export default function BasicTable(props) {
    const [open, setOpen] = React.useState(false);
    const [title, setTitle] = React.useState("");
    const [time, setTime] = React.useState(new Date());
    const [end, setEnd] = React.useState(new Date());
    const [isUpdate, setIsUpdate] = React.useState(undefined);

    let [priority, setPriority] = React.useState(0);

    const moveDrfault = () => {
        setTitle("");
    };

    const handleClickOpen = () => {
        setPriority(props.rows.reduce((accumulator, item) => item.prioritet > accumulator ? item.prioritet : accumulator, 0) + 1);
        setOpen(true);
    };

    const handleClose = () => {
        setIsUpdate(undefined);
        setOpen(false);
    };
    return (
        <TableContainer component={Paper} sx={{ maxHeight: "100%" }}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {props.lable !== 2 && <TableCell align="center"></TableCell>}
                        {props.headers.map((item) => <TableCell key={item} align="center" >{item}</TableCell>)}
                        <TableCell align="right">
                            <IconButton aria-label="delete" onClick={handleClickOpen}>
                                <Add />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.rows.map((row) => (
                        <TableRow
                            key={row.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            {props.lable !== 2 &&
                                <TableCell component="th" scope="row">
                                    {row.status === 1 &&
                                        <IconButton aria-label="delete" onClick={() => { FunctionPutTasksStatus(() => props.callback(), row.id, 2) }}>
                                            <PanoramaFishEyeIcon color="primary" />
                                        </IconButton>}
                                    {row.status === 2 &&
                                        <IconButton aria-label="delete">
                                            <TaskAlt color="success" />
                                        </IconButton>}
                                </TableCell>}

                            {/* Soft */}
                            {props.lable === 1 && <TableCell component="th" scope="row" align="center">
                                {/* {row["prioritet"]} */}
                                <Schet 
                                    value={row.prioritet}
                                    prev={() => {
                                        FunctionPutTasksPrioritet(() => props.callback(), row.id, row.prioritet - 1)
                                    }}
                                    next={() => {
                                        FunctionPutTasksPrioritet(() => props.callback(), row.id, row.prioritet + 1)
                                    }}/>
                                </TableCell>}
                            {props.lable === 1 && <TableCell component="th" scope="row" align="center">{row.title}</TableCell>}
                            {props.lable === 1 && <TableCell component="th" scope="row" align="center">
                                <Schet 
                                    value={row.progress}
                                    prev={() => {
                                        FunctionPutTasksProgress(() => props.callback(), row.id, row.progress - 10)
                                    }}
                                    next={() => {
                                        FunctionPutTasksProgress(() => props.callback(), row.id, row.progress + 10)
                                    }}/>
                            </TableCell>}

                            {/* Hard */}
                            {props.lable === 0 && <TableCell component="th" scope="row" align="center">{row.title}</TableCell>}
                            {props.lable === 0 && <TableCell component="th" scope="row" align="center">{row.start}</TableCell>}
                            {props.lable === 0 && <TableCell component="th" scope="row" align="center">{row.end}</TableCell>}

                            {/* Compaer */}
                            {props.lable === 2 && <TableCell component="th" scope="row" align="center">{row.title}</TableCell>}
                            {props.lable === 2 && <TableCell component="th" scope="row" align="center">{row.create}</TableCell>}

                            <TableCell align="right">
                                <ButtonMenu
                                    name={<Settings />}
                                    list={[
                                        new ActionMenu("Delete", <DeleteOutline style={{color: "rgb(233, 30, 99)"}} />, () => FunctionDeketeTasks(() => props.callback(), row.id)),
                                        new ActionMenu("Update", <EditIcon  style={{color: "rgb(206, 147, 216)"}} />, () => {
                                            setIsUpdate(row);
                                            setTitle(row.title);
                                            setEnd(row.end);
                                            setTime(row.start);
                                            handleClickOpen();
                                        }),
                                    ]} />
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {isUpdate ? "Update tasks" : "Add tasks"}
                </DialogTitle>
                <DialogContent style={{ width: 600 }}>
                    <DialogContentText id="alert-dialog-description">
                        <TextField
                            style={{ width: "100%" }}
                            label="Title"
                            variant="standard"
                            value={title}
                            onChange={(act) => setTitle(act.target.value)} />
                        {props.lable === 1 &&
                            <div>
                                <br /><br /><br />
                                <TextField
                                    value={priority}
                                    onChange={(act) => setPriority(act.target.value)}
                                    style={{ width: "45%" }}
                                    label="Priority"
                                    variant="standard"
                                    type={"number"} />
                            </div>
                        }
                        {props.lable === 0 &&
                            <div>
                                <br /><br /><br />
                                <DateInput label={"Start"} time={time} callback={setTime} />
                                <br /><br /><br />
                                <DateInput label={"End"} time={end} callback={setEnd} />
                            </div>
                        }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancellation</Button>
                    {isUpdate ?
                        <Button onClick={() => {
                            if (props.lable === 0) {
                                FunctionPutTasks(() => props.callback(), isUpdate.id, title, "", 1, props.lable, priority, time, end);
                            }
                            else if (props.lable === 1) {
                                // TODO исправь здесь не ставиться приоритет есил не обновить его 
                                FunctionPutTasks(() => props.callback(), isUpdate.id, title, "", 1, props.lable, priority);
                            }
                            else if (props.lable === 2) {
                                FunctionPutTasks(() => props.callback(), isUpdate.id, title, "", 1, props.lable);
                            }
                            moveDrfault();
                            handleClose();
                        }} autoFocus>Update</Button> :
                        <Button onClick={() => {
                            if (props.lable === 0) {
                                FunctionPostTasks(() => props.callback(), title, "", 1, props.lable, priority, time, end);
                            }
                            else if (props.lable === 1) {
                                // TODO исправь здесь не ставиться приоритет есил не обновить его 
                                FunctionPostTasks(() => props.callback(), title, "", 1, props.lable, priority);
                            }
                            else if (props.lable === 2) {
                                FunctionPostTasks(() => props.callback(), title, "", 1, props.lable);
                            }
                            moveDrfault();
                            handleClose();
                        }} autoFocus>Add</Button>
                    }

                </DialogActions>
            </Dialog>
        </TableContainer>
    );
}
