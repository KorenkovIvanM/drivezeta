import TableForTask from "./TableForTask";
import React, { useState } from "react";
import { useEffect } from "react";
import { FunctionGetTasksType } from "./../../axios/Function";
import { useSelector } from "react-redux";

const Lable = 1;

export default (props) => {
    console.log(props.project);
    let profile = useSelector(state => state.profile);
    useEffect(() => {
        FunctionGetTasksType(profile.id, Lable, setTasks, props.project);
    }, [profile]);
    
    const [tasks, setTasks] = useState([]);

    return (
        <TableForTask rows={tasks} headers={["Priority", "Title", "Progress"]} callback={() => FunctionGetTasksType(profile.id, Lable, setTasks, props.project)} lable={Lable} height={props.height} project={props.project} />
    );
}