import TableForTask from "./TableForTask";
import React, { useState } from "react";
import { useEffect } from "react";
import { FunctionGetTasksType } from "./../../axios/Function"
import { useSelector } from "react-redux";

const Lable = 0;

export default (props) => {
    const profile = useSelector(state => state.profile);
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        FunctionGetTasksType(profile.id, Lable, setTasks, props.project);
    }, [profile]);
    
    return (
        <TableForTask rows={tasks} headers={["Title", "Start", "End"]} callback={() => {
            FunctionGetTasksType(profile.id, Lable, setTasks, props.project)}} lable={Lable} height={props.height} project={props.project} />
    );
}