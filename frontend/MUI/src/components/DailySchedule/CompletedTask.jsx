import TableForTask from "./TableForTask";
import React, { useState } from "react";
import { useEffect } from "react";
import { FunctionGetTasksType } from "./../../axios/Function"
import { useSelector } from "react-redux";

const Lable = 2;

export default () => {
    const profile = useSelector(state => state.profile);
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        FunctionGetTasksType(profile.id, Lable, setTasks);
    }, [profile]);

    return (
        <TableForTask rows={tasks} headers={["Title", "Time"]} callback={() => FunctionGetTasksType(profile.id, Lable, setTasks)} lable={Lable} />
    );
}