import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import "./style.css";

import HardTask from "./HardTask";
import SoftTask from './SoftTask';
import CompletedTask from './CompletedTask';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function BasicGrid(props) {
  return (
    <Box sx={{ flexGrow: 0 }}>
      <Grid container spacing={1}>
        <Grid item xs={4}>
          <Item className="winTable">
            <SoftTask project={props.project} />
          </Item>
        </Grid>
        <Grid item xs={4}>
          <Item className="winTable">
            <HardTask project={props.project} />
          </Item>
        </Grid>
        <Grid item xs={4}>
          <Item className="winTable">
            <CompletedTask project={props.project} />
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
}
