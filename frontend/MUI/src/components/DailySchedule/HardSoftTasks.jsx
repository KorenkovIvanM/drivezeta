import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import "./style.css";

import HardTask from "./HardTask";
import SoftTask from './SoftTask';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function BasicGrid(props) {
  return (
    <Box sx={{ flexGrow: 0 }}>
      <Grid container spacing={1}>
        <Grid item xs={7}>
          <Item className="winTable" sx={{ height: props.height }}>
            <SoftTask project={props.project} />
          </Item>
        </Grid>
        <Grid item xs={5}>
          <Item className="winTable" sx={{ height: props.height }}>
            <HardTask project={props.project} />
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
}
