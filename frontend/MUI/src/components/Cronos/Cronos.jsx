import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
    GetCronos,
    DeleteCronos,
    PostCronosPostfix,
    UpdateCronosPostfix
} from '../../axios/FunctionCronos';
import {
    GetOrderCronos,
    PostOrderCronosPostfix,
    DeleteOrderCronos,
    UpdateOrderCronosPostfix,
} from '../../axios/FunctionOrderCronos';
import TableEntity from "./../ui/TableEntity";
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

// function a11yProps(index) {
//     return {
//         id: `simple-tab-${index}`,
//         'aria-controls': `simple-tabpanel-${index}`,
//     };
// }

export default function BasicTabs() {
    const [coros, setCoros] = useState([]);
    const profile = useSelector(state => state.profile);


    useEffect(() => {
        GetCronos(setCoros, profile.id)
    }, [coros.length]);

    const [value, setValue] = React.useState(3);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    {coros.map((item) => <Tab label={item.title} value={item.id} key={item.id} />)}
                </Tabs>
            </Box>
            {coros.map((item) => <TabPanel value={value} index={item.id} key={item.id}>
                <TableEntity
                    header={[
                        new ColumnsEntitysTable("title", "title", 10, "str", true),
                        new ColumnsEntitysTable("discription", "discription", 10, "str", true),
                        new ColumnsEntitysTable("create", "create", 10, "date", false),
                        new ColumnsEntitysTable("create", "Dowd", 10, "dop", false),
                    ]}
                    get={(callback, id) => { GetOrderCronos(callback, id, item.id) }}
                    post={(callback, id, postfix) => { PostOrderCronosPostfix(callback, id, item.id, postfix) }}
                    delete={DeleteOrderCronos}
                    put={UpdateOrderCronosPostfix}
                    dopFunction={(callback, id) => {UpdateOrderCronosPostfix(callback, id, "", true)}} />
            </TabPanel>)}
        </Box>
    );
}
