import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';





import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
    GetMWD,
    DeleteMWD,
    PostMWDPostfix,
    UpdateMWDPostfix
} from '../../axios/FunctionMWD';
import TableEntity from "./../ui/TableEntity";
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

export default function BasicTabs() {
    const [coros, setCoros] = useState([]);
    const profile = useSelector(state => state.profile);


    useEffect(() => {
        GetMWD(setCoros, profile.id)
    }, [coros.length]);

    const [value, setValue] = React.useState(3);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="Month" value={0} />
                    <Tab label="Week" value={1} />
                </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
                <TableEntity
                    header={[
                        new ColumnsEntitysTable("title", "title", 10, "str", true),
                        new ColumnsEntitysTable("discription", "discription", 10, "str", true),
                        new ColumnsEntitysTable("create", "create", 10, "date", false),
                        new ColumnsEntitysTable("dop", "down", 10, "dop", false),
                    ]}
                    get={(callback, id) => { GetMWD(callback, id, true) }}
                    post={(callback, id, postfix) => { PostMWDPostfix(callback, id, postfix, true) }}
                    delete={DeleteMWD}
                    put={UpdateMWDPostfix}
                    dopFunction={(callback, id) => { UpdateMWDPostfix(callback, id, "", true) }} />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <TableEntity
                    header={[
                        new ColumnsEntitysTable("title", "title", 10, "str", true),
                        new ColumnsEntitysTable("discription", "discription", 10, "str", true),
                        new ColumnsEntitysTable("create", "create", 10, "date", false),
                        new ColumnsEntitysTable("down", "down", 10, "dop", false),
                    ]}
                    get={(callback, id) => { GetMWD(callback, id, false) }}
                    post={(callback, id, postfix) => { PostMWDPostfix(callback, id, postfix, false) }}
                    delete={DeleteMWD}
                    put={UpdateMWDPostfix}
                    dopFunction={(callback, id) => { UpdateMWDPostfix(callback, id, "", true) }} />
            </TabPanel>
        </Box>
    );
}
