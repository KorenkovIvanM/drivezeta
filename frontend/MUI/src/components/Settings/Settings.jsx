import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import {
  GetTemplates,
  DeleteTemplates,
  PostTemplatesPostfix,
  UpdateTemplatesPostfix
} from '../../axios/FunctionTemplate';
import {
  GetCronos,
  DeleteCronos,
  PostCronosPostfix,
  UpdateCronosPostfix
} from '../../axios/FunctionCronos';
import TableEntity from "./../ui/TableEntity";
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
  height: "86vh",
}));


export default function VerticalTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: "88vh", width: "100%" }}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >
        <Tab label="Auto tasks" {...a11yProps(0)} />
        <Tab label="Cronos" {...a11yProps(1)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <Item>
          <TableEntity 
            header={[
              new ColumnsEntitysTable("title", "title", 10, "str", true),
              new ColumnsEntitysTable("cteate", "cteate", 10, "date", true),
              new ColumnsEntitysTable("weight", "weight", 10, "num", true),
              new ColumnsEntitysTable("actev", "actev", 10, "bit", true),
              new ColumnsEntitysTable("monday", "monday", 10, "bit", true),
              new ColumnsEntitysTable("tuesday", "tuesday", 10, "bit", true),
              new ColumnsEntitysTable("wednesday", "wednesday", 10, "bit", true),
              new ColumnsEntitysTable("thursday", "thursday", 10, "bit", true),
              new ColumnsEntitysTable("friday", "friday", 10, "bit", true),
              new ColumnsEntitysTable("saturday", "saturday", 10, "bit", true),
              new ColumnsEntitysTable("sunday", "sunday", 10, "bit", true),
              new ColumnsEntitysTable("time", "time", 10, "date", true),
            ]}
            get={GetTemplates}
            post={PostTemplatesPostfix}
            delete={DeleteTemplates}
            put={UpdateTemplatesPostfix}
            width={"88vw"} />
        </Item>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Item>
          <TableEntity
            header={[
              new ColumnsEntitysTable("title", "title", 10, "str", true),
            ]}
            get={GetCronos}
            post={PostCronosPostfix}
            delete={DeleteCronos}
            put={UpdateCronosPostfix}
            width={"88vw"} />
        </Item>
      </TabPanel>
    </Box>
  );
}
