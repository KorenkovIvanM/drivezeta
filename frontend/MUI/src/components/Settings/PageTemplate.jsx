import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useState } from 'react';
import { useEffect } from 'react';
import FormCreateTemplate from "./FormCreateTemplate";
import { useSelector } from "react-redux";

import ButtonMenu from "./../ui/ButtonMenu";
import ActionMenu from "./../../libraries/ActionMenu";
import EditIcon from '@mui/icons-material/Edit';

import Settings from '@mui/icons-material/Settings';
import DeleteOutline from '@mui/icons-material/DeleteOutline';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';


import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import TextField from '@mui/material/TextField';

import { UpdateTemplates } from '../../axios/FunctionTemplate';


export default function BasicTable(props) {
    const profile = useSelector(state => state.profile);
    const [rows, setRows] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [chooseId, setChooseId] = useState(0);
    const [title, setTitle] = useState("");

    const handleClickOpen = (id) => {
        setChooseId(id);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    useEffect(() => {
        props.get(setRows, profile.id);
    }, [rows.length]);
    const [actev, setActev] = React.useState(false);

    const handleChange = (event) => {
        setActev(event.target.value);
    };
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: "88vw" }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {props.header.map((item) => <TableCell>{item}</TableCell>)}
                        <TableCell>
                            <FormCreateTemplate post={props.post} callback={() => props.get(setRows, profile.id)} />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.Id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            {props.header.map((nameCell) => <TableCell>{`${row[nameCell]}`}</TableCell>)}
                            <TableCell align="right">
                                <ButtonMenu
                                    name={<Settings />}
                                    list={[
                                        new ActionMenu(
                                            "Delete",
                                            <DeleteOutline style={{ color: "rgb(233, 30, 99)" }} />,
                                            () => props.delete(
                                                () => props.get(setRows, profile.id),
                                                row.id)),
                                        new ActionMenu("Update", <EditIcon style={{ color: "rgb(206, 147, 216)" }} />, () => { handleClickOpen(row.id); }),
                                    ]} />
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Use Google's location service?"}
                </DialogTitle>
                <DialogContent style={{ width: 600 }}>
                    <DialogContentText id="alert-dialog-description">
                        <TextField
                            onChange={(e) => setTitle(e.target.value)}
                            autoFocus
                            label="Title"
                            variant="standard"
                            style={{ width: "100%" }} />
                        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                            <InputLabel id="demo-simple-select-standard-label">Actev</InputLabel>
                            <Select
                                labelId="demo-simple-select-standard-label"
                                value={actev}
                                onChange={handleChange}
                                label="Actev"
                            >
                                <MenuItem value={false}>
                                    <em>False</em>
                                </MenuItem>
                                <MenuItem value={true}>True</MenuItem>
                                <MenuItem value={false}>False</MenuItem>
                            </Select>
                        </FormControl>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    <Button onClick={() => {
                        UpdateTemplates(() => props.get(setRows, profile.id), chooseId, title, actev);
                        handleClose();
                    }}>Update</Button>
                </DialogActions>
            </Dialog>
        </TableContainer>
    );
}
