import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { IconButton } from '@mui/material';
import Add from '@mui/icons-material/Add';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { useSelector } from "react-redux";

export default function AlertDialog(props) {
    const [open, setOpen] = React.useState(false);
    const [title, setTitle] = useState("");
    const profile = useSelector(state => state.profile);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <IconButton aria-label="delete" onClick={handleClickOpen}>
                <Add />
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"

            >
                <DialogTitle id="alert-dialog-title">
                    {"Create template for repiat tasks."}
                </DialogTitle>
                <DialogContent style={{ width: 600 }}>
                    <DialogContentText id="alert-dialog-description">
                        <TextField
                            onChange={(e) => setTitle(e.target.value)}
                            autoFocus
                            label="Title"
                            variant="standard"
                            style={{ width: "100%" }} />
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    <Button onClick={() => {
                        props.post(props.callback, title, profile.id);
                        handleClose();
                    }}>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
