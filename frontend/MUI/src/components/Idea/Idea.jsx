import * as React from 'react';
import {
    GetIdea,
    DeleteIdea,
    PostIdeaPostfix,
    UpdateIdeaPostfix
} from '../../axios/FunctionIdea';
import TableEntity from "./../ui/TableEntity";
import ColumnsEntitysTable from '../../libraries/ColumnsEntitysTable';

export default () => {
    return (
        <TableEntity
            header={[
                new ColumnsEntitysTable("title", "title", 10, "str", true),
                new ColumnsEntitysTable("discription", "discription", 10, "str", true),
                new ColumnsEntitysTable("create", "create", 10, "date", false),
            ]}
            get={GetIdea}
            post={PostIdeaPostfix}
            delete={DeleteIdea}
            put={UpdateIdeaPostfix}/>
    );
};