import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import Add from '@mui/icons-material/Add';
import { IconButton } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ButtonMenu from "./../ui/ButtonMenu";
import ActionMenu from "./../../libraries/ActionMenu";
import EditIcon from '@mui/icons-material/Edit';
import Settings from '@mui/icons-material/Settings';
import DeleteOutline from '@mui/icons-material/DeleteOutline';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import ArrowDownward from '@mui/icons-material/ArrowDownward';

export default function BasicTable(props) {

    //-------------------------------------------------------------------------------
    //                          Xyki
    const profile = useSelector(state => state.profile);
    const [rows, setRows] = useState([]);
    const [chooseId, setChooseId] = useState(null);
    useEffect(() => {
        props.get(setRows, profile.id);
    }, [rows.length]);
    //-------------------------------------------------------------------------------
    //              windows create update
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = (id) => {
        setChooseId(id);
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    //-------------------------------------------------------------------------------
    const currentValue = {};
    let width = props.width ? props.width : "100%";
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: width }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {props.header.map((item) => <TableCell>{item.headerName}</TableCell>)}
                        <TableCell width={"30px"}>
                            <IconButton aria-label="delete" onClick={() => handleClickOpen(0)}>
                                <Add />
                            </IconButton>
                            {/* <FormCreateTemplate post={props.post} callback={() => props.get(setRows, profile.id)} /> */}
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.Id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            {props.header.map((nameCell) => <TableCell>{nameCell.type === "dop" ? 
                                <IconButton aria-label="delete" onClick={() => props.dopFunction(() => props.get(setRows, profile.id), row.id)}>
                                    <ArrowDownward />
                                </IconButton> : `${row[nameCell.field]}`}</TableCell>)}
                            {/* {props.dopFunction && } */}
                            <TableCell>
                                <ButtonMenu
                                    name={<Settings />}
                                    list={[
                                        new ActionMenu(
                                            "Delete",
                                            <DeleteOutline style={{ color: "rgb(233, 30, 99)" }} />,
                                            () => props.delete(
                                                () => props.get(setRows, profile.id),
                                                row.id)),
                                        new ActionMenu("Update", <EditIcon style={{ color: "rgb(206, 147, 216)" }} />, () => handleClickOpen(row.id)),
                                    ]} />
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {chooseId !== 0 ? "Update Template" : "Create template"}
                </DialogTitle>
                <DialogContent style={{ width: 600 }}>
                    <DialogContentText id="alert-dialog-description">
                        {props.header.map((item) => {
                            if (item.isUpdate) {
                                if (item.type === "str")
                                    return <TextField
                                        label={item.headerName}
                                        variant="standard"
                                        style={{ width: "100%", padding: "15px" }}
                                        onChange={(e) => currentValue[item.field] = e.target.value} />
                                else if (item.type === "num")
                                    return <TextField
                                        label={item.headerName}
                                        variant="standard"
                                        type={"number"}
                                        style={{ width: "100%", padding: "15px" }}
                                        onChange={(e) => currentValue[item.field] = e.target.value} />
                                else if (item.type === "bit")
                                    return <FormControl variant="standard" sx={{ m: 1 }} style={{ width: "100%", padding: "15px" }}>
                                        <InputLabel id="demo-simple-select-standard-label">{item.headerName}</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-standard-label"
                                            onChange={(e) => currentValue[item.field] = e.target.value}
                                            label={item.headerName}
                                        >
                                            <MenuItem value={true}>True</MenuItem>
                                            <MenuItem value={false}>False</MenuItem>
                                        </Select>
                                    </FormControl>
                                else if (item.type === "date")
                                    return <TextField
                                        label={item.headerName}
                                        type="datetime-local"
                                        style={{ width: "100%", padding: "15px" }}
                                        defaultValue={new Date()}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                // else if (item.type === "dop")
                                //     return <IconButton aria-label="delete" onClick={() => props.dopFunction(() => props.get(setRows, profile.id), item.id)}>
                                //                 <Add />
                                //             </IconButton>;
                            }
                        })}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    {chooseId != 0 ?
                        <Button onClick={() => {
                            let url = "";
                            for (let item in currentValue)
                                url += `&${item}=${currentValue[item]}`
                            props.put(() => props.get(setRows, profile.id), chooseId, url);
                            handleClose();
                        }}>
                            Update
                        </Button> :
                        <Button onClick={() => {
                            let url = "";
                            for (let item in currentValue)
                                url += `&${item}=${currentValue[item]}`
                            props.post(() => props.get(setRows, profile.id), profile.id, url);
                            handleClose();
                        }}>
                            Create
                        </Button>}

                </DialogActions>
            </Dialog>
        </TableContainer>
    );
}
