import React, { useEffect } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { gapi } from 'gapi-script';
import { useDispatch, useSelector } from 'react-redux';

function App() {
    const dispatch = useDispatch();
    const profile = useSelector(state => state.profile);
    const clientId = '777686739935-gjsp3akddbt9i9nv85ndd86kd2aolgkm.apps.googleusercontent.com';
    useEffect(() => {
        const initClient = () => {
            gapi.client.init({
                clientId: clientId,
                scope: ''
            });
        };
        gapi.load('client:auth2', initClient);
    });

    const onSuccess = (res) => {
        dispatch({
            type: "ACTIV_USER", 
            profile: res.profileObj, 
            callback: 
            (e) => {
                dispatch({type: "SET_USER_ID", id: e});
            }})
    };

    const onFailure = (err) => {
        console.log('failed', err);
    };

    const logOut = () => {
        dispatch({type: "NON_ACTIV_USER"});
    };

    if(profile.isActev)
        return <GoogleLogout clientId={clientId} buttonText="Log out" onLogoutSuccess={logOut} />;
    else
        return <GoogleLogin
            clientId={clientId}
            buttonText="Sign in with Google"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={'single_host_origin'}
            isSignedIn={true}/>;
}
export default App;