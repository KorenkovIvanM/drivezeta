import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import Checkbox from '@mui/material/Checkbox';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

export default function InsetDividers(props) {
    return (
        <List
            sx={{
                width: '100%',
                bgcolor: 'background.paper',
            }}
        >
            {props.list.map((e) => {
                return (
                    <div>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar src={e.icon} />
                            </ListItemAvatar>
                            <ListItemText primary={e.name} secondary={`${e.end}`.split('T')[0]} />
                            <Checkbox {...label} defaultChecked style={{color: e.color}} onChange={(event, v) => props.callback(e.id, v)} />
                        </ListItem>
                        <Divider variant="inset" component="li" />
                    </div>
                );
            })}
        </List>
    );
}