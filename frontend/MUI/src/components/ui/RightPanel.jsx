import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Settings from '@mui/icons-material/Settings';
import IconButton from '@mui/material/IconButton';
import ArrowCircleDownIcon from '@mui/icons-material/ArrowCircleDown';
import Sort from '@mui/icons-material/Sort';
import { FunctionDownTask, FunctionSortOrders } from "./../../axios/FunctionToolsOrders";
import { useSelector } from 'react-redux';

export default function TemporaryDrawer() {
    
    const profile = useSelector(state => state.profile);
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <Box
            sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                <ListItem disablePadding>
                    <ListItemButton onClick={() => FunctionDownTask(() => console.log("True"), profile.id)}>
                        <ListItemIcon>
                            <ArrowCircleDownIcon />
                        </ListItemIcon>
                        <ListItemText primary={"Down Task"} />
                    </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                    <ListItemButton onClick={() => FunctionSortOrders(() => console.log("True"), profile.id)}>
                        <ListItemIcon>
                            <Sort />
                        </ListItemIcon>
                        <ListItemText primary={"Sort prioritet"} />
                    </ListItemButton>
                </ListItem>
            </List>
        </Box>
    );

    const anchor = "right";

    return (
        <React.Fragment>
            <IconButton aria-label="upload picture" component="label" onClick={toggleDrawer(anchor, true)}>
                <Settings />
            </IconButton>
            <Drawer
                anchor={anchor}
                open={state[anchor]}
                onClose={toggleDrawer(anchor, false)}
            >
                {list(anchor)}
            </Drawer>
        </React.Fragment>
    );
}
