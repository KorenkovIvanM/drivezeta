import * as React from 'react';
import ButtonGroup from '@mui/material/ButtonGroup';
import IconButton from '@mui/material/IconButton';
import Add from '@mui/icons-material/Add';
import Remove from '@mui/icons-material/Remove';

export default function VariantButtonGroup(props) {
    return (
        <ButtonGroup variant="text" aria-label="text button group">
            <IconButton>
                <Remove onClick={(e) => {
                    props.prev();
                }} />
            </IconButton>
            <IconButton>
                {props.value}
            </IconButton>
            <IconButton>
                <Add onClick={(e) => {
                    props.next()
                }} />
            </IconButton>
        </ButtonGroup>
    );
}