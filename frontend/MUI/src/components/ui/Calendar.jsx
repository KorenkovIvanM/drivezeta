import React, { useEffect, useState } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useSelector } from "react-redux";
import {
    GetTask,
    DeleteTask,
    PostTaskPostfix,
    UpdateTaskPostfix,
} from '../../axios/FunctionTasks';
import { FunctionPostTasks } from "../../axios/Function";
import 'moment/locale/en-gb';
import ListProject from "./ListProject";


import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Unstable_Grid2';
import { GetProject } from "../../axios/FunctionProject";

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));



moment.locale("en-GB");
const localizer = momentLocalizer(moment);

function formatDate(dat) {
    return `${dat.getFullYear()}.${dat.getMonth() + 1}.${dat.getDate()}`;
}


export default function ReactBigCalendar() {
    const [eventsData, setEventsData] = useState([]);
    const [projects, setProjects] = useState([]);
    const [colorProject, setColorProject] = useState([]);
    colorProject[0] = { color: "#9e9e9e", isShow: true };
    for (let index = 0, max = projects.length; index < max; index++)
        colorProject[projects[index].id] = { color: projects[index].color, isShow: true };
    const profile = useSelector(state => state.profile);
    useEffect(() => {
        getOrders();
    }, []);


    const getOrders = () => {
        GetProject(setProjects, profile.id);
        GetTask(setEventsData, profile.id, false, `&Type=0`);
    };

    const calendarStyle = (date) => {
        let currentDate = `${new Date().getDate()} ${new Date().getMonth() + 1} ${new Date().getFullYear()}`
        let allDate = `${date.getDate()} ${date.getMonth() + 1} ${date.getFullYear()}`

        if (allDate === currentDate)
            return {
                style: {
                    backgroundColor: '#90caf9',
                }
            }
        else if (date.getMonth() !== new Date().getMonth())
            return {
                style: {
                    backgroundColor: '#121212',
                }
            }
        else
            return {
                style: {
                    backgroundColor: '#262b32',
                }
            }
    }

    const eventStyleGetter = function (event, start, end, isSelected) {
        // event
        var backgroundColor = colorProject[event.projectId].color || "#9e9e9e";//proect.color;

        var style = {
            backgroundColor: backgroundColor,
            borderRadius: '1px',
            border: '0px',
            display: 'block'
        };
        return {
            style: style
        };
    }

    const handleSelect = ({ start, end }) => {
        const title = window.prompt("Name task");
        PostTaskPostfix(getOrders, profile.id, 0, `&Title=${title}&Start=${formatDate(start)}&End=${formatDate(end)}`, 0);
    };
    return (
        <Grid container spacing={2}>
            <Grid xs={3}>
                <Item style={{ height: "88vh" }}>
                    <ListProject list={projects} callback={(id, value) => {
                        colorProject[id].isShow = value;
                        GetTask((result) => {
                            let _result = result.filter(e => e.projectId).filter(e => colorProject[e.color ? e.projectId : 0].isShow)
                            setEventsData(_result);
                        }, profile.id, false, `&Type=0`);
                        
                        // setColorProject([...colorProject])
                    }
                    } />
                </Item>
            </Grid>
            <Grid xs={9}>
                <Calendar
                    views={["day", "agenda", "work_week", "month"]}
                    selectable
                    localizer={localizer}
                    defaultDate={new Date()}
                    defaultView="month"
                    dayPropGetter={calendarStyle}
                    events={eventsData.filter(e => e.projectId).filter(e => colorProject[e.color ? e.projectId : 0].isShow)}
                    style={{ height: "100%", stopColor: "#f00" }}
                    //onSelectEvent={(event) => alert(event.title)}
                    onSelectSlot={handleSelect}
                    eventPropGetter={(eventStyleGetter)}
                />
            </Grid>
        </Grid>
    );
}
