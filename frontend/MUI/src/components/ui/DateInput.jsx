import * as React from 'react';
import TextField from '@mui/material/TextField';

export default function NativePickers(props) {
    return (
        <TextField
            id="datetime-local"
            label={props.label}
            type="datetime-local"
            defaultValue={props.time}
            onChange={(e) => props.callback(e.target.value)}
            value={props.time}
            sx={{ width: 250 }}
            InputLabelProps={{
                shrink: true,
            }}
        />
    );
}
