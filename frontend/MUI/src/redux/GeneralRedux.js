import React from "react";

// Libraries
import Page from "../libraries/Page";

// icon 
import Home from '@mui/icons-material/Home';
import AccessTime from "@mui/icons-material/AccessTime";
import SettingsIcon from '@mui/icons-material/Settings';
import AvTimer from '@mui/icons-material/AvTimer';
import DateRange from '@mui/icons-material/DateRange';
import Lightbulb from '@mui/icons-material/Lightbulb';
import AccountTree from '@mui/icons-material/AccountTree';
import Apps from '@mui/icons-material/Apps';
import CalendarToday from '@mui/icons-material/CalendarToday';

// Page
import DailySchedule from "../components/DailySchedule/DailySchedule";
import { Autorisation } from "../axios/FunctionUser";
import Settings from "../components/Settings/Settings";
import Coros from "./../components/Cronos/Cronos";
import MWD from "./../components/MWD/WMD";
import Idea from "./../components/Idea/Idea";
import HardSoftTasks from "./../components/DailySchedule/HardSoftTasks";
import ListProject from "./../components/Project/ListProject";
import PageProject from "./../components/Project/PageProject";
import Calendar from "./../components/ui/Calendar";

const defaultState = {
    profile: {
        isActev: false,
        id: 0,
    },
    Site: {
        Name: "DRIVEZETA",
    },
    Pages: [
        new Page("", "/", <Home />, <div></div>, false, true),
        new Page("Home", "/home", <Home />, <div></div>, true, true),
        new Page("Setings", "/settings", <SettingsIcon />, <Settings />, true, false),
        new Page("Project", "/project", <Apps />, <ListProject />, true, false),
        new Page("Page Project", "/pageProject", <Apps />, <PageProject />, false, false),
    ],
    PagesTimeManagment: [
        new Page("Daily schedule", "/dailySchedule", <AccessTime />, <DailySchedule />, true, false),
        new Page("Cronos", "/cronos", <AvTimer />, <Coros />, true, false),
        new Page("Month Week Day", "/mwd", <DateRange />, <MWD />, true, false),
        new Page("Idea", "/idea", <Lightbulb />, <Idea />, true, false),
        new Page("Hard&Soft tasks", "/hardsofttasks", <AccountTree />, <HardSoftTasks />, true, false),
        new Page("Calendar", "/calendar", <CalendarToday />, <Calendar />, true, false),
    ],
    Project: {

    },
    Group: [
        
    ],
}

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case "SET_USER_ID":
            return { ...state, profile: { ...state.profile, id: action.id } };
        case "ACTIV_USER":
            Autorisation(action.callback, action.profile.name, action.profile.email);
            return { ...state, profile: { ...action.profile, isActev: true } };
        case "NON_ACTIV_USER":
            return { ...state, profile: { isActev: false } };
        case "SET_PROJECT":
            return { ...state, Project: { ...action.project } };
        case "SET_GROUP":
            return { ...state, Group: { ...action.group } };
        default:
            return { ...state };
    }

}

export default reducer;