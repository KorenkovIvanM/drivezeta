import React from 'react'
import { Card } from 'antd'
import FullTable from './../UI/FullTable'
import "./../index.css"
import TableEntity from '../UI/TableEntity'
import Idea from '../Libraries/Entitys/Idea'
import type { ColumnsType } from 'antd/es/table'

const columns: ColumnsType<Idea> = [
    {
        title: 'tile',
        dataIndex: 'tile',
        key: 'tile',
        sorter: (a, b) => a.title.length - b.title.length
    },
    {
        title: 'description',
        dataIndex: 'description',
        key: 'description',
        sorter: (a, b) => a.description.length - b.description.length
    },
];

export default () => {
    return (
        <Card className='cardPage'>
            <TableEntity Columns={columns} Entity={"Idea"}/>
        </Card>
    )
}