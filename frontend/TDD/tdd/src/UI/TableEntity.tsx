import React, { useState, useEffect } from 'react'
import { Space, Switch, Table } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import type { TableRowSelection } from 'antd/es/table/interface'
import {Get} from './../Axios/EntitysCRUD'
import Idea from './../Libraries/Entitys/Idea'

interface TableEntityProps {
    Columns: ColumnsType<any>
    Entity: string
}

const rowSelection: TableRowSelection<any> = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

const TableEntity = (props: TableEntityProps) => {
    const [data, setData] = useState<Array<any> | null>(null)
    let refresh = () => Get(setData, props.Entity, {})
    useEffect(() => { if (data == null) refresh() }, [])
    return (
        <Table
            columns={props.Columns}
            rowSelection={{ ...rowSelection, checkStrictly: false }}
            dataSource={data || []}
        />
    )
}

export default TableEntity