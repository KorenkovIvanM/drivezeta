import React from 'react';
import { Button, Space } from 'antd';

const App: React.FC = () => (
  <Space wrap>
    <Button danger>Create</Button>
    <Button danger>Delete</Button>
  </Space>
);

export default App;