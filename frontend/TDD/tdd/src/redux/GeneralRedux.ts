import Page from '../Libraries/Page';

// Page
import HomePage from "../Pages/HomePage";

type User = {
    IsActeve: boolean
    Id: number
}

type Site = {
    Name: string
}

type Project ={
    Id: number
    Title: string
}

type DefaultState = {
    User: User
    Site: Site
    Pages: Array<Page>
    Project?: Project | null
}

function getPage(
    Name: string | null,
    URL: string,
    Page: any,
    Image?: string | null,
    IsShow: boolean = true,
    Access: boolean = true
): Page {
    return {
        Name,
        URL,
        Image,
        Page,
        IsShow,
        Access
    } as Page
}
const defaultPage: Array<Page> = [
    getPage("Home", "/home", typeof HomePage),
    getPage("Test table", "/testTable", typeof HomePage)
  ];

const defaultState: DefaultState = {
    User: {
        IsActeve: false,
        Id: 0,
    },
    Site: {
        Name: "DRIVEZETA",
    },
    Pages: [

    ],
}

const reducer = (state: DefaultState = defaultState): DefaultState => {
    return defaultState;
}

export default reducer;
