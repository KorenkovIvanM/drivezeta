import Page from '../Libraries/Page'
import DefaultState from '../Libraries/DefaultState'

// Page
import HomePage from "../Pages/HomePage"
import TableTestPage from '../Pages/TableTestPage'
import TableTest from '../Pages/TableTest'

function getPage(
    Name: string | null,
    URL: string,
    Page: any,
    Image?: string | null,
    IsShow: boolean = true,
    Access: boolean = true
): Page {
    return {
        Name,
        URL,
        Image,
        Page,
        IsShow,
        Access
    } as Page
}
const defaultPage: Array<Page> = [
    getPage("Home", "/home", <HomePage />),
    getPage("Test table", "/testTable", <TableTestPage />),
    // getPage("Test table entioty", "/testTableEntity", <TableTest />),
    getPage("Settings", "/settings", <HomePage />),
  ];

const defaultState: DefaultState = {
    User: {
        IsActeve: false,
        Id: 0,
    },
    Site: {
        Name: "DRIVEZETA",
    },
    Pages: defaultPage,
}

export default defaultState;
