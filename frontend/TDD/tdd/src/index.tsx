import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './Layout/App'
import reportWebVitals from './reportWebVitals'

// Redux
import { Provider } from "react-redux"
import { createStore } from "redux"
import reducer from './redux/GeneralRedux'


// routers
import { BrowserRouter } from "react-router-dom";

const store = createStore(reducer);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
        <BrowserRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </React.StrictMode>

);
reportWebVitals();
