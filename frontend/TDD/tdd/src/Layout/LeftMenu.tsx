import React, { useState } from 'react';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import { useNavigate } from 'react-router-dom';

import State from "./../redux/_delet_me_place_"

type MenuItem = Required<MenuProps>['items'][number];
let items: MenuItem[] = [];
function getItem(
  label: React.ReactNode,
  key?: React.Key | null,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}



const App: React.FC = () => {
  items = State.Pages.map((page) => getItem(page.Name, page.URL))

  const [current, setCurrent] = useState('1');

  const navigate = useNavigate();
  const onClick: MenuProps['onClick'] = (e) => { 
    setCurrent(e.key); 
    navigate(e.key, { replace: true })
  };

  return (
    <Menu
      theme={'dark'}
      onClick={onClick}
      // TODO move module css
      style={{ width: "100%", height: "calc(100vh - 50px)" }}
      defaultOpenKeys={['sub1']}
      selectedKeys={[current]}
      mode="inline"
      items={items}
    />
  );
};

export default App;