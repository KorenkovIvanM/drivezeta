import React from 'react'
import { Col, Row } from 'antd'
import LeftMenu from "./LeftMenu"
import TopMenu from './TopMenu'
import Context from "./Context";

const App: React.FC = () => (
  <>
    <Row style={{height: "50px"}}>
      <Col span={24}>
        <TopMenu />
      </Col>
    </Row>
    <Row>
      <Col span={4}>
        <LeftMenu />
      </Col>
      <Col span={20}>
        <Context />
      </Col>
    </Row>
  </>
);

export default App;