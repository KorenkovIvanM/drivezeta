import { Routes, Route } from "react-router-dom";

import State from "./../redux/_delet_me_place_"

export default function PersistentDrawerLeft() {
    return (
        <Routes>
            {State.Pages.map(page => <Route key={page.URL} path={page.URL} element={page.Page} />)}
        </Routes>
    );
}
