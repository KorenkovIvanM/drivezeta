import axios from "axios";

const Post = (
    callBack: (result: boolean) => void,
    entity: string,
    data: any) => {
    let request = `https://localhost:7177/${entity}`
    axios({
        method: 'post',
        url: request,
        data: data,
    }).then((respomce) => {
        if (respomce.data)
            callBack(respomce.data)
    })
        .catch((errors) => { console.log(errors) })
}

const Get = (
    callBack: (result: Array<any>) => void,
    entity: string,
    mask: any,
    numberPage: number = 0,
    sizePage: number = 15) => {
    let request = `https://localhost:7177/${entity}?mask=${JSON.stringify(mask)}&numberPage=${numberPage}&sizePage=${sizePage}`
    axios.get(request)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data.map((item: any) => { return { ...item, key: item.id } }))
        })
        .catch((errors) => { console.log(errors) })
}

const Put = (
    callBack: (result: boolean) => void,
    entity: string,
    data: any) => {
    let request = `https://localhost:7177/${entity}?mask=${JSON.stringify(data)}`
    debugger
    axios.put(request)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data)
        })
        .catch((errors) => { console.log(errors) })
}

const Delete = (
    callBack: (result: boolean) => void,
    entity: string,
    id: number) => {
    let request = `https://localhost:7177/${entity}?id=${id}`
    axios.delete(request)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data)
        })
        .catch((errors) => { console.log(errors) })
}

export {
    Post,
    Get,
    Delete,
    Put
}