type Page = 
{
    Name: string | null 
    URL: string
    Image: string | null
    Page: any
    IsShow: boolean
    Access: boolean
}

export default Page;