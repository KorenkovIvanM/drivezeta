import TypeColumns from "./TypeColumns"

type ColumnCrud ={
    title: string
    dataIndex: string
    key: string
    isUpdate: boolean
    type: TypeColumns
    sorter?: (a: any, b: any) => any
}

export default ColumnCrud;