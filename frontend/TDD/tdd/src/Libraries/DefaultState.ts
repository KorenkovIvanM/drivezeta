import Page from '../Libraries/Page'
import User from '../Libraries/User'
import Site from '../Libraries/Site'
import Project from '../Libraries/Project'

type DefaultState = {
    User: User
    Site: Site
    Pages: Array<Page>
    Project?: Project | null
}

export default DefaultState;