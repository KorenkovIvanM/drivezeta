type Idea ={
    title: string
    description: string
    id: number
}

export default Idea;