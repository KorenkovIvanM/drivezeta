enum TypeColumns {
    String = "String",
    Number = "Number",
    Date = "Date",
    Boolean = "Boolean",
}

export default TypeColumns;