import { createStore } from 'redux'

const initialState = {
  sidebarShow: false,
  entity: null,
  nameEntity: "",
  callback: () => console.log("Error"),
  item: undefined,
}

const changeState = (state = initialState, action) => {
  switch (action.type) {
    case 'set':
      return { ...state, ...action }
    case 'SET_ENTITY':
      console.log(action.callback)
      return {
        ...state,
        entity: {
          name: action.name,
          callback: action.callback
        },
        nameEntity: action.nameEntity,
        callback: action.callback
      }
    case 'SET_ITEM':
      console.log("--------------------------------3")
      console.log(action.item);
      return {
        ...state,
        item: action.item
      }
    default:
      return state
  }
}

const store = createStore(changeState)
export default store
