import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilNotes,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavTitle,
    name: 'Time-management',
  },
  {
    component: CNavGroup,
    name: 'Personal',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Hard tasks',
        to: '/forms/hardTasks',
      },
      {
        component: CNavItem,
        name: 'Soft tasks',
        to: '/forms/softTasks',
      },
      {
        component: CNavItem,
        name: 'Month',
        to: '/forms/monthTasks',
      },
      {
        component: CNavItem,
        name: 'Week',
        to: '/forms/weekTasks',
      },
      {
        component: CNavItem,
        name: 'Cronos',
        to: '/forms/Cronos',
      },
      {
        component: CNavItem,
        name: 'Test',
        to: '/forms/test',
      },
    ],
  },
]

export default _nav
