import axios from "axios";

const Get = (callBack, entity, mask, numberPage, sizePage) => {
    let request = `https://localhost:7116/${entity}/${JSON.stringify(mask)}?numberPage=${numberPage}&sizePage=${sizePage}`;
    axios.get(request)
        .then((respomce) => {
            console.log(callBack)
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const GetCount = (callBack, entity) => {
    let request = `https://localhost:7116/${entity}/idea/count`;
    console.log(request);
    axios.get(request)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const Post = (callBack, entity, _data) => {
    axios({
        method: 'post',
        url: `https://localhost:7116/${entity}`,
        data: _data,
    })
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const Delete = (callBack, entity, id) => {
    let request = `https://localhost:7116/${entity}?Id=${id}`;
    axios.delete(request)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

const UpdateIdeaPostfix = (callBack, id, postfix) => {
    axios.put(`https://localhost:7135/Idea?Id=${id}${postfix}`)
        .then((respomce) => {
            if (respomce.data)
                callBack(respomce.data);
        })
        .catch((errors) => {
            console.log(errors);
        });
};

export {
    Get,
    GetCount,
    Post,
    Delete,
};
