import TableCrud from "./../../../components/UI/TableCrud/TableCrud";
import FormCreateCronos from "../FormCreate/FormCreateCronos"

export default () => {
  return (
    <TableCrud
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 1
      }}
      columns={
        [
          { field: 'id', headerName: 'ID', width: 70 },
          { field: 'userId', headerName: 'userId', width: 130 },
          { field: 'title', headerName: 'title', width: 130, render: ({ value }) => <button>{value}</button>, },
          {
            field: 'create',
            headerName: 'create',
            width: 160,
            type: 'date',
          },
          {
            field: 'fullName',
            headerName: 'Full name',
            sortable: false,
            width: 160,
            valueGetter: (params) =>
              {
                return(
                    <p>{params.row.id}</p>
                )
              },
          },
        ]
      }
      form={<FormCreateCronos />}
      entity={"Cronos"} 
      />
  );
}