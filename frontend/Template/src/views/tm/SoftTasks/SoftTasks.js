import TableCrud from "./../../../components/UI/TableCrud/TableCrud";
import FormCreateOrderSoft from "../FormCreate/FormCreateOrderSoft"

export default () => {
  return (
    <TableCrud
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 0
      }}
      columns={
        [
          { field: 'id', headerName: 'ID', width: 70 },
          { field: 'userId', headerName: 'userId', width: 130 },
          { field: 'title', headerName: 'title', width: 130, render: ({ value }) => <button>{value}</button>, },
          {
            field: 'discription',
            headerName: 'discription',
            width: 90,
          },
          {
            field: 'create',
            headerName: 'create',
            width: 160,
            type: 'date',
          },
        ]
      }
      form={<FormCreateOrderSoft />}
      entity={"Order"} 
      />
  );
}