import React, { useState } from 'react'
import {
    CCol,
    CForm,
    CFormInput,
    CFormLabel,
    CButton,
} from '@coreui/react'
import { Post } from "../../../Ajax/Entity"
import { useSelector } from 'react-redux';

const CustomStyles = (props) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");

    const callback = useSelector(state => state.callback)
    const nameEntity = useSelector(state => state.nameEntity)

    return (
        <CForm
            className="row g-3"
        >
            <CCol md={4}>
                <CFormLabel>User</CFormLabel>
                <CFormInput type="text" />
            </CCol>
            <CCol md={8}>
                <CFormLabel>Title</CFormLabel>
                <CFormInput type="text" onChange={(e) => setTitle(e.target.value)} />
            </CCol>
            <CCol md={12}>
                <CFormLabel>Description</CFormLabel>
                <CFormInput type="text" onChange={(e) => setDescription(e.target.value)} />
            </CCol>
            <CCol xs={7}></CCol>
            <CCol xs={3}>
                <CButton color="danger">
                    DISAGREE
                </CButton>
            </CCol>
            <CCol xs={2}>
                <CButton color="primary" onClick={() => {
                    Post(() => callback(), nameEntity ,{
                        title: title,
                        discription: description,
                        create: new Date(),
                        isTemplate: false,
                        status: 1,
                        type: 4
                      })
                }}>
                    ADD
                </CButton>
            </CCol>
        </CForm>
    )
}

const Validation = () => {
    return <CustomStyles />
}

export default Validation
