import TableCrud from "./TableCrud";

export default () => {
  return (
    <TableCrud
      pageSize={5}
      columns={
        [
          { field: 'id', headerName: 'ID', width: 70 },
          { field: 'userId', headerName: 'userId', width: 130 },
          { field: 'title', headerName: 'title', width: 130, render: ({ value }) => <button>{value}</button>, },
          {
            field: 'discription',
            headerName: 'discription',
            width: 90,
          },
          {
            field: 'create',
            headerName: 'create',
            width: 160,
            type: 'date',
          },
        ]
      }
      entity={"Idea"} 
      />
  );
}