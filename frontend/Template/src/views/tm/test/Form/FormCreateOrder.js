import React, { useState } from 'react'
import {
    CCol,
    CForm,
    CFormInput,
    CFormLabel,
    CButton,
} from '@coreui/react'
import { Post } from "../../../../Ajax/Entity"

const CustomStyles = (props) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    return (
        <CForm
            className="row g-3"
        >
            <CCol md={4}>
                <CFormLabel>User</CFormLabel>
                <CFormInput type="text" />
            </CCol>
            <CCol md={8}>
                <CFormLabel>Title</CFormLabel>
                <CFormInput type="text" onChange={(e) => setTitle(e.target.value)} />
            </CCol>
            <CCol md={12}>
                <CFormLabel>Description</CFormLabel>
                <CFormInput type="text" onChange={(e) => setDiscription(e.target.value)} />
            </CCol>
            <CCol xs={7}></CCol>
            <CCol xs={3}>
                <CButton color="danger">
                    DISAGREE
                </CButton>
            </CCol>
            <CCol xs={2}>
                <CButton color="primary" onClick={() => {
                    Post(props.callback, {
                        projectId: 0,
                        userId: 0,
                        title: "string",
                        discription: "string",
                        create: "2022-11-17T08:21:00.200Z",
                        end: "2022-11-17T08:21:00.200Z",
                        start: "2022-11-17T08:21:00.200Z",
                        stop: "2022-11-17T08:21:00.200Z",
                        isTemplate: true,
                        status: 0,
                        type: 0,
                        weight: 0,
                        orderId: 0,
                        prioritet: 0,
                        targetId: 0,
                        progress: 0
                      })
                }}>
                    ADD
                </CButton>
            </CCol>
        </CForm>
    )
}

const Validation = () => {
    return <CustomStyles />
}

export default Validation
