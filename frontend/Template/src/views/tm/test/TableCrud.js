import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Get, GetCount } from '../../../Ajax/Entity';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import ButtonDialog from "./ButtonDialog";
import FormCreateOrder from "./Form/FormCreateOrder";
import { Delete } from "../../../Ajax/Entity"

export default function DataTable(props) {
  const entity = props.entity;
  const [selectionModel, setSelectionModel] = React.useState([]);
  const [rows, setRows] = React.useState([]);
  React.useEffect(() => {
    Get(setRows, entity, {}, 0, 500);
  }, []);
  
  return (
    <div style={{ height: "75vh", width: '100%' }}>
      <Stack spacing={2} direction="row">
        <ButtonDialog
          name={"ADD"}
          form={<FormCreateOrder callback={(e) => Get(setRows, entity, {}, 0, 500)} /> }
          title={"create tasks"}
          entity={entity}
          callback={(e) => Get(setRows, entity, {}, 0, 500)} />
        <Button
          variant="outlined"
          color="error" 
          onClick={() => {
            for(let index = 0, max = selectionModel.length - 1; index < max; index++)
              Delete((e) => console.log(e), entity, selectionModel[index]);
            Delete(() => Get(setRows, entity, {}, 0, 500), entity, selectionModel[selectionModel.length - 1]);
          }}>Delete</Button>
      </Stack>
      <DataGrid
        rows={rows}
        columns={props.columns}
        pageSize={props.pageSize || 5}
        rowsPerPageOptions={[5]}
        onSelectionModelChange={(newSelectionModel) => {
          setSelectionModel(newSelectionModel);
        }}
        selectionModel={selectionModel}
        checkboxSelection
      />
    </div>
  );
}