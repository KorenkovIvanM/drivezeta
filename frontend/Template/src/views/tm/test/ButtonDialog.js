import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import FormCreateOrder from "./Form/FormCreateOrder";
import {
    CCol,
    CForm,
    CFormInput,
    CFormLabel,
    CButton,
} from '@coreui/react'
import { Post } from "../../../Ajax/Entity"

export default function AlertDialog(props) {
  const [open, setOpen] = React.useState(false)
  
  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = (props) => {
    setOpen(false)
  }
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        {props.name}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">
          {props.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {/* {props.form} */}
            <CForm
              className="row g-3"
            >
              <CCol md={4}>
                <CFormLabel>User</CFormLabel>
                <CFormInput type="text" />
              </CCol>
              <CCol md={8}>
                <CFormLabel>Title</CFormLabel>
                <CFormInput type="text" onChange={(e) => setTitle(e.target.value)} />
              </CCol>
              <CCol md={12}>
                <CFormLabel>Description</CFormLabel>
                <CFormInput type="text" onChange={(e) => setDescription(e.target.value)} />
              </CCol>
              <CCol xs={7}></CCol>
              <CCol xs={3}>
                <CButton color="danger" onClick={handleClose}>
                  DISAGREE
                </CButton>
              </CCol>
              <CCol xs={2}>
                <CButton color="primary" onClick={() => {
                  Post(props.callback, props.entity, {
                    userId: 0,
                    title: title,
                    discription: description,
                    create: new Date(),
                  });
                  handleClose();
                }}>
                  ADD
                </CButton>
              </CCol>
            </CForm>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  )
}