import TableCrud from "./../../../components/UI/TableCrud/TableCrud";
import FormCreate from "../FormCreate/FormCreateOrder";
import FormCreateOrderHard from "../FormCreate/FormCreateOrder"

export default () => {
  return (
    <TableCrud
      pageSize={5}
      mask={{
        IsTemplate: false,
        Status: 1,
        Type: 1
      }}
      columns={
        [
          { field: 'id', headerName: 'ID', width: 70 },
          { field: 'userId', headerName: 'userId', width: 130 },
          { field: 'title', headerName: 'title', width: 130, render: ({ value }) => <button>{value}</button>, },
          {
            field: 'discription',
            headerName: 'discription',
            width: 90,
          },
          {
            field: 'create',
            headerName: 'create',
            width: 160,
            type: 'date',
          },
        ]
      }
      form={<FormCreateOrderHard />}
      entity={"Order"} 
      />
  );
}