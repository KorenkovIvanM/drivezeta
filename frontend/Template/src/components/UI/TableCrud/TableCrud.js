import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Get, GetCount } from "./../../../Ajax/Entity";
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import ButtonDialog from "./ButtonDialog";
import { Delete } from "./../../../Ajax/Entity"
import { useDispatch, useSelector } from 'react-redux';

export default function DataTable(props) {
  const entity = props.entity;
  const [selectionModel, setSelectionModel] = React.useState([]);
  const [rows, setRows] = React.useState([]);

  const nameEntity = useSelector(state => state.nameEntity);
  const dispatch = useDispatch();
  const refreshPage = () => {
    Get(setRows, entity, props.mask || {}, 0, 500);
  }
  if (nameEntity != entity) {
    dispatch({
      type: "SET_ENTITY",
      entity: {
        name: entity,
        callback: refreshPage()
      },
      nameEntity: "" + entity,
      callback: refreshPage
    });
  }


  React.useEffect(() => {
    refreshPage();
  }, []);
  // console.log("############################")
  // console.log(selectionModel);
  return (
    <div style={{ height: "75vh", width: '100%' }}>
      <Stack spacing={2} direction="row">
        <ButtonDialog
          name={"ADD"}
          form={props.form}
          title={"create tasks"}
          entity={{ entity }}
          callback={(e) => refreshPage()} />
        {selectionModel.length == 1 ?
          <ButtonDialog
            name={"Update"}
            form={props.form}
            title={"update tasks"}
            entity={{ entity }}
            item={selectionModel[0]}
            callback={(e) => refreshPage()} />
          :
          <Button
            variant="outlined"
            color="warning"
            disabled>Update</Button>}
        <Button
          variant="outlined"
          color="error"
          onClick={() => {
            for (let index = 0, max = selectionModel.length - 1; index < max; index++)
              Delete((e) => console.log(e), entity, selectionModel[index]);
            Delete(() => refreshPage(), entity, selectionModel[selectionModel.length - 1]);
          }}>Delete</Button>
      </Stack>
      <DataGrid
        rows={rows}
        columns={props.columns}
        pageSize={props.pageSize || 5}
        rowsPerPageOptions={[5]}
        onSelectionModelChange={(newSelectionModel) => {
          setSelectionModel(newSelectionModel);
        }}
        selectionModel={selectionModel}
        checkboxSelection
      />
    </div>
  );
}