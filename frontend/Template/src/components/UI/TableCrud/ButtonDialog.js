import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import { useDispatch, useSelector } from 'react-redux'

export default function AlertDialog(props) {
  const [open, setOpen] = React.useState(false)
  
  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = (props) => {
    setOpen(false)
  }

  const dispatch = useDispatch();
  const item = useSelector(state => state.item);
  // console.log(`id ${props}`)
  // console.log(props)
  // debugger;
  if(item != props.item)
  {
    dispatch({
      type: "SET_ITEM",
      item: props.item,
    });
  }
 

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        {props.name}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">
          {props.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.form}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  )
}